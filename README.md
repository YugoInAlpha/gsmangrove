# 📢 Tentang GSMangrove

GSMangrove adalah sebuah aplikasi web yang diciptakan untuk memenuhi kebutuhan kegiatan pada RT. 6 RW. 7 Kelurahan Wonorejo Kecamatan Rungkut Kota Surabaya.

# 📒 Buku Panduan GSMangrove
0. 📖 [Google Drive](https://drive.google.com/drive/folders/1FYNvGsRRMwQHFaUbjjZVQABRJP_922t5?usp=sharing)
1. 📖 [Buku Panduan Pihak RT](https://drive.google.com/file/d/1658BEUqvnVAmoHdRWOoXAnqIWTHsyJZV/view?usp=sharing)
2. 📖 [Buku Panduan Warga](https://drive.google.com/file/d/13ou9gKcKgOuAJjqbU5RfxuV4BZXyTaAA/view?usp=sharing)


# 🎯 Fitur GSMangrove
1. ⚡ Login Berdasarkan Hak Akses Pengguna : Pihak Kepengurusan RT, Warga
2. ⚡ Tampilan Dashboard
3. ⚡ Pendataan Data Kependudukan Mandiri
4. ⚡ Transaksi Pembayaran Iuran Mandiri

# 📑 Pengembangan GSMangrove

| LIST | Versi |
| ------ | ------ |
| Laravel | 8.* |
| php | 8.* |

1. 📌 [Framework Laravel](https://laravel.com/)
- ***Framework*** Laravel digunakan sebagai dasar pengembangan proyek GSMangrove, yang menggunakan bahasa pemrograman **PHP** dan menggunakan prinsip ***Object Oriented Programming*** (**OOP**) yang memudahkan pengembangan lebih lanjut.

2. 📌 [STISLA](https://getstisla.com/)
- STISLA merupakan sebuah *admin template* ***open source*** yang memudahkan dalam membuat sebuah tampilan halaman website

# 📦 Package GSMangrove
### 🗃 Package Umum :
1. 📂 [sweet-alert](https://sweetalert2.github.io/)
2. 📂 [laravel-excel](https://laravel-excel.com/)

### 🗃 Package Mode Pengembangan :
1. 📂 [laravel-debugbar](https://github.com/barryvdh/laravel-debugbar)
2. 📂 [laravel-query-detector](https://github.com/beyondcode/laravel-query-detector)
3. 📂 [laravel-ide-helper](https://github.com/barryvdh/laravel-ide-helper) **Disabled**
4. 📂 [laravel-livewire](https://laravel-livewire.com/) **Not Implemented**

# 📖 Pemasangan Proyek GSMangrove di Localhost/Perangkat Sendiri
### ⚙️ Kebutuhan Software
##### 🎓[Tutorial Instal Laravel](https://youtu.be/pZqk57Xvujs)
1. 💻 [XAMPP](https://www.apachefriends.org/index.html)
2. 💻 [Composer](https://getcomposer.org)
3. 💻 [GIT](https://git-scm.com) **Optional**
4. 💻 [VSCode](https://code.visualstudio.com) **Bisa yang lain**

### 🎞 Video STEP
1. 🎞 [Pemasangan Proyek Local](https://drive.google.com/file/d/1T-sdaQ6dtU8FeePRS1UxtImNBPPJDZL1/view?usp=sharing)

### 🧩 STEP [1]
1. Silahkan unduh proyek pada gitbucket pada **"menu titik tiga"** dan **"download repository"**
2. Setelah di download silakan extract file proyek ke directory perangkat **[Bebas ditaruh dimana]**
3. Setelah extract selesai, silahkan ganti nama file agar lebih mudah untuk persiapan selanjutnya

### 🧩 STEP [2]
1. Saat sudah memasuki file proyek, akan terdapat file dengan nama **".env.example"** silahkan copy file tersebut di tempat yang sama dan ubah nama file menjadi **".env"**
2. Selanjutnya buka XAMPP untuk menyiapkan database dengan menyalakan apache dan MySQL
3. Setelah modul di jalankan, silahkan akses **"localhost/phpmyadmin"** untuk mengakses penyimpanan database
4. Setelah selesai membuat **"localhost/phpmyadmin"**, klik tombol **"Baru"** untuk membuat database baru
5. Saat mengisi nama database pastikan penamaan sama persis dengan data yang terdapat di **".env"** **"DB_DATABASE"** yang bertuliskan **"db_gsm"** sebagai nama database

### 🧩 STEP [3]
1. Setelah membuat database, selanjutnya kita membuka folder proyek dan membuka terminal di vscode / CMD (Manual)
2. Lalu ketik **"composer install"** untuk mendownload package proyek
3. Setelah selesai, selanjutnya ketik **"php artisan key:generate"** untuk membuat kode key laravel
4. Selanjutnya ketik **"php artisan migrate --seed"** untuk menjalankan perintah membuat tabel di database yang telah dibuat sebelumnya
5. Setelah selesai, silahkan ketik **"php artisan storage:link"** untuk membuat link folder agar file/dokumen yang disimpan bisa diakses di halaman website

### 🧩 STEP [Final]
1. ketik **"php artisan serve"** untuk menjalankan website local di perangkat

# 📖 Pemasangan Proyek GSMangrove di Hosting
### ⚙️ Kebutuhan Software
1. 💻 [Putty](https://www.putty.org/)

### Tips
> Untuk contoh [Hosting DomainAsia](https://www.domainesia.com/) pastikan membeli layanan hosting yang menyediakan **"Akses SSH"**

### 🎞 Video STEP
1. 🎞 **[Rekomendasi]** [Pemasangan Proyek Hosting Repository](https://drive.google.com/file/d/1e9iyHA3ee8V6qLwHLS1H6OU-wD3E9zN9/view?usp=sharing)
2. 🎞 [Pemasangan Proyek Hosting Manual](https://drive.google.com/file/d/1eoyx0FCrZ1STGfPJOxHm6Vnh5gH_9JGd/view?usp=sharing)

### 🧩 STEP Repository [1]
1. Pastikan sudah memiliki **"Akun CPanel"**, dan silahkan **"Login"** kedalam CPanel
2. Berikutnya adalah **"merubah versi PHP"** dengan cara:
    a. pilih menu **"Select PHP Version"** pada CPanel
    b. pilih versi PHP 8.0 atau diatasnya lalu klik **"Set as current"**

### 🧩 STEP Repository [2]
0. ***Pastikan Sudah Menginstal Putty***
1. Pertama, **"Login"** Kedalam Putty menggunakan **"Akun CPanel"** dengan cara:
**CONTOH**
> Host Name : [nama pengguna]@[nama website] : gsmangro@gsmangrove.site
> Port : 64000 (mungkin bisa berbeda pada tiap server)
2. Silahkan membuka gitbucket dan klik tombol **"Clone"** setelah itu copy url yang telah disediakan
> Contoh : "git clone https://YugoInAlpha@bitbucket.org/YugoInAlpha/gsmangrove.git"
3. **"Copy"** kedalam terminal Putty dan **"klik Enter"**
4. Tunggu Proses Selesai

### 🧩 STEP Repository [3]
1. Buka **"CPanel"** dan klik tombol **"File Manager"** untuk membuat Penyimpanan Hosting
2. Berikutnya adalah silahkan **"Menghapus File public_html"** bawaan hosting
3. lalu kita buka Putty untuk menjalankan perintah **"ln -s /home/nama_user/nama_proyek/public /home/nama_user/public_html"** yang bertujuan memindahkan folder public proyek ke hosting
4. Buka **"Putty"**, dan pergi kedalam proyek dengan menggunakan perintah **"cd"**  untuk mencari Folder Proyek
5. Setelah masuk kedalam **"Folder Proyek"**, silahkan ketik perintah **"composer install"** untuk menginstall package proyek

### 🧩 STEP Repository [4]
1. Buka **"CPanel"** dan klik tombol **"MySQL® Databases"**
2. Silahkan buat database pada bagian **"Create New Database"**
3. Selanjutnya membuat user pada bagian **"Add New User"**
4. Selanjutnya menempatkan user kedalam database pada bagian **"Add User To Database"** dan klik **"Add"**
5. Pada bagian **"Manage User Privileges"** centang **"ALL PRIVILEGES"** dan klik **"Make Change"**

### 🧩 STEP Repository [5]
1. Buka **"Putty"**, dan pastikan anda masih didalam Folder Proyek
2. Selanjutnya, ketik **"cp .env.example .env"** untuk membuat duplikat file dengan nama .env
3. Selanjutnya, ketik **"nano .env"** dan ubah file didalamnya seperti:
**"DB_DATABASE"** -> disesuaikan dengan database yang telah dibuat
**"DB_USERNAME"** -> disesuaikan dengan akun yang telah dibuat
**"DB_PASSWORD"** -> disesuaikan dengan akun yang telah dibuat
**"APP_URL"** -> disesuaikan dengan domain

[Cara Menggunakan Nano di Putty](https://www.hostinger.co.id/tutorial/cara-install-menggunakan-nano-text-editor)

| Perintah | Keterangan |
| ------ | ------ |
|CTRL+O| Tekan tombol  untuk menyimpan perubahan yang dibuat di file dan melanjutkan proses editing. |
|CTL+X| Tekan tombol  untuk keluar dari editor. Jika Anda mengubah sesuatu, sebelum keluar, Anda akan dimintai konfirmasi apakah perubahan tersebut hendak disimpan atau tidak. Tambahkan Y yang berarti Yes atau N yang adalah No, kemudian tekan Enter. Namun, kalau Anda tidak mengubah sesuatu, Anda bisa langsung keluar dari editor. |

### 🧩 STEP Repository [Final]
1. Ketik **"php artisan key:generate"** untuk membuat key pada .env
2. Ketik **"php artisan storage:link"** untuk membuat dari folder storage ke public
3. Ketik **"php artisan migrate --seed"** untuk membuat tabel kedalam database
4. Silahkan akses domain untuk membuka proyek di browser

 ============ END STEP ============ |
| ------ |

### 🧩 STEP Manual [1]
1. Pertama, **"siapkan Proyek"** dan **"Akun CPanel"**
2. silahkan **"Login"** kedalam CPanel
3. Masuk ke **"File Manager"** untuk mengupload File Proyek kedalam Penyimpanan Hosting
4. Jika sudah mengupload proyek di **"Directory"** yang ditentukan, berikutnya adalah melakukan **"Extract"** ke root Directory atau Directory paling luar dari Penyimpanan Hosting

### 🧩 STEP Manual [2]
1. Setelah menyiapkan aplikasi **"Putty"**
2. Silahkan melakukan Open SSH dengan menggunakan **"Akun CPanel"** yang sudah dimiliki dengan cara:
**CONTOH**
> [nama pengguna]@[nama website] : gsmangro@gsmangrove.site
> Port : 64000 (mungkin bisa berbeda pada tiap server)

2. lalu **"klik open"** untuk Connect kedalam SSH CPanel dengan **"memasukkan Password CPanel"**
3. Cari Folder **"Proyek"** yang telah di extract dengan menggunakan perintah **"cd"** untuk mencari Folder Proyek
4. Silahkan ketik **"php artisan storage:link"** untuk membuat link folder agar file/dokumen yang disimpan bisa diakses
5. Pindahkan folder **"Public"** pada proyek ke **"public_html"** di Penyimpanan Hosting dengan cara:
ketik **"ln -s /home/nama_user/nama_folder_proyek/public /home/nama_user/public_html/nama_folder_proyek"**

### 🧩 STEP Manual [3]
1. Selanjutnya adalah membuat database Proyek dengan mengklik **"MySQL Database"** pada menu CPanel
2. buat sebuah database baru pada **"Create New Database"**
3. buat sebuah User agar dapat mengakses database yang sudah dibuat sebelumnya
4. Setelah membuat sebuah database, tambahkan User yang sudah dibuat kedalam database pada **"Add User To Database"**
5. Selanjutnya memberikan Hak Akses di **"Manage User Privileges"**

### 🧩 STEP Manual [4]
1. Akses menu **"phpMyAdmin"** pada CPanel
2. Pilih **"Import"** dan pilih file **"db_GSM.sql"**[link](https://drive.google.com/file/d/1uSzfVvKhiwXob9BQRym1QcBECKNwQkId/view?usp=sharing) lalu kirim untuk menyimpan

### 🧩 STEP Manual [Final]
1. Masuk kedalam Folder Proyek
2. Lakukan **"ubah data .env"** seperti
**"DB_DATABASE"** -> disesuaikan dengan database yang telah dibuat
**"DB_USERNAME"** -> disesuaikan dengan akun yang telah dibuat
**"DB_PASSWORD"** -> disesuaikan dengan akun yang telah dibuat
**"APP_URL"** -> disesuaikan dengan domain