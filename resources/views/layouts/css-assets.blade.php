 <!-- General CSS Files -->
 <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
 <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}" rel="preload">

 <!-- CSS Library -->
 @stack('css-library')

 <!-- CSS Template -->
 <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
 <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">

 @stack('css-template')

 <style>
     .swal-button.swal-button--pembayaran_sukses {
         box-shadow: 0 2px 6px #acb5f6;
         background-color: #6777ef;
     }

     .swal-button.swal-button--pembayaran_sukses:hover {
         opacity: .8;
     }

     .swal-button.swal-button--pembayaran_gagal {
         box-shadow: 0 2px 6px #fd9b96;
         background-color: #fc544b;
     }

     .swal-button.swal-button--pembayaran_gagal:hover {
         opacity: .8;
     }

 </style>
