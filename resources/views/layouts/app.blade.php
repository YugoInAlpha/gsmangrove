@extends("layouts.base")
@section('body')
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <x-admin-components.navbar></x-admin-components.navbar>
            <x-admin-components.sidebar></x-admin-components.sidebar>
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>@yield("title", "")</h1>
                    </div>
                    <div class="section-body">
                        @yield("content")
                    </div>
                </section>
            </div>

            @yield("modal")
            <x-admin-components.footer></x-admin-components.footer>
        </div>
    </div>
@endsection
