<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ config('app.name') }} | @yield("title", "")</title>
    @include("layouts.auth-layout.css-assets")
</head>

<body>
    @include('sweetalert::alert')
    @yield("body")
    @include("layouts.auth-layout.js-assets")
</body>

</html>
