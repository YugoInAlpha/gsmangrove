@extends("layouts.auth-layout.base")
@section('body')
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="@yield('class-form')">
                        <div class="login-brand">
                            <h4>Sistem Informasi GSMangrove</h4>
                            <h5><strong>RT.6 RW.7</strong></h5>
                        </div>
                        @yield("content")
                        <x-auth-components.footer></x-auth-components.footer>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
