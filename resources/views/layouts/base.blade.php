<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ config('app.name') }} | @yield("title", "")</title>
    @include("layouts.css-assets")
</head>

<body>
    @yield("body")
    @include('sweetalert::alert')
    @include("layouts.js-assets")
</body>

</html>
