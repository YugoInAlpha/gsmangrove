@extends("layouts.app")

@section('title', 'Tambah Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Perumahan Green Semanggi Mangrove
    </p>
    <form action="{{ route('rumah.store') }}" method="POST" novalidate>
        @csrf
        @include("pengguna.rumah._form")
    </form>
@endsection
