@extends("layouts.app")

@section('title', 'Ubah Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Perumahan Green Semanggi Mangrove
    </p>
    <form action="{{ route('rumah.update', $rumah->id_perumahan) }}" method="POST" novalidate>
        @method("PUT")
        @csrf
        @include("pengguna.rumah._form")
    </form>
@endsection