@extends("layouts.app")

@section('title', 'Daftar Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Perumahan Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('rumah.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Perumahan
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables">
                            <thead>
                                <tr>
                                    <th>Perumahan</th>
                                    <th>Kepemilikan Rumah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($perumahan->perumahan as $data_perumahan)
                                    <tr>
                                        <td>{{ $data_perumahan->cluster->nama_cluster }} {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}</td>
                                        <td>{{ $data_perumahan->kepemilikan_rumah }}</td>
                                        <td>
                                            <div class="d-block"></div>
                                            @if ($data_perumahan->kepemilikan_rumah == 'Kontrak/Sewa')
                                                <a href="#" class="btn btn-sm btn-icon icon-left btn-info btn-lihat" data-toggle="modal" data-target=".modal-detail" data-perumahan="{{ $data_perumahan->nama_cluster }} {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}" data-kepemilikan_rumah="{{ $data_perumahan->kepemilikan_rumah }}" data-periode_kontrak="{{ $data_perumahan->periode_kontrak }}" data-periode_waktu="{{ $data_perumahan->periode_waktu }}" data-tanggal_mulai_kontrak="{{ $data_perumahan->tanggal_mulai_kontrak }}" data-tanggal_akhir_kontrak="{{ $data_perumahan->tanggal_akhir_kontrak }}">
                                                    <i class="far fa-eye"></i> Lihat
                                                </a>
                                            @else
                                                <a href="#" class="btn btn-sm btn-icon icon-left btn-info btn-lihat disabled">
                                                    <i class="far fa-eye"></i> Lihat
                                                </a>
                                            @endif
                                            <a href="{{ route('rumah.edit', $data_perumahan->id_perumahan) }}" class="btn btn-sm btn-icon icon-left btn-warning mr-1">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>
                                            <form style="all: unset" action="{{ route('rumah.destroy', $data_perumahan->id_perumahan) }}" id="form_delete_perumahan_{{ $data_perumahan->id_perumahan }}" data-id="{{ $data_perumahan->cluster->nama_cluster }} {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirmPerumahan('form_delete_perumahan_{{ $data_perumahan->id_perumahan }}')" class="btn btn-sm btn-icon icon-left btn-danger"><i class="far fa-trash-alt"></i>Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    <div class="modal fade modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Perumahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-ktp">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <tr>
                                <th class="w-50">Perumahan</th>
                                <td class="w-50 text-left" id="perumahan">null</td>
                            </tr>
                            <tr>
                                <th>Kepemilikan Rumah</th>
                                <td class="w-50 text-left" id="kepemilikan_rumah">null</td>
                            </tr>
                            <tr>
                                <th>Periode Kontrak/Sewa</th>
                                <td class="w-50 text-left" id="periode_kontrak">null</td>
                            </tr>
                            <tr>
                                <th>Tanggal Mulai Kontrak</th>
                                <td class="w-50 text-left" id="tanggal_mulai">null</td>
                            </tr>
                            <tr>
                                <th>Tanggal Akhir Kontrak</th>
                                <td class="w-50 text-left" id="tanggal_akhir">null</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true,
                    },
                    {
                        "width": "28%",
                        "targets": -1,
                    },
                    {
                        orderable: false,
                        targets: -1,
                    },
                ],
            });

            $(".btn-lihat").click(function() {
                var perumahan = $(this).data("perumahan");
                var kepemilikan_rumah = $(this).data("kepemilikan_rumah");
                var periode_kontrak = $(this).data("periode_kontrak");
                var periode_waktu = $(this).data("periode_waktu");
                var tanggal_mulai_kontrak = $(this).data("tanggal_mulai_kontrak");
                var tanggal_akhir_kontrak = $(this).data("tanggal_akhir_kontrak");

                if (periode_waktu == "years") {
                    var periode = "Tahun";
                } else {
                    var periode = "Bulan";
                };

                var opsi = {
                    day: 'numeric',
                    weekday: 'long',
                    month: 'long',
                    year: 'numeric',
                };
                const convert_mulai = new Date(tanggal_mulai_kontrak);
                const convert_akhir = new Date(tanggal_akhir_kontrak);

                var mulai = convert_mulai.toLocaleDateString("id", opsi);
                var akhir = convert_akhir.toLocaleDateString("id", opsi);
                $("#perumahan").text(perumahan);
                $("#kepemilikan_rumah").text(kepemilikan_rumah);
                $("#periode_kontrak").text(periode_kontrak + " " + periode);
                $("#tanggal_mulai").text(mulai);
                $("#tanggal_akhir").text(akhir);
            });
        });

        function deleteconfirmPerumahan(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
