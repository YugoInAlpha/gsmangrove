@extends("layouts.app")

@section('title', 'Dashboard')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row">
                <div class="col col-xl-3 col-lg-12 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-user-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Anggota Keluarga</h4>
                            </div>
                            <div class="card-body">
                                {{ $warga }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-xl-3 col-lg-12 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-success">
                            <i class="far fa-check-circle size-icn"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Iuran Dibayar</h4>
                            </div>
                            <div class="card-body" id="iuran_bayar">
                                0
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-xl-3 col-lg-12 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-danger">
                            <i class="far fa-times-circle size-icn"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Iuran Belum Dibayar</h4>
                            </div>
                            <div class="card-body" id="iuran_belum_bayar">
                                0
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-primary">Iuran Perumahan : {{ $judul_dashboard }}</h4>
                    </div>
                    <form action="{{ route('daftar.rumah_warga') }}" method="POST">
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="form-group p-1">
                                    <select name="data_perumahan" class="form-control select2">
                                        <option value="">Pilih Perumahan</option>
                                        @if (!empty(session()->get('auth_data.0.data_perumahan'))) {
                                            @for ($i = 0; $i < count(session()->get('auth_data.0.data_perumahan')); $i++)
                                                <option value="{{ $i }}"
                                                    @if (session()->get('auth_data.0.data_perumahan.' . $i . '.status_dashboard') == 'Aktif') selected @endif>
                                                    {{ session()->get('auth_data.0.data_perumahan.' . $i . '.nama_cluster') }}
                                                    {{ session()->get('auth_data.0.data_perumahan.' . $i . '.blok_perumahan') }}
                                                    -
                                                    {{ session()->get('auth_data.0.data_perumahan.' . $i . '.nama_perumahan') }}
                                                </option>
                                            @endfor
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group p-1">
                                    <button type="submit" class="btn btn-block btn-icon icon-left btn-success">
                                        <i class="fas fa-home"></i> Ganti Perumahan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body p-0">
                        <div class="table-responsive table-scroll">
                            <table class="table table-bordered">
                                @php
                                    $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                    
                                    $tranasksi_bulanan_sudah_terbayar = 0;
                                    $count_transaksi_sudah_terbayar = 0;
                                    
                                    $tranasksi_bulanan_belum_terbayar = 0;
                                    $count_transaksi_belum_terbayar = 0;
                                    
                                    $data_iuran_belum_terbayar = [];
                                    
                                    $tahun_sekarang = \Carbon\Carbon::now()->isoFormat('Y');
                                    $bulan_sekarang = \Carbon\Carbon::now()->isoFormat('M');
                                    $tahun_kedepan = date('Y', strtotime('+1 year', strtotime($tahun_sekarang)));
                                    
                                    for ($i = $tahun_sekarang; $i <= $tahun_kedepan; $i++) {
                                        for ($y = 1; $y <= count($bulan); $y++) {
                                            if ($y <= $bulan_sekarang && $i == $tahun_sekarang) {
                                                $kalender[$i][$y] = ['status' => 'Belum Dibayar'];
                                            } else {
                                                $kalender[$i][$y] = ['status' => 'Kosong'];
                                            }
                                        }
                                    }
                                    
                                    foreach ($data_iuran as $iuran) {
                                        foreach ($iuran->detailiuran as $detail_iuran) {
                                            $bulan_transaksi = \Carbon\Carbon::parse($detail_iuran->periode_iuran_bulan)->isoFormat('M');
                                            $tahun_transaksi = \Carbon\Carbon::parse($detail_iuran->periode_iuran_bulan)->isoFormat('Y');
                                            if ($tahun_transaksi >= $tahun_sekarang) {
                                                $kalender[$tahun_transaksi][$bulan_transaksi] = ['status' => $iuran->status_pembayaran];
                                            }
                                        }
                                    }
                                    
                                    ksort($kalender);
                                @endphp

                                <thead class="text-center">
                                    @foreach ($kalender as $key => $data)
                                        @for ($i = 0; $i < count($kalender[$key]); $i++)
                                            <th>{{ $bulan[$i] }} {{ $key }}</th>
                                        @endfor
                                    @endforeach
                                </thead>
                                <tbody>
                                    @foreach ($kalender as $key => $data)
                                        @for ($i = 1; $i <= count($kalender[$key]); $i++)
                                            @if ($kalender[$key][$i]['status'] == 'Pembayaran Sukses')
                                                @php
                                                    $count_transaksi_sudah_terbayar += 1;
                                                    $tranasksi_bulanan_sudah_terbayar = $count_transaksi_sudah_terbayar;
                                                @endphp
                                                <td class="table-success text-center">
                                                    <i class="far fa-check-circle size-icn"></i>
                                                </td>
                                            @elseif($kalender[$key][$i]['status'] == 'Menunggu Konfirmasi')
                                                <td class="table-warning text-center">
                                                    <i class="far fa-check-circle size-icn"></i>
                                                </td>
                                            @elseif ($kalender[$key][$i]['status'] == 'Belum Dibayar' || $kalender[$key][$i]['status'] == 'Pembayaran Gagal')
                                                @php
                                                    $count_transaksi_belum_terbayar += 1;
                                                    $tranasksi_bulanan_belum_terbayar = $count_transaksi_belum_terbayar;
                                                    
                                                    array_push($data_iuran_belum_terbayar, ['Tahun' => $key, 'Bulan' => $i]);
                                                @endphp
                                                <td class="table-danger text-center">
                                                    <i class="far fa-times-circle size-icn"></i>
                                                </td>
                                            @else
                                                <td class="table-secondary text-center"></td>
                                            @endif
                                        @endfor
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Transaksi Iuran Terbaru</h4>
                                        <div class="card-header-action">
                                            <a href="{{ route('transaksi_iuran.index') }}" class="btn btn-primary">Lihat
                                                lebih lanjut <i class="fas fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <div style="overflow: hidden;">
                                            <table class="table table-striped" id="datatables-transaksi-iuran">
                                                <thead>
                                                    <tr>
                                                        <th>Tanggal Transaksi</th>
                                                        <th>Periode</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data_iuran as $data)
                                                        <tr>
                                                            <td><a
                                                                    href="{{ route('transaksi_iuran.show', $data->id_transaksi_iuran) }}">{{ \Carbon\Carbon::parse($data->tanggal_transaksi)->isoFormat('dddd, D MMMM Y') }}</a>
                                                            </td>
                                                            <td>{{ $data->periode_pembayaran }} Bulan</td>
                                                            @php
                                                                if ($data->status_pembayaran == 'Menunggu Konfirmasi') {
                                                                    $kondisi = 'badge-warning';
                                                                } elseif ($data->status_pembayaran == 'Pembayaran Sukses') {
                                                                    $kondisi = 'badge-success';
                                                                } elseif ($data->status_pembayaran == 'Pembayaran Gagal') {
                                                                    $kondisi = 'badge-danger';
                                                                }
                                                            @endphp
                                                            <td class="font-weight-600"><span
                                                                    class="badge {{ $kondisi }}">{{ $data->status_pembayaran }}</span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="text-primary">Daftar Iuran Belum Terbayar | Tahun:
                                            {{ $tahun_sekarang }}</h4>
                                    </div>
                                    <div class="card-body pt-0">
                                        <div style="overflow: hidden;">
                                            <table class="table table-striped" id="datatables-iuran-belum-dibayar">
                                                <thead>
                                                    <tr>
                                                        <th>Iuran Bulan</th>
                                                        <th>Biaya</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (!$data_iuran->isEmpty() || !empty(session()->get('auth_data.0.data_perumahan')))
                                                        @foreach ($data_iuran_belum_terbayar as $data_iuran)
                                                            <tr class="text-center">
                                                                <th>{{ $bulan[$data_iuran['Bulan'] - 1] }}</th>
                                                                <td>Rp. {{ $biaya_iuran }}</td>
                                                                <td class=" font-weight-600">
                                                                    <a href="{{ route('transaksi_iuran_bulan', ['bulan' => $data_iuran['Bulan'],'tahun' => $data_iuran['Tahun'],'id_perumahan' => $id_perumahan]) }}"
                                                                        class="btn btn-sm btn-danger">Bayar Iuran</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection


@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="assets/modules/chart.min.js"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script> --}}
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            $("#datatables-transaksi-iuran").dataTable({
                responsive: true,
                lengthChange: false,
                pageLength: 4,
                paging: false,
                ordering: false,
                info: false,
                filter: true,
                length: false,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });

            $("#datatables-iuran-belum-dibayar").dataTable({
                responsive: true,
                lengthChange: false,
                pageLength: 4,
                paging: true,
                ordering: false,
                info: false,
                filter: true,
                length: false,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });

            const mouseWheel = document.querySelector('.table-scroll');

            mouseWheel.addEventListener('wheel', function(e) {
                const race = 20;
                if (e.deltaY > 0)
                    mouseWheel.scrollLeft += race;
                else
                    mouseWheel.scrollLeft -= race;
                e.preventDefault();
            });

            document.getElementById("iuran_bayar").innerHTML = "{{ $tranasksi_bulanan_sudah_terbayar }}";
            document.getElementById("iuran_belum_bayar").innerHTML = "{{ $tranasksi_bulanan_belum_terbayar }}";

            $('.konfirmasi-pembayaran').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pembayaran Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: ["Batal", "Ya, Konfirmasi Pembayaran!"],
                    dangerMode: true,
                }).then(function(result) {
                    if (result.value) {
                        window.location.href = url;
                    }
                });
            });
        });
    </script>
@endpush
