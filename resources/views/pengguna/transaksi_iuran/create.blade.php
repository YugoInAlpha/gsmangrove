@extends("layouts.app")

@section('title', 'Transaksi Iuran')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Transaksi Iuran Green Semanggi Mangrove
    </p>
    <form id="form_transaksi_iuran" action="{{ route('transaksi_iuran.store') }}" enctype="multipart/form-data"
        method="POST" novalidate>
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>NIK Pembayar</label>
                                <input type="text" class="form-control" name="nik" value="{{ $warga->nik }}" readonly>
                                <input type="hidden" class="form-control" name="id_warga" value="{{ $warga->id_warga }}"
                                    readonly>
                            </div>
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>Nama Pembayar</label>
                                <input type="text" class="form-control" value="{{ $warga->nama_lengkap }}" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>Perumahan</label>
                                <select name="id_perumahan" id="perumahan"
                                    class="form-control id_perumahan select2 @error('id_perumahan') is-invalid @enderror">
                                    <option value="">Pilih Perumahan</option>
                                    @foreach ($perumahan->perumahan as $data_perumahan)
                                        <option value="{{ $data_perumahan->id_perumahan }}"
                                            data-id_cluster="{{ $data_perumahan->cluster->id_cluster }}"
                                            data-nama_cluster="{{ $data_perumahan->cluster->nama_cluster }}"
                                            data-nama_perumahan="{{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}"
                                            data-kepemilikan_rumah="{{ $data_perumahan->kepemilikan_rumah }}"
                                            data-biaya_iuran="{{ $data_perumahan->cluster->biaya_iuran }}"
                                            data-biaya_iuran_belum_ditempati="{{ $data_perumahan->cluster->biaya_iuran_tidak_ditempati }}"
                                            @if (old('id_perumahan') == $data_perumahan->id_perumahan) selected @endif>
                                            {{ $data_perumahan->cluster->nama_cluster }}
                                            {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('id_perumahan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>Kepemilikan Rumah</label>
                                <input type="text" class="form-control" id="kepemilikan_rumah" name="kepemilikan_rumah"
                                    value="" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>Periode Pembayaran</label>
                                <select name="periode_pembayaran"
                                    class="form-control periode_pembayaran select2 @error('periode_pembayaran') is-invalid @enderror">
                                    <option value="">Pilih Periode Pembayaran</option>
                                    <option value="1" @if (old('periode_pembayaran') === '1') selected @endif>1 Bulan</option>
                                    <option value="2" @if (old('periode_pembayaran') === '2') selected @endif>2 Bulan</option>
                                    <option value="3" @if (old('periode_pembayaran') === '3') selected @endif>3 Bulan</option>
                                    <option value="4" @if (old('periode_pembayaran') === '4') selected @endif>4 Bulan</option>
                                    <option value="5" @if (old('periode_pembayaran') === '5') selected @endif>5 Bulan</option>
                                    <option value="6" @if (old('periode_pembayaran') === '6') selected @endif>6 Bulan</option>
                                    <option value="7" @if (old('periode_pembayaran') === '7') selected @endif>7 Bulan</option>
                                    <option value="8" @if (old('periode_pembayaran') === '8') selected @endif>8 Bulan</option>
                                    <option value="9" @if (old('periode_pembayaran') === '9') selected @endif>9 Bulan</option>
                                    <option value="10" @if (old('periode_pembayaran') === '10') selected @endif>10 Bulan</option>
                                    <option value="11" @if (old('periode_pembayaran') === '11') selected @endif>11 Bulan</option>
                                    <option value="12" @if (old('periode_pembayaran') === '12') selected @endif>12 Bulan</option>
                                </select>
                                @error('periode_pembayaran')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>Tanggal Mulai</label>
                                <input type="month" id="tanggal_mulai" name="tanggal_mulai"
                                    class="form-control @error('tanggal_mulai') is-invalid @enderror"
                                    value="{{ date('Y-m', strtotime(\Carbon\Carbon::now())) }}">
                                @error('tanggal_mulai')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Periode Pembayaran (tanggal mulai - tanggal periode)</label>
                            <input type="text" class="form-control" id="tanggal" readonly>
                        </div>
                        <div class="form-group">
                            <label>Biaya Iuran Cluster</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <strong>Rp.</strong>
                                    </div>
                                </div>
                                <input type="text" name="biaya_iuran" id="biaya_iuran"
                                    class="form-control @error('biaya_iuran') is-invalid @enderror" value="0" readonly>
                                @error('biaya_iuran')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Total Biaya Iuran</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div
                                        class="input-group-text @error('total_biaya_iuran') border border-danger @enderror">
                                        <strong>Rp.</strong>
                                    </div>
                                </div>
                                <input type="text" name="total_biaya_iuran" id="total_biaya_iuran"
                                    class="form-control @error('total_biaya_iuran') is-invalid @enderror" value="0"
                                    readonly>
                                @error('total_biaya_iuran')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>Rekening Iuran</label>
                                <select name="id_metode_pembayaran"
                                    class="form-control id_metode_pembayaran select2 @error('id_metode_pembayaran') is-invalid @enderror">
                                </select>
                                @error('id_metode_pembayaran')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12 col-lg-6">
                                <label>No Rekening (atas nama: <strong id="atas_nama"></strong>)</label>
                                <input type="text" class="form-control" id="input-no-rekening" readonly>
                            </div>
                        </div>

                        <div class="alert alert-info alert-has-icon">
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">informasi</div>
                                Silahkan melakukan pembayaran sebesar <strong id="tunai"></strong> ke nomor rekening <strong
                                    id="nama_rekening"></strong>&nbsp;<strong id="nomor_rekening"></strong> dan silahkan
                                upload bukti pembayaran di bawah ini
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Dokumen Bukti Pembayaran</label>
                            <div class="custom-file">
                                <input type="file"
                                    class="custom-file-input @error('file_bukti_pembayaran') is-invalid @enderror"
                                    accept="application/pdf,image/*" id="file_bukti_pembayaran"
                                    name="file_bukti_pembayaran">
                                <label class="custom-file-label" for="customFile">Pilih File</label>
                                <div class="invalid-feedback">
                                    @error('file_bukti_pembayaran')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Catatan</label>

                            <textarea style="height: auto !important;" rows="2" name=" catatan" rows="100"
                                class="form-control @error('catatan') is-invalid @enderror">{{ old('catatan') }}</textarea>
                            <div class="invalid-feedback">
                                @error('catatan')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="card-footer text-left">
                        <a href="#" data-toggle="modal" data-target=".modal-konfirmasi-iuran" class="btn btn-primary">Bayar
                            Iuran</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('modal')
    <div class="modal fade modal-konfirmasi-iuran" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Konfirmasi Pembayaran Iuran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <tr>
                                <th>NIK</th>
                                <td>{{ $warga->nik }}</td>
                            </tr>
                            <tr>
                                <th>Nama Pembayar</th>
                                <td>{{ $warga->nama_lengkap }}</td>
                            </tr>
                            <tr>
                                <th>Iuran Perumahan</th>
                                <td>
                                    <div id="iuran_perumahan"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Periode Pembayaran</th>
                                <td>
                                    <div id="periode"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Transfer ke Rekening</th>
                                <td>
                                    <div id="transfer"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Total Biaya Iuran</th>
                                <td>
                                    <div id="total_iuran"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button form="form_transaksi_iuran" type="submit" class="btn right btn-primary">Bayar Iuran</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-ui/jquery-ui.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            var year = new Date().getFullYear();
            document.getElementById('tanggal_mulai').setAttribute("min", year + "-01-01");
            document.getElementById('tanggal_mulai').setAttribute("max", year + "-12-31");

            document.querySelector('#file_bukti_pembayaran').addEventListener('change', function(e) {
                var fileName = document.getElementById("file_bukti_pembayaran").files[0].name;
                var nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            var atas_nama = $('.id_metode_pembayaran option:selected').filter(':selected').attr('data-nama');
            var no = $('.id_metode_pembayaran option:selected').filter(':selected').attr('data-no');
            var rekening = $('.id_metode_pembayaran option:selected').filter(':selected').attr('data-rekening');
            if (rekening !== '') {
                $("#input-no-rekening").val(no);
                $("#atas_nama").text(atas_nama);

                $("#nama_rekening").text(rekening);
                $("#nomor_rekening").text(no);
                $("#transfer").text("(" + rekening + ")" + " " + no);

            } else {
                $("#input-no-rekening").val('');
                $("#atas_nama").text('');

                $("#nama_rekening").text('');
                $("#nomor_rekening").text('');
                $("#transfer").text('');
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            var id_cluster = $('#perumahan option:selected').filter(':selected').attr('data-id_cluster');
            var kepemilikan_rumah = $('#perumahan option:selected').filter(':selected').attr(
                'data-kepemilikan_rumah');
            var biaya_iuran_input = $('#perumahan option:selected').filter(':selected').attr('data-biaya_iuran');
            var biaya_iuran_tidak_ditempati = $('#perumahan option:selected').filter(':selected').attr(
                'data-biaya_iuran_belum_ditempati');
            var nama_cluster = $('#perumahan option:selected').filter(':selected').attr('data-nama_cluster');
            var nama_perumahan = $('#perumahan option:selected').filter(':selected').attr('data-nama_perumahan');
            $("#kepemilikan_rumah").val(kepemilikan_rumah);
            $("#iuran_perumahan").text(nama_cluster + " " + nama_perumahan);

            if (kepemilikan_rumah == "Tidak Ditempati") {
                $('#biaya_iuran').val(biaya_iuran_tidak_ditempati);
            } else {
                $('#biaya_iuran').val(biaya_iuran_input);
            }

            $.ajax({
                url: '/rekening-dropdown',
                method: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'id_cluster': id_cluster
                },
                success: function(response) {
                    $('.id_metode_pembayaran').empty();
                    $('.id_metode_pembayaran').append("<option value=''>Pilih Rekening</option>");
                    console.log(response)
                    $.each(response, function(index, value) {
                        $('.id_metode_pembayaran').append("<option value=" + value
                            .id_metode_pembayaran + " data-rekening='" + value
                            .nama_rekening + "' data-nama='" + value.rekening_atas_nama +
                            "' data-no='" + value.nomor_rekening + "'>" + value
                            .nama_rekening + " " + value.rekening_atas_nama + "</option>");
                    });
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var periode_pembayaran = $('.periode_pembayaran option:selected').filter(':selected').val();
            if (periode_pembayaran !== '') {
                var periode = periode_pembayaran - 1
            } else {
                var periode = null;
            }
            var tanggal_mulai = $("#tanggal_mulai").val();
            const tanggal = new Date(tanggal_mulai);

            var opsi = {
                month: 'long',
                year: 'numeric',
            };
            const generate_bulan = new Date(tanggal.getFullYear(), tanggal.getMonth() + parseInt(periode));

            var bulan_sekarang = tanggal.toLocaleDateString("id", opsi);
            var bulan_kedepan = generate_bulan.toLocaleDateString("id", opsi);
            $("#tanggal").val(bulan_sekarang + " - " + bulan_kedepan);

            var biaya_iuran = $('#biaya_iuran').val();
            var total_biaya_iuran = biaya_iuran * parseInt(periode_pembayaran);

            if (!isNaN(total_biaya_iuran)) {
                $("#total_biaya_iuran").val(total_biaya_iuran);
                $("#total_iuran").text("Rp " + total_biaya_iuran);
                $("#tunai").text("Rp. " + total_biaya_iuran);
            } else {
                $("#total_biaya_iuran").val('');
                $("#total_iuran").text('');
                $("#tunai").text('');
            }

            if (periode_pembayaran === "") {
                $("#periode").text('');
            } else {
                $("#periode").text(periode_pembayaran + " Bulan");
            }
        });
    </script>
    <script>
        $('.id_metode_pembayaran').change(function() {
            var atas_nama = $(this).find(':selected').attr('data-nama');
            var no = $(this).find(':selected').attr('data-no');
            var rekening = $(this).find(':selected').attr('data-rekening');
            if (rekening !== '') {
                $("#input-no-rekening").val(no);
                $("#atas_nama").text(atas_nama);

                $("#nama_rekening").text(rekening);
                $("#nomor_rekening").text(no);
                $("#transfer").text("(" + rekening + ")" + " " + no);

            } else {
                $("#input-no-rekening").val('');
                $("#atas_nama").text('');

                $("#nama_rekening").text('');
                $("#nomor_rekening").text('');
                $("#transfer").text('');
            }
        });

        $('.periode_pembayaran').change(function() {
            var periode_pembayaran = $(this).val();
            if (periode_pembayaran !== '') {
                var periode = periode_pembayaran - 1
            } else {
                var periode = null;
            }
            var tanggal_mulai = $("#tanggal_mulai").val();
            const tanggal = new Date(tanggal_mulai);

            var opsi = {
                month: 'long',
                year: 'numeric',
            };
            const generate_bulan = new Date(tanggal.getFullYear(), tanggal.getMonth() + parseInt(periode));

            var bulan_sekarang = tanggal.toLocaleDateString("id", opsi);
            var bulan_kedepan = generate_bulan.toLocaleDateString("id", opsi);
            $("#tanggal").val(bulan_sekarang + " - " + bulan_kedepan);

            var biaya_iuran = $('#biaya_iuran').val();
            var total_biaya_iuran = biaya_iuran * parseInt(periode_pembayaran);
            if (!isNaN(total_biaya_iuran)) {
                $("#total_biaya_iuran").val(total_biaya_iuran);
                $("#total_iuran").text("Rp. " + total_biaya_iuran);
                $("#tunai").text("Rp. " + total_biaya_iuran);
            } else {
                $("#total_biaya_iuran").val('');
                $("#total_iuran").text('');
                $("#tunai").text('');
            }

            if (periode_pembayaran === "") {
                $("#periode").text('');
            } else {
                $("#periode").text(periode_pembayaran + " Bulan");
            }
        });

        $('#tanggal_mulai').change(function() {
            var periode_pembayaran = $('.periode_pembayaran option:selected').filter(':selected').val();
            var tanggal_mulai = $("#tanggal_mulai").val();
            const tanggal = new Date(tanggal_mulai);

            var opsi = {
                month: 'long',
                year: 'numeric',
            };
            const generate_bulan = new Date(tanggal.getFullYear(), tanggal.getMonth() + parseInt(
                periode_pembayaran - 1));

            var bulan_sekarang = tanggal.toLocaleDateString("id", opsi);
            var bulan_kedepan = generate_bulan.toLocaleDateString("id", opsi);
            $("#tanggal").val(bulan_sekarang + " - " + bulan_kedepan);

            var biaya_iuran = $('#biaya_iuran').val();
            var total_biaya_iuran = biaya_iuran * parseInt(periode_pembayaran);
            if (!isNaN(total_biaya_iuran)) {
                $("#total_biaya_iuran").val(total_biaya_iuran);
                $("#total_iuran").text("Rp. " + total_biaya_iuran);
                $("#tunai").text("Rp. " + total_biaya_iuran);
            } else {
                $("#total_biaya_iuran").val('');
                $("#total_iuran").text('');
                $("#tunai").text('');
            }

            if (periode_pembayaran === "") {
                $("#periode").text('');
            } else {
                $("#periode").text(periode_pembayaran + " Bulan");
            }
        });

        $('#perumahan').change(function() {
            var id_cluster = $(this).find(':selected').attr('data-id_cluster');
            var kepemilikan_rumah = $(this).find(':selected').attr('data-kepemilikan_rumah');
            var biaya_iuran = $(this).find(':selected').attr('data-biaya_iuran');
            var biaya_iuran_tidak_ditempati = $(this).find(':selected').attr('data-biaya_iuran_belum_ditempati');
            var nama_cluster = $(this).find(':selected').attr('data-nama_cluster');
            var nama_perumahan = $(this).find(':selected').attr('data-nama_perumahan');
            $("#kepemilikan_rumah").val(kepemilikan_rumah);
            $("#iuran_perumahan").text(nama_cluster + " " + nama_perumahan);

            var periode_pembayaran = $('.periode_pembayaran option:selected').filter(':selected').val();

            if (kepemilikan_rumah == "Tidak Ditempati") {
                $('#biaya_iuran').val(biaya_iuran_tidak_ditempati);
                var biaya_iuran = $('#biaya_iuran').val();
                var total_biaya_iuran = biaya_iuran * parseInt(periode_pembayaran);
                if (!isNaN(total_biaya_iuran)) {
                    $("#total_biaya_iuran").val(total_biaya_iuran);
                    $("#total_iuran").text("Rp. " + total_biaya_iuran);
                    $("#tunai").text("Rp. " + total_biaya_iuran);
                } else {
                    $("#total_biaya_iuran").val('');
                    $("#total_iuran").text('');
                    $("#tunai").text('');
                }
            } else {
                $('#biaya_iuran').val(biaya_iuran);
                var biaya_iuran = $('#biaya_iuran').val();
                var total_biaya_iuran = biaya_iuran * parseInt(periode_pembayaran);
                if (!isNaN(total_biaya_iuran)) {
                    $("#total_biaya_iuran").val(total_biaya_iuran);
                    $("#total_iuran").text("Rp. " + total_biaya_iuran);
                    $("#tunai").text("Rp. " + total_biaya_iuran);
                } else {
                    $("#total_biaya_iuran").val('');
                    $("#total_iuran").text('');
                    $("#tunai").text('');
                }
            }

            $.ajax({
                url: '/rekening-dropdown',
                method: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'id_cluster': id_cluster
                },
                success: function(response) {
                    $('.id_metode_pembayaran').empty();
                    $('.id_metode_pembayaran').append("<option value=''>Pilih Rekening</option>");
                    $.each(response, function(index, value) {
                        $('.id_metode_pembayaran').append("<option value=" + value
                            .id_metode_pembayaran + " data-rekening='" + value
                            .nama_rekening + "' data-nama='" + value.rekening_atas_nama +
                            "' data-no='" + value.nomor_rekening + "'>" + value
                            .nama_rekening + " " + value.rekening_atas_nama + "</option>");
                    });
                }
            });
        });
    </script>
@endpush
