@extends("layouts.app")

@section('title', 'Daftar Transaksi Iuran')
@section('content')
<h2 class="section-title">@yield("title")</h2>
<p class="section-lead">
    Halaman Manajemen Iuran Green Semanggi Mangrove
</p>
<div class="card">
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-2">
                <a href="{{ route('transaksi_iuran.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                    <i class="fas fa-money-bill"></i> Bayar Iuran
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div style="overflow: hidden;">
                    <table class="table table-striped" id="datatables">
                        <thead>
                            <tr>
                                <th>Tanggal Transaksi</th>
                                <th>Periode Pembayaran</th>
                                <th>Iuran Perumahan</th>
                                <th>Total Bayar</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transaksi_iuran as $transaksi)
                            <tr>
                                <td>{{ date('d F Y', strtotime($transaksi->tanggal_transaksi)) }}
                                </td>
                                <td>{{ $transaksi->periode_pembayaran }} Bulan</td>
                                <td>{{ $transaksi->perumahan->cluster->nama_cluster }}
                                    {{ $transaksi->perumahan->blok_perumahan }}-{{ $transaksi->perumahan->nama_perumahan }}
                                </td>
                                <td>Rp. {{ $transaksi->total_iuran }}</td>
                                @php
                                if ($transaksi->status_pembayaran == 'Menunggu Konfirmasi') {
                                $kondisi = 'badge-warning';
                                } elseif ($transaksi->status_pembayaran == 'Pembayaran Sukses') {
                                $kondisi = 'badge-success';
                                } elseif ($transaksi->status_pembayaran == 'Pembayaran Gagal') {
                                $kondisi = 'badge-danger';
                                }
                                @endphp
                                <td>
                                    <span class="badge {{ $kondisi }}">{{ $transaksi->status_pembayaran }}</span>
                                </td>
                                <td>
                                    <a href="{{ route('transaksi_iuran.show', $transaksi->id_transaksi_iuran) }}" class="btn btn-sm btn-icon btn-info"><i class="far fa-eye hidden-lg"></i>Lihat</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush


@push('js-template')
<script>
    $(document).ready(function() {
        $("#datatables").dataTable({
            responsive: true
            , paging: true
            , ordering: true
            , info: true
            , filter: true
            , length: true
            , processing: true
            , deferRender: true
            , autoWidth: false
            , language: {
                url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
            }
            , responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function(row) {
                            var data = row.data();
                            return 'Data Iuran';
                        }
                    })
                    , renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                }
            }
            , columnDefs: [{
                    targets: -1
                    , visible: true
                }
                , {
                    "width": "10%"
                    , "targets": -1
                }
                , {
                    orderable: false
                    , targets: -1
                }
            , ]
        , });
    });

</script>
@endpush
