@extends("layouts.app")

@section('title', 'Detail Transaksi Iuran')
@section('content')
<h2 class="section-title">Lihat @yield("title")</h2>
<p class="section-lead">
    Halaman Manajemen Iuran Green Semanggi Mangrove
</p>
<div class="card author-box card-primary">
    <div class="card-body">
        <div class="author-box-description">
            <div class="row">
                <div class="col col-sm-12 col-lg-6 col-md-6 mt-2">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive-sm">
                                <table class="table table-striped table-sm">
                                    <tr>
                                        <th>Tanggal Transaksi</th>
                                        <td>{{ \Carbon\Carbon::parse($transaksi_iuran->tanggal_transaksi)->isoFormat('dddd, D MMMM Y') }}</td>
                                        <th class="w-25 d-none d-sm-block"></th>
                                    </tr>
                                    <tr>
                                        <th>Iuran Perumahan</th>
                                        <td>{{ $transaksi_iuran->perumahan->cluster->nama_cluster }} {{ $transaksi_iuran->perumahan->blok_perumahan }}-{{ $transaksi_iuran->perumahan->nama_perumahan }}</td>
                                        <th class="w-25 d-none d-sm-block"></th>
                                    </tr>
                                    <tr>
                                        <th>Periode</th>
                                        <td>{{ $transaksi_iuran->periode_pembayaran }} Bulan</td>
                                        <th class="w-25 d-none d-sm-block"></th>
                                    </tr>
                                    <tr>
                                        <th>Catatan</th>
                                        <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">{{ $transaksi_iuran->catatan }}</td>
                                        <th class="w-25 d-none d-sm-block"></th>
                                    </tr>
                                    <tr>
                                        @php
                                        if ($transaksi_iuran->status_pembayaran == 'Menunggu Konfirmasi') {
                                        $kondisi = 'badge-warning';
                                        } elseif ($transaksi_iuran->status_pembayaran == 'Pembayaran Sukses') {
                                        $kondisi = 'badge-success';
                                        } elseif ($transaksi_iuran->status_pembayaran == 'Pembayaran Gagal') {
                                        $kondisi = 'badge-danger';
                                        }
                                        @endphp
                                        <th>Status</th>
                                        <td><span class="badge {{ $kondisi }}">{{ $transaksi_iuran->status_pembayaran }}</span></td>
                                        <th class="w-25 d-none d-sm-block"></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">

                            <a href="#" class="btn btn-primary mr-1 ml-1 mb-1" data-toggle="modal" data-target=".modal-file-bukti-pembayaran">File Bukti Pembayaran</a>
                        </div>
                    </div>
                </div>
                <div class="col col-sm-12 col-lg-6 col-md-6 mt-2">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm">
                            <thead class="table-primary">
                                <tr class="text-center">
                                    <th>Pembayaran Iuran Bulan</th>
                                    <th>Tahun</th>
                                    <th>Biaya Iuran</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $total_biaya_iuran = 0;
                                @endphp
                                @foreach ($transaksi_iuran->detailiuran as $detail_transaksi)
                                <tr>
                                    <td>{{ \Carbon\Carbon::parse($detail_transaksi->periode_iuran_bulan)->isoFormat('MMMM') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($detail_transaksi->periode_iuran_bulan)->isoFormat('Y') }}</td>
                                    @php
                                    $total_biaya_iuran += $detail_transaksi->biaya_iuran;
                                    @endphp
                                    <td>Rp. {{ $detail_transaksi->biaya_iuran }}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2"><strong>Total Biaya Iuran</strong></td>
                                    <td><strong>Rp. {{ $total_biaya_iuran }}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade modal-file-bukti-pembayaran" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Bukti Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-bukti">
                <div class="embed-responsive embed-responsive-16by9">
                    <object data="{{ asset('storage/assets/pdf/dokumen_bukti_pembayaran/' . $transaksi_iuran->file_bukti_transaksi) }}" type="application/pdf"></object>
                </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endsection


@push('file-css-library')

@endpush

@push('file-css')

@endpush

@push('file-js-library')
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('file-js')
<script>
    $(document).ready(function() {
        $(".modal-file-bukti-pembayaran").on("hidden.bs.modal", function() {
            $(".modal-bukti").html("");
            $(".modal-bukti").html("<div class='embed-responsive embed-responsive-4by3'><object data='{{ asset('storage/assets/pdf/dokumen_bukti_pembayaran/' . $transaksi_iuran->file_bukti_transaksi) }}' type='application/pdf'></object></div>");
        });

        $('.konfirmasi-pembayaran').on('click', function(event) {
            event.preventDefault();
            const url = $(this).attr('href');
            swal({
                title: 'Apakah Anda Yakin Mengkonfirmasi Pembayaran Ini ?'
                , text: 'Anda Tidak Akan Dapat Mengembalikannya!'
                , icon: 'warning'
                , buttons: ["Batal", "Ya, Konfirmasi Pembayaran!"]
                , dangerMode: true
            , }).then(function(result) {
                if (result.value) {
                    window.location.href = url;
                }
            });
        });
    });

</script>
@endpush
