@extends("layouts.app")

@section('title', 'Daftar Anggota Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Anggota Keluarga Green Semanggi Mangrove
    </p>

    <div class="row">
        <div class="col col-sm-12 col-lg-6">
            <div class="card card-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div style="overflow: hidden;">
                                <table class="table table-striped" id="datatables-kk">
                                    <thead>
                                        <tr>
                                            <th>No. KK</th>
                                            <th>File</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $kartu_keluarga->no_kk }}</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="#" class="btn btn-sm btn-icon btn-primary" data-toggle="modal"
                                                        data-target=".modal-file-kk">File KK</a>
                                                    <a href="#"
                                                        class="btn btn-sm btn-icon btn-primary  @if (empty($kartu_keluarga->file_pernikahan)) disabled @endif"
                                                        data-toggle="modal" data-target=".modal-file-pernikahan">File
                                                        Pernikahan</a>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('kartu_keluarga.edit', $kartu_keluarga->id_kartu_keluarga) }}"
                                                    class="btn btn-sm btn-icon icon-left btn-warning">
                                                    <i class="far fa-edit"></i>Ubah
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('anggota_keluarga.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Anggota Keluarga
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables-anggota-warga">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama Lengkap</th>
                                    <th>Status Hubungan Keluarga</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($warga as $data_warga)
                                    <tr>
                                        <td>{{ $data_warga->nik }}</td>
                                        <td>{{ $data_warga->nama_lengkap }}</td>
                                        <td>{{ $data_warga->status_hubungan_keluarga }}</td>
                                        <td>
                                            <a href="{{ route('anggota_keluarga.show', $data_warga->id_warga) }}"
                                                class="btn btn-sm btn-icon btn-info mb-1">
                                                <i class="far fa-eye hidden-lg"></i> Lihat
                                            </a>
                                            <a href="{{ route('anggota_keluarga.edit', $data_warga->id_warga) }}"
                                                class="btn btn-sm btn-icon btn-warning mr-1 ml-1 mb-1">
                                                <i class="far fa-edit"></i> Ubah
                                            </a>
                                            <form style="all: unset"
                                                action="{{ route('anggota_keluarga.destroy', $data_warga->id_warga) }}"
                                                id="form_delete_{{ $data_warga->id_warga }}"
                                                data-id="{{ $data_warga->nik }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button"
                                                    onclick="deleteconfirm('form_delete_{{ $data_warga->id_warga }}')"
                                                    class="btn btn-sm btn-icon btn-danger mb-1"><i
                                                        class="far fa-trash-alt"></i>
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade modal-file-kk" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Kartu Keluarga</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-kk">
                    <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                        <object data="{{ asset('storage/assets/pdf/dokumen_kk/' . $kartu_keluarga->file_kk) }}"
                            type="application/pdf"></object>
                    </div>
                    <div class="alert alert-warning d-block d-sm-none">
                        Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a
                            href="{{ asset('storage/assets/pdf/dokumen_kk/' . $kartu_keluarga->file_kk) }}"
                            download>sini</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-file-pernikahan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Pernikahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-pernikahan">
                    @if (!empty($kartu_keluarga->file_pernikahan))
                        <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                            <object
                                data="{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $kartu_keluarga->file_pernikahan) }}"
                                type="application/pdf"></object>
                        </div>
                        <div class="alert alert-warning d-block d-sm-none">
                            Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a
                                href="{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $kartu_keluarga->file_pernikahan) }}"
                                download>sini</a>
                        </div>
                    @else
                        <div class="alert alert-danger alert-has-icon">
                            <div class="alert-icon"><i class="fas fa-praying-hands"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Mohon Maaf</div>
                                File dokumen ini tidak tersedia
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $(".modal-file-kk").on("hidden.bs.modal", function() {
                $(".modal-kk").html("");
                $(".modal-kk").html(
                    "<div class='embed-responsive embed-responsive-4by3 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_kk/' . $kartu_keluarga->file_kk) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'> Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_kk/' . $kartu_keluarga->file_kk) }}' download>sini</a></div>"
                );
            });

            $(".modal-file-pernikahan").on("hidden.bs.modal", function() {
                $(".modal-pernikahan").html("@if (!empty($kartu_keluarga->file_pernikahan)) <div class='embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $kartu_keluarga->file_pernikahan) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $kartu_keluarga->file_pernikahan) }}' download>sini</a></div>@else<div class='alert alert-danger alert-has-icon'><div class='alert-icon'><i class='fas fa-praying-hands'></i></div><div class='alert-body'><div class='alert-title'>Mohon Maaf</div>File dokumen ini tidak tersedia</div></div>@endif");
            });

            $("#datatables-anggota-warga").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data Anggota Keluarga';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });

            $("#datatables-kk").dataTable({
                responsive: true,
                paging: false,
                ordering: false,
                info: false,
                filter: false,
                length: false,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });
        });

        function deleteconfirm(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
