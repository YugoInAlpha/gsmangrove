@extends("layouts.app")

@section('title', 'Tambah Anggota Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Anggota Keluarga Green Semanggi Mangrove
    </p>
    <form action="{{ route('anggota_keluarga.store') }}" method="POST" enctype="multipart/form-data" novalidate>
        @csrf
        @include("pengguna.anggota_keluarga._form")
    </form>
@endsection
