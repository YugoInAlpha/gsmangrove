@extends("layouts.app")

@section('title', 'Ubah Anggota Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Anggota Keluarga Green Semanggi Mangrove
    </p>
    <form action="{{ route('anggota_keluarga.update', $warga->id_warga) }}" method="POST" enctype="multipart/form-data" novalidate>
        @method("PUT")
        @csrf
        @include("pengguna.anggota_keluarga._form")
    </form>
@endsection
