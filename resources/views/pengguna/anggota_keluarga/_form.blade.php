<div class="card">
    <div class="card-body">
        <div class="form-group">
            <label>No. KK <strong class="text-danger">*</strong></label>
            <input type="text" class="form-control @error('no_kk') is-invalid @enderror" name="no_kk"
                value="{{ old('no_kk', $kartu_keluarga->no_kk) }}" readonly>
            <input type="hidden" class="form-control @error('id_kartu_keluarga') is-invalid @enderror"
                name="id_kartu_keluarga" value="{{ old('id_kartu_keluarga', $kartu_keluarga->id_kartu_keluarga) }}"
                @if ($model_form['model'] == 'edit') readonly @endif>
            @error('id_kartu_keluarga')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="row">
            <div class="form-group col-12 col-md-6 col-lg-6">
                <label>NIK <strong class="text-danger">*</strong></label>
                <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik"
                    value="{{ old('nik', $warga->nik) }}">
                <div class="invalid-feedback">
                    @error('nik')
                        {{ $message }}
                    @enderror
                </div>
            </div>

            <div class="form-group col-12 col-md-6 col-lg-6">
                <label>Nama Lengkap <strong class="text-danger">*</strong></label>
                <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap"
                    value="{{ old('nama_lengkap', $warga->nama_lengkap) }}">
                <div class="invalid-feedback">
                    @error('nama_lengkap')
                        {{ $message }}
                    @enderror
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Alamat Sesuai KTP <strong class="text-danger">*</strong></label>

            <textarea style="height: auto !important;" rows="2" name=" alamat_ktp" rows="100"
                class="form-control @error('alamat_ktp') is-invalid @enderror">{{ old('alamat_ktp', $warga->alamat_ktp) }}</textarea>
            <div class="invalid-feedback">
                @error('alamat_ktp')
                    {{ $message }}
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label class="form-label">Jenis Kelamin <strong class="text-danger">*</strong></label>
            <div class="selectgroup w-100">
                <label class="selectgroup-item @error('jenis_kelamin') border border-danger @enderror">
                    <input type="radio" name="jenis_kelamin" value="Laki-laki" class="selectgroup-input"
                        @if (old('jenis_kelamin', $warga->jenis_kelamin) == 'Laki-laki') checked @enderror>
                    <span class="selectgroup-button">Laki-laki</span>
                </label>
                <label class="selectgroup-item @error('jenis_kelamin') border border-danger @enderror">
                    <input type="radio" name="jenis_kelamin" value="Perempuan" class="selectgroup-input"
                        @if (old('jenis_kelamin', $warga->jenis_kelamin) == 'Perempuan') checked @enderror>
                    <span class="selectgroup-button">Perempuan</span>
                </label>
            </div>
            <div class="invalid-feedback d-block">
                @error('jenis_kelamin')
                    {{ $message }}
                @enderror
            </div>
        </div>

        <div class="row">
            <div id="form_agama" class="form-group col-12">
                <label class="form-label">Agama <strong class="text-danger">*</strong></label>
                @php
                    $agama = ['Buddha', 'Hindu', 'Islam', 'Katolik', 'Konghucu', 'Protestan'];
                @endphp
                <select name="agama" id="agama" class="form-control select2 @error('agama') is-invalid @enderror">
                    <option value="">Pilih</option>
                    @php
                        if ($model_form['model'] == 'edit') {
                            $match_data = false;
                        } else {
                            $match_data = true;
                        }
                    @endphp
                    @foreach ($agama as $data_agama)
                        <option value="{{ $data_agama }}"
                            @if (old('agama', $warga->agama) == $data_agama) selected @php $match_data = true; @endphp @endif>{{ $data_agama }}</option>
                    @endforeach
                    @if ($match_data == false)
                        <option value="Lain-lain" @if (old('agama') == 'Lain-lain' || $match_data == false) selected @endif>Lain-lain
                        </option>
                    @else
                        <option value="Lain-lain" @if (old('agama') == 'Lain-lain') selected @endif>Lain-lain
                        </option>
                    @endif
                </select>
                <div class="invalid-feedback">
                    @error('agama')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group col-12 d-none" id="form_input_lain">
                <label class="form-label">Agama Lain <strong class="text-danger">*</strong></label>
                <input type="text" name="agama_lain" id="agama_lain"
                    class="form-control @error('agama_lain') is-invalid @enderror"
                    value="{{ old('agama_lain', $warga->agama) }}" placeholder="Silahkan Mengisi Data Agama Lain">
                <div class="invalid-feedback">
                    @error('agama_lain')
                        {{ $message }}
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-sm-12">
                <label>Tempat Lahir <strong class="text-danger">*</strong></label>
                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir"
                    value="{{ old('tempat_lahir', $warga->tempat_lahir) }}">
                <div class="invalid-feedback">
                    @error('tempat_lahir')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group col-lg-6 col-sm-12">
                <label>Tanggal Lahir <strong class="text-danger">*</strong></label>
                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror"
                    name="tanggal_lahir" value="{{ old('tanggal_lahir', $warga->tanggal_lahir) }}">
                <div class="invalid-feedback">
                    @error('tanggal_lahir')
                        {{ $message }}
                    @enderror
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="form-label">Golongan Darah <strong class="text-danger">*</strong></label>
            <div class="selectgroup w-100">
                <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                    <input type="radio" name="golongan_darah" value="A" class="selectgroup-input"
                        @if (old('golongan_darah', $warga->golongan_darah) == 'A') checked @enderror>
                    <span class="selectgroup-button">A</span>
                </label>
                <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                    <input type="radio" name="golongan_darah" value="B" class="selectgroup-input"
                        @if (old('golongan_darah', $warga->golongan_darah) == 'B') checked @enderror>
                    <span class="selectgroup-button">B</span>
                </label>
                <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                    <input type="radio" name="golongan_darah" value="AB" class="selectgroup-input"
                        @if (old('golongan_darah', $warga->golongan_darah) == 'AB') checked @enderror>
                    <span class="selectgroup-button">AB</span>
                </label>
                <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                    <input type="radio" name="golongan_darah" value="O" class="selectgroup-input"
                        @if (old('golongan_darah', $warga->golongan_darah) == 'O') checked @enderror>
                    <span class="selectgroup-button">O</span>
                </label>
            </div>
            <div class="invalid-feedback d-block">
                @error('golongan_darah')
                    {{ $message }}
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="form-group col-12 col-md-6 col-lg-6">
                <label>Nomor Telepon (Whatsapp) <strong class="text-danger">*</strong></label>
                <input type="text" class="form-control @error('nomor_telepon') is-invalid @enderror"
                    name="nomor_telepon" value="{{ old('nomor_telepon', $warga->nomor_telepon) }}">
                <div class="invalid-feedback">
                    @error('nomor_telepon')
                        {{ $message }}
                    @enderror
                </div>
            </div>

            <div class="form-group col-12 col-md-6 col-lg-6">
                <label>Pekerjaan <strong class="text-danger">*</strong></label>
                <input type="text" class="form-control @error('pekerjaan') is-invalid @enderror" name="pekerjaan"
                    value="{{ old('pekerjaan', $warga->pekerjaan) }}">
                <div class="invalid-feedback">
                    @error('pekerjaan')
                        {{ $message }}
                    @enderror
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="form-label">Status Perkawinan <strong class="text-danger">*</strong></label>
            <div class="selectgroup w-100">
                <label class="selectgroup-item @error('status_kawin') border border-danger @enderror">
                    <input type="radio" name="status_kawin" value="Menikah" class="selectgroup-input"
                        @if (old('status_kawin', $warga->status_kawin) == 'Menikah') checked @enderror>
                    <span class="selectgroup-button">Menikah</span>
                </label>
                <label class="selectgroup-item @error('status_kawin') border border-danger @enderror">
                    <input type="radio" name="status_kawin" value="Belum Menikah" class="selectgroup-input"
                        @if (old('status_kawin', $warga->status_kawin) == 'Belum Menikah') checked @enderror>
                    <span class="selectgroup-button">Belum Menikah</span>
                </label>
            </div>
            <div class="invalid-feedback d-block">
                @error('status_kawin')
                    {{ $message }}
                @enderror
            </div>
        </div>
        @php
            $status_hubungan_keluarga = ['Kepala Keluarga', 'Suami', 'Istri', 'Anak', 'Menantu', 'Cucu', 'Orang Tua', 'Mertua', 'Famili Lain', 'Pembantu', 'Lainnya'];
        @endphp
        <div class="form-group">
            <label>Status Hubungan Dalam Keluarga <strong class="text-danger">*</strong></label>
            <select name="status_hubungan_keluarga"
                class="form-control select2 @error('status_hubungan_keluarga') is-invalid @enderror">
                <option value="">Pilih</option>
                @foreach ($status_hubungan_keluarga as $data_shk)
                    <option value="{{ $data_shk }}" @if (old('status_hubungan_keluarga', $warga->status_hubungan_keluarga) == $data_shk) selected @endif>
                        {{ $data_shk }}</option>
                @endforeach
            </select>
            @error('status_hubungan_keluarga')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label>KTP (PDF/Gambar) <strong class="text-danger">*</strong></label>
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('file_ktp') is-invalid @enderror"
                    accept="image/*,application/pdf" id="file_ktp" name="file_ktp">
                <label class="custom-file-label" for="customFile">
                    @if (!empty($warga->file_ktp))
                    {{ $warga->file_ktp }} @else Pilih file @endif
                </label>
                <div class="invalid-feedback">
                    @error('file_ktp')
                        {{ $message }}
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-left">
        <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            document.querySelector('#file_ktp').addEventListener('change', function(e) {
                var fileName = document.getElementById("file_ktp").files[0].name;
                var nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
            });
        });
    </script>

    <script>
        $(document).ready(function() {

            var data = $('#agama option:selected').filter(':selected').val();
            if (data === 'Lain-lain') {
                $('#form_input_lain').removeClass('d-none');
                $('#form_agama').addClass('col-md-6 col-lg-6');
            } else {
                $('#form_input_lain').addClass('d-none');
                $('#form_agama').removeClass('col-md-6 col-lg-6');
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#agama').change(function() {
                if ($(this).val() === 'Lain-lain') {
                    $('#form_input_lain').removeClass('d-none');
                    $('#form_agama').addClass('col-md-6 col-lg-6');
                } else {
                    $('#form_input_lain').addClass('d-none');
                    $('#form_agama').removeClass('col-md-6 col-lg-6');
                }
            });
        });
    </script>
@endpush
