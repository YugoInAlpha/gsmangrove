@extends("layouts.app")

@section('title', 'Ubah Anggota Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Anggota Keluarga Green Semanggi Mangrove
    </p>
    <form action="{{ route('kartu_keluarga.update', $kartu_keluarga->id_kartu_keluarga) }}" method="POST"
        enctype="multipart/form-data" novalidate>
        @method("PUT")
        @csrf
        @include("pengguna.anggota_keluarga._form_kk")
    </form>
@endsection
