<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>No. KK <strong class="text-danger">*</strong></label>
                    <input type="text" class="form-control @error('no_kk') is-invalid @enderror" name="no_kk"
                        value="{{ old('no_kk', $kartu_keluarga->no_kk) }}">
                    <input type="hidden" class="form-control @error('id_kartu_keluarga') is-invalid @enderror"
                        name="id_kartu_keluarga"
                        value="{{ old('id_kartu_keluarga', $kartu_keluarga->id_kartu_keluarga) }}"
                        @if ($model_form['model'] == 'edit') readonly @endif>
                    <div class="invalid-feedback">
                        @error('no_kk')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label>Dokumen Kartu Keluarga (PDF/Gambar) <strong class="text-danger">*</strong></label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input @error('file_kk') is-invalid @enderror"
                            accept="image/*,application/pdf" id="file_kk" name="file_kk">
                        <label class="custom-file-label" for="customFile">
                            @if (!empty($kartu_keluarga->file_kk))
                            {{ $kartu_keluarga->file_kk }} @else Pilih file
                            @endif
                        </label>
                        <div class="invalid-feedback">
                            @error('file_kk')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Dokumen Pernikahan (PDF/Gambar)</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input @error('file_pernikahan') is-invalid @enderror"
                            accept="image/*,application/pdf" id="file_pernikahan" name="file_pernikahan">
                        <label class="custom-file-label" for="customFile">
                            @if (!empty($kartu_keluarga->file_pernikahan))
                            {{ $kartu_keluarga->file_pernikahan }} @else Pilih file
                            @endif
                        </label>
                        <div class="invalid-feedback">
                            @error('file_pernikahan')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer text-left">
                <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
            </div>
        </div>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            document.querySelector('#file_kk').addEventListener('change', function(e) {
                var fileName = document.getElementById("file_kk").files[0].name;
                var nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
            });

            document.querySelector('#file_pernikahan').addEventListener('change', function(e) {
                var fileName = document.getElementById("file_pernikahan").files[0].name;
                var nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
            });
        });
    </script>
@endpush
