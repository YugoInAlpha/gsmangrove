<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ config('app.name') }} &mdash; @yield('code')</title>

    @include('layouts.css-assets')
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="page-error">
                    <div class="page-inner">
                        <h1>@yield('code')</h1>
                        <div class="page-description">
                            @yield('message')
                        </div>
                    </div>
                </div>
                <div class="simple-footer mt-5">
                    Copyright &copy; 2021<div class="bullet"></div> <a href="#">GSMangrove</a>
                    <p>
                        Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
                    </p>
                </div>
            </div>
        </section>
    </div>

    @include('layouts.js-assets')
</body>

</html>
