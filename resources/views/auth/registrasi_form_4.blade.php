<form action="{{ route('registrasi.store_4') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Dokumen Kartu Keluarga (PDF/Gambar) <strong class="text-danger">*</strong></label>
        <div class="custom-file">
            <input type="file" class="custom-file-input @error('file_kk') is-invalid @enderror" accept="image/*,application/pdf" id="file_kk" name="file_kk">
            <label class="custom-file-label" for="customFile">Pilih file</label>
            <div class="invalid-feedback">
                @error('file_kk')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>KTP (PDF/Gambar) <strong class="text-danger">*</strong></label>
        <div class="custom-file">
            <input type="file" class="custom-file-input @error('file_ktp') is-invalid @enderror" accept="image/*,application/pdf" id="file_ktp" name="file_ktp">
            <label class="custom-file-label" for="customFile">Pilih file</label>
            <div class="invalid-feedback">
                @error('file_ktp')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Dokumen Pernikahan (PDF/Gambar)</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input @error('file_pernikahan') is-invalid @enderror" accept="image/*,application/pdf" id="file_pernikahan" name="file_pernikahan">
            <label class="custom-file-label" for="customFile">Pilih file</label>
            <div class="invalid-feedback">
                @error('file_pernikahan')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg btn-block">
            Daftar
        </button>
    </div>
</form>

@push('js-template')
    <script>
        document.querySelector('#file_kk').addEventListener('change', function(e) {
            var fileName = document.getElementById("file_kk").files[0].name;
            var nextSibling = e.target.nextElementSibling;
            nextSibling.innerText = fileName;
        })
    </script>

    <script>
        document.querySelector('#file_ktp').addEventListener('change', function(e) {
            var fileName = document.getElementById("file_ktp").files[0].name;
            var nextSibling = e.target.nextElementSibling;
            nextSibling.innerText = fileName;
        })
    </script>

    <script>
        document.querySelector('#file_pernikahan').addEventListener('change', function(e) {
            var fileName = document.getElementById("file_pernikahan").files[0].name;
            var nextSibling = e.target.nextElementSibling;
            nextSibling.innerText = fileName;
        })
    </script>

@endpush
