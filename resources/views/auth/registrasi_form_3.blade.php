<form action="{{ route('registrasi.store_3') }}" method="POST">
    @csrf
    <div class="row">
        <div class="form-group col-12 col-md-6 col-lg-6">
            <label>NIK <strong class="text-danger">*</strong></label>
            <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik', session()->get('data_registrasi.0.nik')) }}">
            <div class="invalid-feedback">
                @error('nik')
                    {{ $message }}
                @enderror
            </div>
        </div>

        <div class="form-group col-12 col-md-6 col-lg-6">
            <label>Nama Kepala Keluarga <strong class="text-danger">*</strong></label>
            <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ old('nama_lengkap', session()->get('data_registrasi.0.nama_lengkap')) }}">
            <div class="invalid-feedback">
                @error('kata_sandi')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Alamat Sesuai KTP <strong class="text-danger">*</strong></label>

        <textarea style="height: auto !important;" rows="2" name=" alamat_ktp" rows="100" class="form-control @error('alamat_ktp') is-invalid @enderror">{{ old('alamat_ktp', session()->get('data_registrasi.0.alamat_ktp')) }}</textarea>
        <div class="invalid-feedback">
            @error('alamat_ktp')
                {{ $message }}
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label class="form-label">Jenis Kelamin <strong class="text-danger">*</strong></label>
        <div class="selectgroup w-100">
            <label class="selectgroup-item @error('jenis_kelamin') border border-danger @enderror">
                <input type="radio" name="jenis_kelamin" value="Laki-laki" class="selectgroup-input" @if (old('jenis_kelamin', session()->get('data_registrasi.0.jenis_kelamin')) == 'Laki-laki') checked @enderror>
                <span class="selectgroup-button">Laki-laki</span>
            </label>
            <label class="selectgroup-item @error('jenis_kelamin') border border-danger @enderror">
                <input type="radio" name="jenis_kelamin" value="Perempuan" class="selectgroup-input" @if (old('jenis_kelamin', session()->get('data_registrasi.0.jenis_kelamin')) == 'Perempuan') checked @enderror>
                <span class="selectgroup-button">Perempuan</span>
            </label>
        </div>
        <div class="invalid-feedback d-block">
            @error('jenis_kelamin')
                {{ $message }}
            @enderror
        </div>
    </div>

    <div class="row">
        <div id="form_agama" class="form-group col-12">
            <label class="form-label">Agama <strong class="text-danger">*</strong></label>
            @php
                $agama = ['Buddha', 'Hindu', 'Islam', 'Katolik', 'Konghucu', 'Protestan'];
            @endphp
            <select name="agama" id="agama" class="form-control select2 @error('agama') is-invalid @enderror">
                <option value="">Pilih</option>
                @foreach ($agama as $data_agama)
                    <option value="{{ $data_agama }}" @if (old('agama', session()->get('data_registrasi.0.agama')) == $data_agama) selected @endif>{{ $data_agama }}</option>
                @endforeach
                <option value="Lain-lain" @if (old('agama', session()->get('data_registrasi.0.agama')) == 'Lain-lain') selected @endif>Lain-lain</option>
            </select>
            <div class="invalid-feedback">
                @error('agama')
                    {{ $message }}
                @enderror
            </div>
        </div>
        <div class="form-group col-12 col-md-6 col-lg-6 d-none" id="form_input_lain">
            <label class="form-label">Agama Lain <strong class="text-danger">*</strong></label>
            <input type="text" name="agama_lain" id="agama_lain" class="form-control @error('agama_lain') is-invalid @enderror" value="{{ old('agama_lain', session()->get('data_registrasi.0.agama_lain')) }}" placeholder="Silahkan Mengisi Data Agama Lain">
            <div class="invalid-feedback">
                @error('agama_lain')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-12 col-md-6 col-lg-6">
            <label>Tempat Lahir <strong class="text-danger">*</strong></label>
            <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ old('tempat_lahir', session()->get('data_registrasi.0.tempat_lahir')) }}">
            <div class="invalid-feedback">
                @error('tempat_lahir')
                    {{ $message }}
                @enderror
            </div>
        </div>
        <div class="form-group col-12 col-md-6 col-lg-6">
            <label>Tanggal Lahir <strong class="text-danger">*</strong></label>
            <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ old('tanggal_lahir', session()->get('data_registrasi.0.tanggal_lahir')) }}">
            <div class="invalid-feedback">
                @error('tanggal_lahir')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="form-label">Golongan Darah <strong class="text-danger">*</strong></label>
        <div class="selectgroup w-100">
            <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                <input type="radio" name="golongan_darah" value="A" class="selectgroup-input" @if (old('golongan_darah', session()->get('data_registrasi.0.golongan_darah')) == 'A') checked @enderror>
                <span class="selectgroup-button">A</span>
            </label>
            <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                <input type="radio" name="golongan_darah" value="B" class="selectgroup-input" @if (old('golongan_darah', session()->get('data_registrasi.0.golongan_darah')) == 'B') checked @enderror>
                <span class="selectgroup-button">B</span>
            </label>
            <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                <input type="radio" name="golongan_darah" value="AB" class="selectgroup-input" @if (old('golongan_darah', session()->get('data_registrasi.0.golongan_darah')) == 'AB') checked @enderror>
                <span class="selectgroup-button">AB</span>
            </label>
            <label class="selectgroup-item @error('golongan_darah') border border-danger @enderror">
                <input type="radio" name="golongan_darah" value="O" class="selectgroup-input" @if (old('golongan_darah', session()->get('data_registrasi.0.golongan_darah')) == 'O') checked @enderror>
                <span class="selectgroup-button">O</span>
            </label>
        </div>
        <div class="invalid-feedback d-block">
            @error('golongan_darah')
                {{ $message }}
            @enderror
        </div>
    </div>

    <div class="row">
        <div class="form-group col-12 col-md-6 col-lg-6">
            <label>Nomor Telepon (Whatsapp) <strong class="text-danger">*</strong></label>
            <input type="text" class="form-control @error('nomor_telepon') is-invalid @enderror" name="nomor_telepon" value="{{ old('nomor_telepon', session()->get('data_registrasi.0.nomor_telepon')) }}">
            <div class="invalid-feedback">
                @error('nomor_telepon')
                    {{ $message }}
                @enderror
            </div>
        </div>

        <div class="form-group col-12 col-md-6 col-lg-6">
            <label>Pekerjaan <strong class="text-danger">*</strong></label>
            <input type="text" class="form-control @error('pekerjaan') is-invalid @enderror" name="pekerjaan" value="{{ old('pekerjaan', session()->get('data_registrasi.0.pekerjaan')) }}">
            <div class="invalid-feedback">
                @error('pekerjaan')
                    {{ $message }}
                @enderror
            </div>
        </div>

    </div>
    <div class="form-group">
        <label class="form-label">Status Perkawinan <strong class="text-danger">*</strong></label>
        <div class="selectgroup w-100">
            <label class="selectgroup-item @error('status_kawin') border border-danger @enderror">
                <input type="radio" name="status_kawin" value="Menikah" class="selectgroup-input" @if (old('status_kawin', session()->get('data_registrasi.0.status_kawin')) == 'Menikah') checked @enderror>
                <span class="selectgroup-button">Menikah</span>
            </label>
            <label class="selectgroup-item @error('status_kawin') border border-danger @enderror">
                <input type="radio" name="status_kawin" value="Belum Menikah" class="selectgroup-input" @if (old('status_kawin', session()->get('data_registrasi.0.status_kawin')) == 'Belum Menikah') checked @enderror>
                <span class="selectgroup-button">Belum Menikah</span>
            </label>
        </div>
        <div class="invalid-feedback d-block">
            @error('status_kawin')
                {{ $message }}
            @enderror
        </div>
    </div>

    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary btn-lg">
            Berikutnya
        </button>
    </div>
</form>

@push('file-js')
    <script>
        $(document).ready(function() {

            var data = $('#agama option:selected').filter(':selected').val();
            if (data === 'Lain-lain') {
                $('#form_input_lain').removeClass('d-none');
                $('#form_agama').addClass('col-md-6 col-lg-6');
            } else {
                $('#form_input_lain').addClass('d-none');
                $('#form_agama').removeClass('col-md-6 col-lg-6');
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#agama').change(function() {
                if ($(this).val() === 'Lain-lain') {
                    $('#form_input_lain').removeClass('d-none');
                    $('#form_agama').addClass('col-md-6 col-lg-6');
                } else {
                    $('#form_input_lain').addClass('d-none');
                    $('#form_agama').removeClass('col-md-6 col-lg-6');
                }
            });
        });
    </script>
@endpush
