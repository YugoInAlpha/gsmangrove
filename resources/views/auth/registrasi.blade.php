@extends("layouts.auth-layout.app")

@section('title', 'Daftar')
@section('class-form', 'col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2')
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>Daftar {{ $title_form }} Dari 4</h4>
        </div>
        <div class="card-body">
            @include("auth.$form_register")
            <div class="text-muted text-center">
                Sudah Memiliki Akun? <a href="{{ route('login') }}">Masuk Sekarang</a>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush
