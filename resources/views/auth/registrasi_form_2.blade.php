<form action="{{ route('registrasi.store_2') }}" method="POST" enctype="multipart/form-data">
    @csrf
    @for ($i = 0; $i < session()->get('data_registrasi.0.jumlah_perumahan'); $i++)
        <div class="row">
            <div class="form-group col-md-6 col-lg-6">
                <label>Rumah {{ $i + 1 }} <strong class="text-danger">*</strong></label>
                <select name="rumah[]" class="form-control select2  @if ($errors->has('rumah.' . $i)) is-invalid  @endif">
                    <option value="">Pilih</option>
                    @foreach ($perumahan as $data_perumahan)
                        <option value="{{ $data_perumahan->id_perumahan }}" @if (old('rumah.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.id_perumahan')) == $data_perumahan->id_perumahan) selected @endif>{{ $data_perumahan->nama_cluster }} {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}</option>
                    @endforeach
                </select>
                <div class="invalid-feedback">
                    @if ($errors->has('rumah.' . $i))
                        {{ $errors->first('rumah.' . $i) }}
                    @endif
                </div>
            </div>
            <div class="form-group col-md-6 col-lg-6">
                <label>Status Kepemilikan Rumah <strong class="text-danger">*</strong></label>
                <select name="kepemilikan_rumah[]" id="kepemilikan_rumah_{{ $i }}" class="form-control select2 @if ($errors->has('kepemilikan_rumah.' . $i)) is-invalid  @endif">
                    <option value="">Pilih</option>
                    <option value="Tidak Ditempati" @if (old('kepemilikan_rumah.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.kepemilikan_rumah')) === 'Tidak Ditempati') selected @endif>Tidak Ditempati</option>
                    <option value="Rumah Sendiri" @if (old('kepemilikan_rumah.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.kepemilikan_rumah')) === 'Rumah Sendiri') selected @endif>Rumah Sendiri</option>
                    <option value="Kontrak/Sewa" @if (old('kepemilikan_rumah.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.kepemilikan_rumah')) === 'Kontrak/Sewa') selected @endif>Kontrak/Sewa</option>
                    <option value="Lain-lain" @if (old('kepemilikan_rumah.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.kepemilikan_rumah')) === 'Lain-lain') selected @endif>Lain-lain</option>
                </select>
                <div class="invalid-feedback">
                    @if ($errors->has('kepemilikan_rumah.' . $i))
                        {{ $errors->first('kepemilikan_rumah.' . $i) }}
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group d-none" id="input_lain_{{ $i }}">
            <label>Status Kepemilikan Rumah (Lain-lain) <strong class="text-danger">*</strong></label>
            <input type="text" name="lain_lain[]" id="lain_lain_{{ $i }}" class="form-control @if ($errors->has('lain_lain.' . $i)) is-invalid  @endif" value="{{ old('lain_lain.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.lain_lain')) }}" placeholder="Silahkan Mengisi Data Kepemilikan Rumah">
            <div class="invalid-feedback">
                @if ($errors->has('lain_lain.' . $i))
                    {{ $errors->first('lain_lain.' . $i) }}
                @endif
            </div>
        </div>
        <div class="d-none" id="input_kontrak_sewa_{{ $i }}">
            <div class="row">
                <div class="form-group col-12 mb-1">
                    <label>Tanggal Mulai Kontrak/Sewa <strong class="text-danger">*</strong></label>
                    <input type="date" class="form-control @if ($errors->has('tanggal_mulai_kontrak_sewa.' . $i)) is-invalid  @endif" name="tanggal_mulai_kontrak_sewa[]" id="tanggal_mulai_kontrak_sewa_{{ $i }}" value="{{ old('tanggal_mulai_kontrak_sewa.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.tanggal_mulai_kontrak_sewa')) }}">
                    <div class="invalid-feedback">
                        @if ($errors->has('tanggal_mulai_kontrak_sewa.' . $i))
                            {{ $errors->first('tanggal_mulai_kontrak_sewa.' . $i) }}
                        @endif
                    </div>
                </div>
                <div class="form-group col col-sm-12 col-md-6 col-lg-6">
                    <input type="number" name="periode_kontrak_sewa[]" id="periode_kontrak_sewa_{{ $i }}" class="form-control @if ($errors->has('periode_kontrak_sewa.' . $i)) is-invalid  @endif" value="{{ old('periode_kontrak_sewa.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.periode_kontrak_sewa')) }}" placeholder="Silahakan Mengisi Periode Kontrak/Sewa">
                    <div class="invalid-feedback">
                        @if ($errors->has('periode_kontrak_sewa.' . $i))
                            {{ $errors->first('periode_kontrak_sewa.' . $i) }}
                        @endif
                    </div>
                </div>
                <div class="form-group col col-sm-12 col-md-6 col-lg-6">
                    <select name="periode_waktu[]" id="periode_waktu_{{ $i }}" class="form-control select2 @if ($errors->has('periode_waktu.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.periode_waktu'))) is-invalid  @endif">
                        <option value="">Pilih</option>
                        <option value="years" @if (old('periode_waktu.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.periode_waktu')) === 'Tahun') selected @endif>Tahun</option>
                        <option value="months" @if (old('periode_waktu.' . $i, session()->get('data_registrasi.0.data_perumahan.' . $i . '.periode_waktu')) === 'Bulan') selected @endif>Bulan</option>
                    </select>
                    <div class="invalid-feedback">
                        @if ($errors->has('periode_waktu.' . $i))
                            {{ $errors->first('periode_waktu.' . $i) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endfor
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary btn-lg">
            Berikutnya
        </button>
    </div>
</form>


@push('js-template')
    @for ($i = 0; $i < session()->get('data_registrasi.0.jumlah_perumahan'); $i++)
        <script>
            $(document).ready(function() {

                var data = $('#kepemilikan_rumah_{{ $i }} option:selected').filter(':selected').val();
                if (data === 'Lain-lain') {
                    $('#input_kontrak_sewa_{{ $i }}').addClass('d-none');
                    $('#input_lain_{{ $i }}').removeClass('d-none');
                } else if (data === "Kontrak/Sewa") {
                    $('#input_lain_{{ $i }}').addClass('d-none');
                    $('#input_kontrak_sewa_{{ $i }}').removeClass('d-none');
                } else {
                    $('#tanggal_mulai_kontrak_sewa_{{ $i }}').val(null);
                    $('#periode_kontrak_sewa_{{ $i }}').val(null);
                    $('#periode_waktu_{{ $i }}').val(null).trigger('change');
                    $('#lain_lain_{{ $i }}').val(null);

                    $('#input_kontrak_sewa_{{ $i }}').addClass('d-none');
                    $('#input_lain_{{ $i }}').addClass('d-none');
                }
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#kepemilikan_rumah_{{ $i }}').change(function() {
                    if ($(this).val() === 'Lain-lain') {
                        $('#input_kontrak_sewa_{{ $i }}').addClass('d-none');
                        $('#input_lain_{{ $i }}').removeClass('d-none');
                    } else if ($(this).val() === 'Kontrak/Sewa') {
                        $('#input_lain_{{ $i }}').addClass('d-none');
                        $('#input_kontrak_sewa_{{ $i }}').removeClass('d-none');
                    } else {
                        $('#tanggal_mulai_kontrak_sewa_{{ $i }}').val(null);
                        $('#periode_kontrak_sewa_{{ $i }}').val(null);
                        $('#periode_waktu_{{ $i }}').val(null).trigger('change');
                        $('#lain_lain_{{ $i }}').val(null);

                        $('#input_kontrak_sewa_{{ $i }}').addClass('d-none');
                        $('#input_lain_{{ $i }}').addClass('d-none');
                    }
                });
            });
        </script>
    @endfor
@endpush
