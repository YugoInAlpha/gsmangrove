<form action="{{ route('registrasi.store_1') }}" method="POST">
    @csrf
    <div class="form-group">
        <label>No. KK <strong class="text-danger">*</strong></label>
        <input type="text" class="form-control @error('no_kk') is-invalid @enderror" name="no_kk" value="{{ old('no_kk', session()->get('data_registrasi.0.jumlah_perumahan')) }}" autofocus>
        <div class="invalid-feedback">
            @error('no_kk')
                {{ $message }}
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label>Nama Pengguna <strong class="text-danger">*</strong></label>
        <input type="text" class="form-control  @error('nama_pengguna') is-invalid @enderror" name="nama_pengguna" value="{{ old('nama_pengguna', session()->get('data_registrasi.0.jumlah_perumahan')) }}">
        <div class="invalid-feedback">
            @error('nama_pengguna')
                {{ $message }}
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="d-block">Kata Sandi <strong class="text-danger">*</strong></label>
        <div class="input-group" id="show_hide_password">
            <div class="input-group-append">
                <div class="input-group-text @error('kata_sandi') border border-danger @enderror">
                    <a href="" tabindex="-1"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                </div>
            </div>
            <input type="password" class="form-control pwstrength @error('kata_sandi') is-invalid @enderror" data-indicator="pwindicator" name="kata_sandi" value="{{ old('kata_sandi', session()->get('data_registrasi.0.kata_sandi')) }}">
            <div class="invalid-feedback">
                @error('kata_sandi')
                    {{ $message }}
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Jumlah unit rumah di GSM <strong class="text-danger">*</strong></label>
        <input type="number" class="form-control  @error('jumlah_perumahan') is-invalid @enderror" name="jumlah_perumahan" value="{{ old('jumlah_perumahan', session()->get('data_registrasi.0.jumlah_perumahan')) }}">
        <div class="invalid-feedback">
            @error('jumlah_perumahan')
                {{ $message }}
            @enderror
        </div>
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary btn-lg">
            Berikutnya
        </button>
    </div>
</form>

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>
@endpush
