@extends("layouts.auth-layout.app")

@section('title', 'Masuk')
@section('class-form', 'col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4')
@section('content')
    @if (\Session::has('pesan'))
        <div class="alert alert-warning alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {!! \Session::get('pesan') !!}
            </div>
        </div>
    @endif
    @if (\Session::has('pesan_register'))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {!! \Session::get('pesan_register') !!}
            </div>
        </div>
    @endif
    <div class="card card-primary">
        <div class="card-header">
            <h4>Masuk</h4>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('login.authentication') }}">
                @csrf
                <div class="form-group">
                    <label>Nama Pengguna</label>
                    <input type="text" class="form-control  @error('nama_pengguna') is-invalid @enderror" name="nama_pengguna" value="{{ old('nama_pengguna') }}" tabindex="1" autofocus>
                    @error('nama_pengguna')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="d-block">
                        <label class="control-label">Kata Sandi</label>
                        <div class="float-right">
                            <a href="#" class="text-small">
                                Lupa Kata Sandi?
                            </a>
                        </div>
                    </div>
                    <div class="input-group" id="show_hide_password">
                        <div class="input-group-append">
                            <div class="input-group-text @error('kata_sandi') border border-danger @enderror">
                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <input type="password" tabindex="2" class="form-control pwstrength @error('kata_sandi') is-invalid @enderror" data-indicator="pwindicator" name="kata_sandi" value="{{ old('kata_sandi') }}">
                        <div class="invalid-feedback">
                            @error('kata_sandi')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-md btn-block" tabindex="3">
                        Masuk
                    </button>
                </div>
            </form>
            <div class="text-muted text-center">
                Belum Memiliki Akun? <a href="{{ route('registrasi.form_1') }}">Buat Sekarang</a>
            </div>
        </div>
    </div>
@endsection

@push('file-css')

@endpush

@push('file-js')
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>
@endpush
