@extends("layouts.app")

@section('title', 'Detail Warga')
@section('content')
    <h2 class="section-title">Lihat @yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Warga Green Semanggi Mangrove
    </p>
    <div class="row">
        @if (Auth::user()->peran != 'Administrator')
            <div class="col col-sm-12 col-lg-6">
                <div class="card author-box card-primary">
                    <div class="card-body">
                        <div>
                            <div class="author-box-name">
                                <a
                                    href="{{ route('anggota_keluarga.show', $warga->nik) }}">{{ $warga->nama_lengkap }}</a>
                            </div>
                            <div class="author-box-job">No KK. {{ $warga->no_kk }}</div>
                            <div class="w-100 d-sm-none"></div>
                        </div>
                        <div class="author-box-description">

                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <tr>
                                        <th>NIK</th>
                                        <td>{{ $warga->nik }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <td>{{ $warga->nama_lengkap }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin</th>
                                        <td>{{ $warga->jenis_kelamin }}</td>
                                    </tr>
                                    <tr>
                                        <th>Agama</th>
                                        <td>{{ $warga->agama }}</td>
                                    </tr>
                                    <tr>
                                        <th>No Telepon</th>
                                        <td>{{ $warga->nomor_telepon }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pekerjaan</th>
                                        <td>{{ $warga->pekerjaan }}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat KTP</th>
                                        <td>{{ $warga->alamat_ktp }}</td>
                                    </tr>
                                    @foreach ($warga->kartukeluarga->perumahan as $key => $data_perumahan)
                                        <tr>
                                            <th>Alamat Perumahan {{ $key + 1 }}</th>
                                            <td>
                                                {{ $data_perumahan->cluster->nama_cluster }},
                                                {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <th>Status Kawin</th>
                                        <td>{{ $warga->status_kawin }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tempat/Tgl. Lahir</th>
                                        <td>{{ $warga->tempat_lahir }},
                                            {{ \Carbon\Carbon::parse($warga->tanggal_lahir)->isoFormat('D MMMM Y') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Gol. Darah</th>
                                        <td>{{ $warga->golongan_darah }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status Hubungan Keluarga</th>
                                        <td>{{ $warga->status_hubungan_keluarga }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col col-sm-12 col-lg-6">
            <div class="card">
                <form action="{{ route('profil.update', $pengguna->id_pengguna) }}" method="POST">
                    @method("PUT")
                    @csrf
                    <div class="card-header">
                        <h4>Edit Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12 col-12">
                                <label>Nama Pengguna</label>
                                <input type="text" class="form-control" value="{{ $pengguna->nama_pengguna }}"
                                    name="nama_pengguna">
                                @error('nama_pengguna')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-12 col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="ubah_password" class="custom-control-input" tabindex="3"
                                        id="ubah_password" @if (old('ubah_password') == 'on') checked @endif>
                                    <label class=" custom-control-label" for="ubah_password"><strong>Ubah</strong> kata
                                        sandi</label>
                                </div>
                                <div class="form-password d-none">
                                    <div class="form-group">
                                        <label class="d-block">Kata Sandi </label>
                                        <div class="input-group" id="show_hide_password">
                                            <div class="input-group-append">
                                                <div
                                                    class="input-group-text @error('kata_sandi') border border-danger @enderror">
                                                    <a href="" tabindex="-1"><i class="fa fa-eye-slash"
                                                            aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <input type="password" id="kata_sandi"
                                                class="form-control pwstrength @error('kata_sandi') is-invalid @enderror"
                                                data-indicator="pwindicator" name="kata_sandi"
                                                value="{{ old('kata_sandi') }}">
                                            <div class="invalid-feedback">
                                                @error('kata_sandi')
                                                    {{ $message }}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Konfirmasi Kata Sandi </label>
                                        <div class="input-group" id="show_hide_password">
                                            <div class="input-group-append">
                                                <div
                                                    class="input-group-text @error('konfirmasi_kata_sandi') border border-danger @enderror">
                                                    <a href="" tabindex="-1"><i class="fa fa-eye-slash"
                                                            aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <input type="password" id="konfirmasi_kata_sandi"
                                                class="form-control pwstrength @error('konfirmasi_kata_sandi') is-invalid @enderror"
                                                data-indicator="pwindicator" name="konfirmasi_kata_sandi"
                                                value="{{ old('konfirmasi_kata_sandi') }}">
                                            <div class="invalid-feedback">
                                                @error('konfirmasi_kata_sandi')
                                                    {{ $message }}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-warning mt-4">Ubah Profil</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js-library')
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('css-template')
    <style>
        table {
            table-layout: auto !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        table .absorbing-column {
            width: 80% !important;
            align-content: left;
        }

    </style>
@endpush

@push('js-template')
    <script>
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("fa-eye-slash");
                $('#show_hide_password i').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("fa-eye-slash");
                $('#show_hide_password i').addClass("fa-eye");
            }
        });

        var ubah_password = $('#ubah_password').prop('checked');
        if (ubah_password) {
            $(".form-password").removeClass('d-none');
        } else {
            $(".form-password").addClass('d-none');
        }

        $('#ubah_password').click(function() {
            if ($(this).prop('checked')) {
                $(".form-password").removeClass('d-none');
            } else {
                $(".form-password").addClass('d-none');
            }
        });
    </script>
@endpush
