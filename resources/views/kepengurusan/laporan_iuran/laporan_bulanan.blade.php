@extends("layouts.app")

@section('title', 'Laporan Transaksi Iuran Bulanan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Laporan Transaksi Iuran Warga Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-header">
            <h4>Data Iuran</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table">
                        <table class="table-bordered text-nowrap" id="datatables" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Perumahan</th>
                                    <th>Nama Penghuni</th>
                                    @for ($y = 0; $y < count($bulan); $y++)
                                        <th class="text-center">
                                            {{ \Carbon\Carbon::parse($bulan[$y])->isoFormat('Y MMMM') }}</th>
                                    @endfor
                                    <th class="noExport">Aksi</th>
                                    <th>Jumlah Iuran</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data_laporan as $key_cluster)
                                    @foreach ($key_cluster as $key_blok)
                                        @foreach ($key_blok as $key_nama_rumah)
                                            @foreach ($key_nama_rumah as $konten)
                                                <tr>
                                                    <td
                                                        class="@if ($konten['nama_warga'] == 'Kosong') table-danger @endif text-center">
                                                        {{ $konten['perumahan'] }}</td>
                                                        <td
                                                        class="@if ($konten['nama_warga'] == 'Kosong' || $konten['id_warga'] == 'Kosong') table-danger @endif text-center">
                                                        @if ($konten['id_warga'] == 'Kosong')
                                                       {{ $konten['nama_warga'] }}
                                                        @else
                                                        <a href="{{ route('warga.show', $konten['id_warga']) }}">{{ $konten['nama_warga'] }}</a>
                                                        @endif
                                                    </td>

                                                    @php
                                                        $total_iuran = 0;

                                                        $iuran_lunas = 0;
                                                        $iuran_belum_konfirmasi = 0;
                                                        $iuran_belum_bayar = 0;
                                                    @endphp
                                                    @foreach ($konten['iuran'] as $key => $transaksi_iuran)
                                                        @if (isset($transaksi_iuran['tanggal_transaksi']))
                                                            @if ($transaksi_iuran['status_iuran'] == 'Pembayaran Sukses')
                                                                @php
                                                                $iuran_lunas += 1;
                                                                @endphp
                                                                <td class="table-success text-center">
                                                                    {{ \Carbon\Carbon::parse($transaksi_iuran['tanggal_transaksi'])->isoFormat('dddd, D MMMM') }}
                                                                </td>
                                                            @elseif($transaksi_iuran['status_iuran'] == 'Menunggu Konfirmasi')
                                                                @php
                                                                $iuran_belum_konfirmasi += 1;
                                                                @endphp
                                                                <td class="table-warning text-center"><i class="far fa-check-circle size-icn"></i></td>
                                                            @elseif($transaksi_iuran['status_iuran'] == 'Belum Dibayar')
                                                                @php
                                                                $iuran_belum_bayar += 1;
                                                                @endphp
                                                                <td class="table-danger text-center"><i class="far fa-times-circle size-icn"></i></td>
                                                            @endif
                                                            @php
                                                                $total_iuran += $transaksi_iuran['biaya_iuran'];
                                                            @endphp
                                                        @else
                                                            @if ($konten['nama_warga'] == 'Kosong')
                                                                <td class="table-danger text-center"></td>
                                                            @elseif($key > Carbon\Carbon::now()->isoFormat("M"))
                                                            <td class="table-secondary text-center"></td>
                                                            @else
                                                                @php
                                                                    $iuran_belum_bayar += 1;
                                                                @endphp
                                                             <td class="table-danger text-center"><i class="far fa-times-circle size-icn"></i></td>
                                                            @endif
                                                        @endif
                                                        @if ($loop->last)
                                                            @if ($konten['nama_warga'] == 'Kosong')
                                                                <td class="table-danger text-center"></td>
                                                                <td class="table-danger text-center"></td>
                                                            @else
                                                                <td>
                                                                    <a href="#" class="btn btn-sm btn-icon icon-left btn-info btn-lihat" data-toggle="modal" data-target=".modal-detail" data-nama_penghuni="{{ $konten['nama_warga'] }}" data-perumahan="{{ $konten['perumahan'] }}" data-iuran_lunas="{{ $iuran_lunas }}" data-iuran_belum_konfirmasi="{{ $iuran_belum_konfirmasi }}" data-iuran_belum_dibayar="{{ $iuran_belum_bayar }}" data-total_iuran="{{ $total_iuran }}">
                                                                        <i class="far fa-eye"></i> Lihat
                                                                    </a>
                                                                </td>
                                                                <td>{{ $total_iuran }}</td>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center">TOTAL</th>
                                    <th></th>
                                    @for ($i = 0; $i < count($bulan) + 2; $i++)
                                        <th></th>
                                    @endfor
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    <div class="modal fade modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Transaksi Iuran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-ktp">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <tr>
                                <th class="w-50">Nama Penghuni</th>
                                <td class="w-50 text-left" id="nama_penghuni">null</td>
                            </tr>
                            <tr>
                                <th>Perumahan</th>
                                <td class="w-50 text-left" id="perumahan">null</td>
                            </tr>
                            <tr class="text-success">
                                <th>Total Iuran Lunas</th>
                                <td class="w-50 text-left" id="iuran_lunas">null</td>
                            </tr>
                            <tr class="text-warning">
                                <th>Total Iuran Belum Dikonfirmasi</th>
                                <td class="w-50 text-left" id="iuran_belum_konfirmasi">null</td>
                            </tr>
                            <tr class="text-danger">
                                <th>Total Iuran Belum Dibayar</th>
                                <td class="w-50 text-left" id="iuran_belum_dibayar">null</td>
                            </tr>
                            <tr class="text-primary">
                                <th>Total Iuran</th>
                                <td class="w-50 text-left" id="total_iuran">null</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('css-template')
    <style>
        table.table-bordered>thead>tr>th {
            border: 1px solid gray;
        }

        table.table-bordered>tbody>tr>td {
            border: 1px solid gray;
        }

    </style>
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables").dataTable({
                dom: "<'row'<'col-sm-12 col-md-12 p-1'B>>" +
                    "<'row'<'col-sm-12 col-md-6'l>" + "<'col-sm-12 col-md-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i>" + "<'col-sm-12 col-md-7'p>>",
                buttons: [{
                        extend: 'copy',
                        footer: true,
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'csv',
                        footer: true,
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'excel',
                        footer: true,
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                ],
                responsive: false,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: true,
                scrollCollapse: true,
                "lengthMenu": [
                    [20, 50, 100, 200, -1],
                    [20, 50, 100, 200, "All"]
                ],
                "scrollX": true,
                "scrollY": true,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/#|/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(-1)
                        .data()
                        .reduce(function(a, b) {
                            return Number(intVal(a)) + Number(intVal(b));
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(-1, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return Number(intVal(a)) + Number(intVal(b));
                        }, 0);

                    // Update footer
                    $(api.column(-1).footer()).html(
                        pageTotal
                    );
                },
                "drawCallback": function() {
                    this.api().fixedColumns().update();
                },
            });
        });
    </script>

    <script>
        $(".btn-lihat").click(function() {
                var nama_penghuni = $(this).data("nama_penghuni");
                var perumahan = $(this).data("perumahan");
                var iuran_lunas = $(this).data("iuran_lunas");
                var iuran_belum_konfirmasi = $(this).data("iuran_belum_konfirmasi");
                var iuran_belum_dibayar = $(this).data("iuran_belum_dibayar");
                var total_iuran = $(this).data("total_iuran");

                $("#nama_penghuni").text(nama_penghuni);
                $("#perumahan").text(perumahan);
                $("#iuran_lunas").text(iuran_lunas);
                $("#iuran_belum_konfirmasi").text(iuran_belum_konfirmasi);
                $("#iuran_belum_dibayar").text(iuran_belum_dibayar);
                $("#total_iuran").text("Rp. " + total_iuran);
            });
    </script>
@endpush
