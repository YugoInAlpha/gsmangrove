@extends("layouts.app")

@section('title', 'Laporan Transaksi Iuran')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Laporan Transaksi Iuran Warga Green Semanggi Mangrove
    </p>

    <div class="row">
        <div class="col col-sm-12 col-lg-6">
            @php
                $bulan_sekarang = \Carbon\Carbon::now()->isoFormat('MMMM');
                $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            @endphp
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Pelaporan Iuran Bulanan | Tahun: 2021</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('laporan_iuran.laporan_bulanan') }}" method="POST" id="bulanan">
                        @csrf
                        <div class="row">
                            <div class="form-group col">
                                <label>Bulan Pelaporan</label>
                                <select name="bulan" class="form-control select2">
                                    <option value="">Pilih</option>
                                    @foreach ($bulan as $key => $m)
                                        <option value="{{ $loop->iteration }}"
                                            @if ($m == $bulan_sekarang) selected @endif>{{ $m }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label>Cluster</label>
                                <select name="cluster" class="form-control select2">
                                    <option value="semua_cluster">Semua Cluster</option>
                                    @foreach ($cluster as $data_cluster)
                                        <option value="{{ $data_cluster->id_cluster }}">
                                            {{ $data_cluster->nama_cluster }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" form="bulanan" class="btn btn-icon icon-left btn-primary"><i
                                class="fas fa-file-pdf"></i> Cetak Laporan</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col col-sm-12 col-lg-6">
            @php
                $tahun_sekarang = \Carbon\Carbon::now()->isoFormat('Y');
                $tahun = date('Y', strtotime('-2 year', strtotime($tahun_sekarang)));
                $batas_tahun = date('Y', strtotime('+7 year', strtotime($tahun_sekarang)));
            @endphp
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Pelaporan Iuran Tahunan</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('laporan_iuran.laporan_tahunan') }}" method="POST" id="tahunan">
                        @csrf
                        <div class="row">
                            <div class="form-group col">
                                <label>Tahun Pelaporan</label>
                                <select name="tahun" class="form-control @error('tahun') is-invalid @enderror select2">
                                    <option value="">Pilih</option>
                                    @for ($y = $tahun; $y <= $batas_tahun; $y++)
                                        <option value="{{ $y }}"
                                            @if ($y == $tahun_sekarang) selected @endif>{{ $y }}</option>
                                    @endfor
                                </select>
                                <div class="invalid-feedback">
                                    @error('tahun')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group col">
                                <label>Cluster</label>
                                <select name="cluster" class="form-control @error('cluster') is-invalid @enderror select2">
                                    <option value="semua_cluster">Semua Cluster</option>
                                    @foreach ($cluster as $data_cluster)
                                        <option value="{{ $data_cluster->id_cluster }}">
                                            {{ $data_cluster->nama_cluster }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    @error('cluster')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" form="tahunan" class="btn btn-icon icon-left btn-primary"><i
                                class="fas fa-file-pdf"></i> Cetak Laporan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
@endpush


@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endpush
