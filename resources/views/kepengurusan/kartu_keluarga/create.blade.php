@extends("layouts.app")

@section('title', 'Tambah Kartu Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Kartu Keluarga Green Semanggi Mangrove
    </p>
    <form action="{{ route('kartu_keluarga_warga.store') }}" method="POST" enctype="multipart/form-data" novalidate>
        @csrf
        @include("kepengurusan.kartu_keluarga._form")
    </form>
@endsection
