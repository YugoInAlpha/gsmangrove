@extends("layouts.app")

@section('title', 'Tambah Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Perumahan Green Semanggi Mangrove
    </p>
    <form action="{{ route('rumah_warga.store', $id) }}" method="POST" novalidate>
        @csrf
        @include("kepengurusan.kartu_keluarga.rumah._form")
    </form>
@endsection
