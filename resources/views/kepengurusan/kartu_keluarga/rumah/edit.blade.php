@extends("layouts.admin-layout.app")

@section('title', 'Edit Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Edit Data Perumahan Green Semanggi Mangrove
    </p>
    <form action="{{ route('rumah_warga.update', [$id, $id_kk]) }}" method="POST" novalidate>
        @method("PUT")
        @csrf
        @include("admin.kartu_keluarga.rumah._form")
    </form>
@endsection
