<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Perumahan <strong class="text-danger">*</strong></label>
                    @if ($model_form['model'] == 'edit')
                        <input type="text" class="form-control @error('perumahan') is-invalid @enderror" name="perumahan" id="perumahan" value="{{ old('perumahan', $rumah->nama_cluster . ' ' . $rumah->blok_perumahan . '-' . $rumah->nama_perumahan) }}" readonly>
                        <input type="hidden" class="form-control @error('id_perumahan') is-invalid @enderror" name="id_perumahan" id="id_perumahan" value="{{ old('id_perumahan', $rumah->id_perumahan) }}" readonly>
                    @else
                        <select name="id_perumahan" class="form-control select2 @error('id_perumahan') is-invalid @enderror">
                            <option value="">Pilih</option>
                            @foreach ($perumahan as $data_perumahan)
                                <option value="{{ $data_perumahan->id_perumahan }}" @if (old('id_perumahan', $rumah->id_perumahan) === $data_perumahan->id_perumahan) selected @endif>{{ $data_perumahan->nama_cluster }} {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}</option>
                            @endforeach
                        </select>
                    @endif
                    <div class="invalid-feedback">
                        @error('id_perumahan')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                @php
                    if ($model_form['model'] == 'edit') {
                        $kondisi_lain = true;
                    } else {
                        $kondisi_lain = false;
                    }
                @endphp
                <div class="form-group">
                    <label>Status Kepemilikan Rumah <strong class="text-danger">*</strong></label>
                    <select name="kepemilikan_rumah" id="kepemilikan_rumah" class="form-control select2 @error('kepemilikan_rumah') is-invalid @enderror">
                        <option value="">Pilih</option>
                        <option value="Tidak Ditempati" @if (old('kepemilikan_rumah', $rumah->kepemilikan_rumah) === 'Tidak Ditempati') @php $kondisi_lain=false; @endphp selected @endif>Tidak Ditempati</option>
                        <option value="Rumah Sendiri" @if (old('kepemilikan_rumah', $rumah->kepemilikan_rumah) === 'Rumah Sendiri') @php $kondisi_lain=false; @endphp selected @endif>Rumah Sendiri</option>
                        <option value="Kontrak/Sewa" @if (old('kepemilikan_rumah', $rumah->kepemilikan_rumah) === 'Kontrak/Sewa') @php $kondisi_lain=false; @endphp selected @endif>Kontrak/Sewa</option>
                        <option value="Lain-lain" @if (old('kepemilikan_rumah', $rumah->kepemilikan_rumah) === 'Lain-lain')selected @endif @if ($kondisi_lain == true) selected @endif>Lain-lain</option>
                    </select>
                    <div class="invalid-feedback">
                        @error('kepemilikan_rumah')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="form-group d-none" id="input_lain">
                    <label>Status Kepemilikan Rumah (Lain-lain) <strong class="text-danger">*</strong></label>
                    <input type="text" name="lain_lain" id="lain_lain" class="form-control @error('lain_lain') is-invalid @enderror" value="{{ old('lain_lain', $rumah->kepemilikan_rumah) }}" placeholder="Silahkan Mengisi Data Kepemilikan Rumah">
                    <div class="invalid-feedback">
                        @error('lain_lain')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="d-none" id="input_kontrak_sewa">
                    <div class="row">
                        <div class="form-group col-12 mb-1">
                            <label>Tanggal Mulai Kontrak/Sewa <strong class="text-danger">*</strong></label>
                            <input type="date" class="form-control @error('tanggal_mulai_kontrak_sewa') is-invalid @enderror" name="tanggal_mulai_kontrak_sewa" id="tanggal_mulai_kontrak_sewa" value="{{ old('tanggal_mulai_kontrak_sewa', $rumah->tanggal_mulai_kontrak) }}">
                            <div class="invalid-feedback">
                                @error('tanggal_mulai_kontrak_sewa')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col col-sm-12 col-md-6 col-lg-6">
                            <input type="number" name="periode_kontrak_sewa" id="periode_kontrak_sewa" class="form-control @error('periode_kontrak_sewa') is-invalid @enderror" value="{{ old('periode_kontrak_sewa', $rumah->periode_kontrak) }}" placeholder="Silahakan Mengisi Periode Kontrak/Sewa">
                            <div class="invalid-feedback">
                                @error('periode_kontrak_sewa')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col col-sm-12 col-md-6 col-lg-6">
                            <select name="periode_waktu" id="periode_waktu" class="form-control select2 @error('periode_waktu') is-invalid @enderror">
                                <option value="">Pilih</option>
                                <option value="years" @if (old('periode_waktu', $rumah->periode_waktu) === 'years') selected @endif>Tahun</option>
                                <option value="months" @if (old('periode_waktu', $rumah->periode_waktu) === 'months') selected @endif>Bulan</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('periode_waktu')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-left">
                <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
            </div>
        </div>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            var data = $('#kepemilikan_rumah option:selected').filter(':selected').val();
            if (data === 'Lain-lain') {
                $('#input_kontrak_sewa').addClass('d-none');
                $('#input_lain').removeClass('d-none');
            } else if (data === "Kontrak/Sewa") {
                $('#input_lain').addClass('d-none');
                $('#input_kontrak_sewa').removeClass('d-none');
            } else {
                $('#tanggal_mulai_kontrak_sewa').val(null);
                $('#periode_kontrak_sewa').val(null);
                $('#periode_waktu').val(null).trigger('change');
                $('#lain_lain').val(null);

                $('#input_kontrak_sewa').addClass('d-none');
                $('#input_lain').addClass('d-none');
            }

            $('#kepemilikan_rumah').change(function() {
                if ($(this).val() === 'Lain-lain') {
                    $('#input_kontrak_sewa').addClass('d-none');
                    $('#input_lain').removeClass('d-none');
                } else if ($(this).val() === 'Kontrak/Sewa') {
                    $('#input_lain').addClass('d-none');
                    $('#input_kontrak_sewa').removeClass('d-none');
                } else {
                    $('#tanggal_mulai_kontrak_sewa').val(null);
                    $('#periode_kontrak_sewa').val(null);
                    $('#periode_waktu').val(null).trigger('change');
                    $('#lain_lain').val(null);

                    $('#input_kontrak_sewa').addClass('d-none');
                    $('#input_lain').addClass('d-none');
                }
            });
        });
    </script>
@endpush
