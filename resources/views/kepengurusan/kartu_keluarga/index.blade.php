@extends("layouts.app")

@section('title', 'Daftar Kartu Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Kartu Keluarga Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('kartu_keluarga_warga.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Kartu Keluarga
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables-kk">
                            <thead>
                                <tr>
                                    <th>No KK</th>
                                    <th>Jumlah Warga</th>
                                    <th>Jumlah Rumah</th>
                                    <th>Aksi Kartu Keluarga</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($kartu_keluarga as $kk)
                                    <tr>
                                        <td>{{ $kk->no_kk }}</td>
                                        <td>{{ $kk->warga_count }}</td>
                                        <td>{{ $kk->perumahan_count }}</td>
                                        <td>
                                            <a href="{{ route('kartu_keluarga_warga.show', $kk->id_kartu_keluarga) }}" class="btn btn-sm btn-icon btn-info mb-1">
                                                <i class="far fa-eye hidden-lg"></i> Lihat
                                            </a>
                                            <a href="{{ route('kartu_keluarga_warga.edit', $kk->id_kartu_keluarga) }}" class="btn btn-sm btn-icon btn-warning mb-1">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>

                                            <form style="all: unset" action="{{ route('kartu_keluarga_warga.destroy', $kk->id_kartu_keluarga) }}" id="form_delete_{{ $kk->id_kartu_keluarga }}" data-id="{{ $kk->no_kk }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirm('form_delete_{{ $kk->id_kartu_keluarga }}')" class="btn btn-sm btn-icon btn-danger mb-1"><i class="far fa-trash-alt"></i>
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush


@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables-kk").dataTable({
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data kartu Keluarga';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });
        });

        function deleteconfirm(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
