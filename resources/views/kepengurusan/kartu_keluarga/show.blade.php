@extends("layouts.app")

@section('title', 'Detail Kartu Keluarga')
@section('content')
    <h2 class="section-title">@yield("title") || No. KK : {{ $kk->no_kk }}</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Kartu Keluarga Green Semanggi Mangrove
    </p>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Warga</h4>
                    </div>
                    <div class="card-body">
                        {{ $kk->warga_count }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-home"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Rumah</h4>
                    </div>
                    <div class="card-body">
                        {{ $kk->perumahan_count }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="text-primary">Daftar Data Warga</h3>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('warga.create_spesifik', $kk->id_kartu_keluarga) }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Warga
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables-warga">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama Lengkap</th>
                                    <th>Status Hubungan Keluarga</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($kk->warga as $data_warga)
                                    <tr>
                                        <td>{{ $data_warga->nik }}</td>
                                        <td>{{ $data_warga->nama_lengkap }}</td>
                                        <td>{{ $data_warga->status_hubungan_keluarga }}</td>
                                        <td>
                                            <a href="{{ route('warga.show', $data_warga->id_warga) }}" class="btn btn-sm btn-icon btn-info mb-1">
                                                <i class="far fa-eye hidden-lg"></i> Lihat
                                            </a>
                                            <a href="{{ route('warga.edit', $data_warga->id_warga) }}" class="btn btn-sm btn-icon btn-warning mr-1 ml-1 mb-1">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>
                                            <form style="all: unset" action="{{ route('warga.destroy', $data_warga->id_warga) }}" id="form_delete_{{ $data_warga->id_warga }}" data-id="{{ $data_warga->nik }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirm('form_delete_{{ $data_warga->id_warga }}')" class="btn btn-sm btn-icon btn-danger mb-1"><i class="far fa-trash-alt"></i>
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="text-primary">Daftar Data Rumah Warga</h3>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('rumah_warga.create', $kk->id_kartu_keluarga) }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Perumahan
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables-perumahan">
                            <thead>
                                <tr>
                                    <th>Perumahan</th>
                                    <th>Kepemilikan Rumah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($kk))
                                    @foreach ($kk->perumahan as $data)
                                        <tr>
                                            <td>{{ $data->cluster->nama_cluster }} {{ $data->blok_perumahan }}-{{ $data->nama_perumahan }}</td>
                                            <td>{{ $data->kepemilikan_rumah }}</td>
                                            <td>
                                                <div class="d-block"></div>
                                                @if ($data->kepemilikan_rumah == 'Kontrak/Sewa')
                                                    <a href="#" class="btn btn-sm btn-icon icon-left btn-info btn-lihat" data-toggle="modal" data-target=".modal-detail" data-perumahan="{{ $data->cluster->nama_cluster }} {{ $data->nama_perumahan }}" data-kepemilikan_rumah="{{ $data->kepemilikan_rumah }}" data-periode_kontrak="{{ $data->periode_kontrak }}" data-periode_waktu="{{ $data->periode_waktu }}" data-tanggal_mulai_kontrak="{{ $data->tanggal_mulai_kontrak }}" data-tanggal_akhir_kontrak="{{ $data->tanggal_akhir_kontrak }}">
                                                        <i class="far fa-eye"></i> Lihat
                                                    </a>
                                                @else
                                                    <a href="#" class="btn btn-sm btn-icon icon-left btn-info btn-lihat disabled">
                                                        <i class="far fa-eye"></i> Lihat
                                                    </a>
                                                @endif
                                                <a href="{{ route('rumah_warga.edit', [$data->id_perumahan, $kk->id_kartu_keluarga]) }}" class="btn btn-sm btn-icon icon-left btn-warning mr-1">
                                                    <i class="far fa-edit "></i> Ubah
                                                </a>
                                                <form style="all: unset" action="{{ route('rumah_warga.destroy', [$data->id_perumahan, $kk->id_kartu_keluarga]) }}" id="form_delete_perumahan_{{ $data->id_perumahan }}" data-id="{{ $data->cluster->nama_cluster }} {{ $data->blok_perumahan }}-{{ $data->nama_perumahan }}" method="POST">
                                                    @method("DELETE")
                                                    @csrf
                                                    <button type="button" onclick="deleteconfirm('form_delete_perumahan_{{ $data->id_perumahan }}')" class="btn btn-sm btn-icon icon-left btn-danger"><i class="far fa-trash-alt"></i>Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Perumahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-ktp">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <tr>
                                <th class="w-50">Perumahan</th>
                                <td class="w-50 text-left" id="perumahan">null</td>
                            </tr>
                            <tr>
                                <th>Kepemilikan Rumah</th>
                                <td class="w-50 text-left" id="kepemilikan_rumah">null</td>
                            </tr>
                            <tr>
                                <th>Periode Kontrak/Sewa</th>
                                <td class="w-50 text-left" id="periode_kontrak">null</td>
                            </tr>
                            <tr>
                                <th>Tanggal Mulai Kontrak</th>
                                <td class="w-50 text-left" id="tanggal_mulai">null</td>
                            </tr>
                            <tr>
                                <th>Tanggal Akhir Kontrak</th>
                                <td class="w-50 text-left" id="tanggal_akhir">null</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush


@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables-warga").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data Warga';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });


            $("#datatables-perumahan").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true,
                    },
                    {
                        "width": "28%",
                        "targets": -1,
                    },
                    {
                        orderable: false,
                        targets: -1,
                    },
                ],
            });

            $(".btn-lihat").click(function() {
                var perumahan = $(this).data("perumahan");
                var kepemilikan_rumah = $(this).data("kepemilikan_rumah");
                var periode_kontrak = $(this).data("periode_kontrak");
                var periode_waktu = $(this).data("periode_waktu");
                var tanggal_mulai_kontrak = $(this).data("tanggal_mulai_kontrak");
                var tanggal_akhir_kontrak = $(this).data("tanggal_akhir_kontrak");

                if (periode_waktu == "years") {
                    var periode = "Tahun";
                } else {
                    var periode = "Bulan";
                };

                var opsi = {
                    day: 'numeric',
                    weekday: 'long',
                    month: 'long',
                    year: 'numeric',
                };
                const convert_mulai = new Date(tanggal_mulai_kontrak);
                const convert_akhir = new Date(tanggal_akhir_kontrak);

                var mulai = convert_mulai.toLocaleDateString("id", opsi);
                var akhir = convert_akhir.toLocaleDateString("id", opsi);
                $("#perumahan").text(perumahan);
                $("#kepemilikan_rumah").text(kepemilikan_rumah);
                $("#periode_kontrak").text(periode_kontrak + " " + periode);
                $("#tanggal_mulai").text(mulai);
                $("#tanggal_akhir").text(akhir);
            });


        });

        function deleteconfirm(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
