@extends("layouts.app")

@section('title', 'Ubah Kartu Keluarga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Kartu Keluarga Green Semanggi Mangrove
    </p>
    <form action="{{ route('kartu_keluarga_warga.update', $kartu_keluarga->id_kartu_keluarga) }}" method="POST" enctype="multipart/form-data" novalidate>
        @method("PUT")
        @csrf
        @include("kepengurusan.kartu_keluarga._form")
    </form>
@endsection
