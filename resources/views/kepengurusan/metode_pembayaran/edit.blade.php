@extends("layouts.app")

@section('title', 'Ubah Metode Pembayaran')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Metode Pembayaran Green Semanggi Mangrove
    </p>
    <form action="{{ route('metode_pembayaran.update', $metode_pembayaran->id_metode_pembayaran) }}" method="POST" novalidate>
        @method("PUT")
        @csrf
        @include("kepengurusan.metode_pembayaran._form")
    </form>
@endsection
