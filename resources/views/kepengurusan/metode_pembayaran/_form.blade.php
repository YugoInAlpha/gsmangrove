<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Rekening Cluster</label>
                    <select name="id_cluster" class="form-control select2 @error('id_cluster') is-invalid @enderror">
                        <option value="">Pilih</option>
                        @foreach ($cluster as $data_cluster)
                            <option value="{{ $data_cluster->id_cluster }}" @if (old('id_cluster', $metode_pembayaran->id_cluster) == $data_cluster->id_cluster) selected @endif>{{ $data_cluster->nama_cluster }}</option>
                        @endforeach
                    </select>
                    @error('id_cluster')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Nama Rekening</label>
                    <input type="text" class="form-control  @error('nama_rekening') is-invalid @enderror" name="nama_rekening" value="{{ old('nama_rekening', $metode_pembayaran->nama_rekening) }}">
                    <div class="invalid-feedback">
                        @error('nama_rekening')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label>Nomor Rekening</label>
                    <input type="text" class="form-control  @error('nomor_rekening') is-invalid @enderror" name="nomor_rekening" value="{{ old('nomor_rekening', $metode_pembayaran->nomor_rekening) }}">
                    <div class="invalid-feedback">
                        @error('nomor_rekening')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label>Rekening Atas Nama</label>
                    <input type="text" class="form-control  @error('rekening_atas_nama') is-invalid @enderror" name="rekening_atas_nama" value="{{ old('rekening_atas_nama', $metode_pembayaran->rekening_atas_nama) }}">
                    <div class="invalid-feedback">
                        @error('rekening_atas_nama')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                @if ($model_form['model'] == 'edit')
                    <div class="form-group">
                        <label>Status Metode Pembayaran</label>
                        <select name="status_rekening" class="form-control select2 @error('status_rekening') is-invalid @enderror">
                            <option value="">Pilih</option>
                            <option value="Aktif" @if (old('status_rekening', $metode_pembayaran->status_rekening) === 'Aktif') selected @endif>Aktif</option>
                            <option value="Tidak Aktif" @if (old('status_rekening', $metode_pembayaran->status_rekening) === 'Tidak Aktif') selected @endif>Tidak AKtif</option>
                        </select>
                        @error('status_rekening')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="card-footer text-left">
                <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
            </div>
        </div>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endpush
