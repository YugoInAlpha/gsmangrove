@extends("layouts.app")

@section('title', 'Daftar Metode Pembayaran')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Metode Pembayaran Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('metode_pembayaran.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Metode Pembayaran
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables">
                            <thead>
                                <tr>
                                    <th>Rekening</th>
                                    <th>Nomor Rekening</th>
                                    <th>Untuk Cluster</th>
                                    <th>Atas Nama</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($metode_pembayaran as $metode)
                                    <tr>
                                        <td>{{ $metode->nama_rekening }}</td>
                                        <td>{{ $metode->nomor_rekening }}</td>
                                        <td>{{ $metode->cluster->nama_cluster }}</td>
                                        <td>{{ $metode->rekening_atas_nama }}</td>
                                        <td>{{ $metode->status_rekening }}</td>
                                        <td>
                                            <a href="{{ route('metode_pembayaran.edit', $metode->id_metode_pembayaran) }}" class="btn btn-sm btn-icon btn-warning mr-1 ml-1 mb-1">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>
                                            <form style="all: unset" action="{{ route('metode_pembayaran.destroy', $metode->id_metode_pembayaran) }}" id="form_delete_{{ $metode->id_metode_pembayaran }}" data-id="{{ $metode->id_metode_pembayaran }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirm('form_delete_{{ $metode->id_metode_pembayaran }}')" class="btn btn-sm btn-icon btn-danger mr-1 ml-1 mb-1"><i class="far fa-trash-alt"></i>
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data Metode Pembayaran';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "20%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });
        });

        function deleteconfirm(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
