@extends("layouts.app")

@section('title', 'Tambah Metode Pembayaran')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Metode Pembayaran Green Semanggi Mangrove
    </p>
    <form action="{{ route('metode_pembayaran.store') }}" method="POST" novalidate>
        @csrf
        @include("kepengurusan.metode_pembayaran._form")
    </form>
@endsection
