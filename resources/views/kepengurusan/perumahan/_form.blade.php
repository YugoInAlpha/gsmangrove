<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Nama Cluster</label>
                    <select name="id_cluster" class="form-control select2 @error('id_cluster') is-invalid @enderror">
                        <option value="">Pilih</option>
                        @foreach ($cluster as $data_cluster)
                            <option value="{{ $data_cluster->id_cluster }}"
                                @if (old('id_cluster', $perumahan->id_cluster) == $data_cluster->id_cluster) selected @endif>{{ $data_cluster->nama_cluster }}
                            </option>
                        @endforeach
                    </select>
                    @error('id_cluster')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label>Blok Rumah</label>
                        <input type="text" name="blok_rumah"
                            class="form-control @error('blok_rumah') is-invalid @enderror"
                            value="{{ old('blok_rumah', $perumahan->blok_perumahan) }}">
                        @error('blok_rumah')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col">
                        <label>Nomor Rumah</label>
                        <input type="text" name="nomor_rumah"
                            class="form-control @error('nomor_rumah') is-invalid @enderror"
                            value="{{ old('nomor_rumah', $perumahan->nama_perumahan) }}">
                        @error('nomor_rumah')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label>Status Rumah</label>
                    <select name="kepemilikan_rumah" id="kepemilikan_rumah"
                        class="form-control select2 @error('kepemilikan_rumah') is-invalid @enderror  @error('lain_lain') is-invalid @enderror">
                        <option value="Kosong" @if (old('kepemilikan_rumah', $perumahan->kepemilikan_rumah) === 'Kosong') selected @endif>Kosong</option>
                        <option value="Tidak Ditempati" @if (old('kepemilikan_rumah', $perumahan->kepemilikan_rumah) === 'Tidak Ditempati') selected @endif>Tidak
                            Ditempati</option>
                        <option value="Rumah Sendiri" @if (old('kepemilikan_rumah', $perumahan->kepemilikan_rumah) === 'Rumah Sendiri') selected @endif>Rumah Sendiri
                        </option>
                        <option value="Kontrak/Sewa" @if (old('kepemilikan_rumah', $perumahan->kepemilikan_rumah) === 'Kontrak/Sewa') selected @endif>Kontrak/Sewa
                        </option>
                        <option value="Lain-lain"
                            @if (old('kepemilikan_rumah') === 'Lain-lain') selected @elseif($model_form['model'] == 'edit' && $perumahan->kepemilikan_rumah !== 'Kosong' && $perumahan->kepemilikan_rumah !== 'Tidak Ditempati' && $perumahan->kepemilikan_rumah !== 'Rumah Sendiri' && $perumahan->kepemilikan_rumah !== 'Kontrak/Sewa') selected @endif>
                            Lain-lain</option>
                    </select>
                    @error('kepemilikan_rumah', 'lain_lain')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group d-none" id="input_lain">
                    <label>Status Kepemilikan Rumah (Lain-lain) <strong class="text-danger">*</strong></label>
                    <input type="text" name="lain_lain" id="lain_lain"
                        class="form-control @error('lain_lain') is-invalid @enderror"
                        value="{{ old('lain_lain', $perumahan->kepemilikan_rumah) }}"
                        placeholder="Silahkan Mengisi Data Kepemilikan Rumah">
                    @error('lain_lain')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group d-none" id="input_kontrak_sewa">
                    <div class="row">
                        <div class="form-group col-12 mb-1">
                            <label>Tanggal Mulai Kontrak/Sewa <strong class="text-danger">*</strong></label>
                            <input type="date"
                                class="form-control  @error('tanggal_mulai_kontrak_sewa') is-invalid @enderror"
                                name="tanggal_mulai_kontrak_sewa" id="tanggal_mulai_kontrak_sewa"
                                value="{{ old('tanggal_mulai_kontrak_sewa', $perumahan->tanggal_mulai_kontrak) }}">
                            <div class="invalid-feedback">

                            </div>
                        </div>
                        <div class="form-group col col-sm-12 col-md-7 col-lg-7">
                            <input type="number" name="periode_kontrak_sewa" id="periode_kontrak_sewa"
                                class="form-control @error('periode_kontrak_sewa') is-invalid @enderror"
                                value="{{ old('periode_kontrak_sewa', $perumahan->periode_kontrak) }}"
                                placeholder="Periode Kontrak/Sewa">
                            <div class="invalid-feedback">

                            </div>
                        </div>
                        <div class="form-group col col-sm-8 col-md-6 col-lg-5">
                            <select name="periode_waktu" id="periode_waktu"
                                class="form-control select2 @error('periode_waktu') is-invalid @enderror">
                                <option value="">Pilih</option>
                                <option value="years" @if (old('periode_waktu', $perumahan->periode_waktu) === 'years') selected @endif>Tahun</option>
                                <option value="months" @if (old('periode_waktu', $perumahan->periode_waktu) === 'months') selected @endif>Bulan</option>
                            </select>
                            <div class="invalid-feedback">

                            </div>
                        </div>
                    </div>
                </div>
                @if ($model_form['model'] == 'edit')
                    <div class="form-group">
                        <label>Status Perumahan</label>
                        <select name="status_perumahan"
                            class="form-control select2 @error('status_perumahan') is-invalid @enderror">
                            <option value="">Pilih</option>
                            <option value="Aktif" @if (old('status_perumahan', $perumahan->status_perumahan) === 'Aktif') selected @endif>Aktif</option>
                            <option value="Tidak Aktif" @if (old('status_perumahan', $perumahan->status_perumahan) === 'Tidak Aktif') selected @endif>Tidak AKtif
                            </option>
                        </select>
                        @error('status_perumahan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="card-footer text-left">
                <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
            </div>
        </div>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            var data = $('#kepemilikan_rumah option:selected').filter(':selected').val();
            if (data === 'Lain-lain') {
                $('#option_select').removeClass('d-none');
                $('#option_select').addClass('d-block');
            } else {
                $('#option_select').removeClass('d-block');
                $('#option_select').addClass('d-none');
            }

            $('#kepemilikan_rumah').change(function() {
                if ($(this).val() === 'Lain-lain') {
                    $('#option_select').removeClass('d-none');
                    $('#option_select').addClass('d-block');
                } else {
                    $('#option_select').removeClass('d-block');
                    $('#option_select').addClass('d-none');
                }
            });

            var data = $('#kepemilikan_rumah option:selected').filter(':selected').val();
            if (data === 'Lain-lain') {
                $('#input_kontrak_sewa').addClass('d-none');
                $('#input_lain').removeClass('d-none');
            } else if (data === "Kontrak/Sewa") {
                $('#input_lain').addClass('d-none');
                $('#input_kontrak_sewa').removeClass('d-none');
            } else {
                $('#tanggal_mulai_kontrak_sewa').val(null);
                $('#periode_kontrak_sewa').val(null);
                $('#periode_waktu').val(null).trigger('change');
                $('#lain_lain').val(null);

                $('#input_kontrak_sewa').addClass('d-none');
                $('#input_lain').addClass('d-none');
            }

            $('#kepemilikan_rumah').change(function() {
                if ($(this).val() === 'Lain-lain') {
                    $('#input_kontrak_sewa').addClass('d-none');
                    $('#input_lain').removeClass('d-none');
                } else if ($(this).val() === 'Kontrak/Sewa') {
                    $('#input_lain').addClass('d-none');
                    $('#input_kontrak_sewa').removeClass('d-none');
                } else {
                    $('#tanggal_mulai_kontrak_sewa').val(null);
                    $('#periode_kontrak_sewa').val(null);
                    $('#periode_waktu').val(null).trigger('change');
                    $('#lain_lain').val(null);

                    $('#input_kontrak_sewa').addClass('d-none');
                    $('#input_lain').addClass('d-none');
                }
            });

        });
    </script>
@endpush
