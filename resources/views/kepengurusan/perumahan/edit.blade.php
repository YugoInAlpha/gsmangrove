@extends("layouts.app")

@section('title', 'Ubah Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Perumahan Green Semanggi Mangrove
    </p>
    <form action="{{ route('perumahan.update', $perumahan->id_perumahan) }}" method="POST" novalidate>
        @method("PUT")
        @csrf
        @include("kepengurusan.perumahan._form")
    </form>
@endsection
