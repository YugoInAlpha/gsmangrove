@extends("layouts.app")

@section('title', 'Daftar Perumahan')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Perumahan Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="btn-group mb-3 btn-group-sm text-white" role="group">
                <a href="{{ route('perumahan.index') }}" class="btn btn-primary">Perumahan</a>
                <a href="{{ route('cluster.index') }}" class="btn btn-primary">Cluster</a>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('perumahan.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Perumahan
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables">
                            <thead>
                                <tr>
                                    <th>Nama Cluster</th>
                                    <th>Perumahan</th>
                                    <th>Kepemilikan Rumah</th>
                                    <th>Status Perumahan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($perumahan as $data_perumahan)
                                    <tr>
                                        <td>{{ $data_perumahan->cluster->nama_cluster }}</td>
                                        <td>{{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}</td>
                                        <td>{{ $data_perumahan->kepemilikan_rumah }}</td>
                                        <td>{{ $data_perumahan->status_perumahan }}</td>
                                        <td>
                                            <div class="m-1  d-none d-sm-block"></div>
                                            <a href="{{ route('perumahan.edit', $data_perumahan->id_perumahan) }}" class="btn btn-sm btn-icon icon-left btn-warning mr-1">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>
                                            <form style="all: unset" action="{{ route('perumahan.destroy', $data_perumahan->id_perumahan) }}" id="form_delete_perumahan_{{ $data_perumahan->id_perumahan }}" data-id="{{ $data_perumahan->cluster->nama_cluster }} {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirmPerumahan('form_delete_perumahan_{{ $data_perumahan->id_perumahan }}')" class="btn btn-sm btn-icon icon-left btn-danger"><i class="far fa-trash-alt"></i>Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true,
                    },
                    {
                        "width": "20%",
                        "targets": -1,
                    },
                    {
                        orderable: false,
                        targets: -1,
                    },
                ],
            });
        });

        function deleteconfirmPerumahan(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
