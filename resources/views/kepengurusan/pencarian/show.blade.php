@extends("layouts.app")

@section('title', 'Detail Pencarian')
@section('content')
    <h2 class="section-title">Lihat @yield("title")</h2>
    <p class="section-lead">
        Halaman Manajemen Iuran Green Semanggi Mangrove
    </p>
    <div class="card author-box card-primary">
        <div class="card-body">
            <div class="author-box-description">
                <div class="row">
                    <div class="col col-sm-12 col-lg-6 col-md-6 mt-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped table-sm">
                                        <tr>
                                            <th>Nama Warga</th>
                                            <td>
                                                <a href="{{ route('warga.show', $transaksi_iuran->warga->id_warga) }}">
                                                    {{ $transaksi_iuran->warga->nama_lengkap }}
                                                </a>
                                            </td>
                                            <th class="w-25 d-none d-sm-block"></th>
                                        </tr>
                                        <tr>
                                            <th>Perumahan</th>
                                            <td>{{ $transaksi_iuran->perumahan->cluster->nama_cluster }}
                                                {{ $transaksi_iuran->perumahan->blok_perumahan }}-{{ $transaksi_iuran->perumahan->nama_perumahan }}
                                            </td>
                                            <th class="w-25 d-none d-sm-block"></th>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Transaksi</th>
                                            <td>{{ \Carbon\Carbon::parse($transaksi_iuran->tanggal_transaksi)->isoFormat('dddd, D MMMM Y') }}
                                            </td>
                                            <th class="w-25 d-none d-sm-block"></th>
                                        </tr>
                                        <tr>
                                            <th>Periode</th>
                                            <td>{{ $transaksi_iuran->periode_pembayaran }} Bulan</td>
                                            <th class="w-25 d-none d-sm-block"></th>
                                        </tr>
                                        <tr>
                                            <th>Catatan</th>
                                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">
                                                {{ $transaksi_iuran->catatan }}</td>
                                            <th class="w-25 d-none d-sm-block"></th>
                                        </tr>
                                        <tr>
                                            @php
                                                if ($transaksi_iuran->status_pembayaran == 'Menunggu Konfirmasi') {
                                                    $kondisi = 'badge-warning';
                                                } elseif ($transaksi_iuran->status_pembayaran == 'Pembayaran Sukses') {
                                                    $kondisi = 'badge-success';
                                                } elseif ($transaksi_iuran->status_pembayaran == 'Pembayaran Gagal') {
                                                    $kondisi = 'badge-danger';
                                                }
                                            @endphp
                                            <th>Status</th>
                                            <td><span
                                                    class="badge {{ $kondisi }}">{{ $transaksi_iuran->status_pembayaran }}</span>
                                            </td>
                                            <th class="w-25 d-none d-sm-block"></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12">

                                <a href="#" class="btn btn-primary mr-1 ml-1 mb-1" data-toggle="modal"
                                    data-target=".modal-file-bukti-pembayaran">File Bukti Pembayaran</a>
                                @if ($transaksi_iuran->status_pembayaran != 'Menunggu Konfirmasi')
                                    @php
                                        $status = 'disabled';
                                    @endphp
                                @else
                                    @php
                                        $status = '';
                                    @endphp
                                @endif
                                <a href="{{ route('pencarian.update_status_iuran', $transaksi_iuran->id_transaksi_iuran) }}"
                                    class="btn btn-icon btn-warning mr-1 ml-1 mb-1 konfirmasi-pembayaran {{ $status }}">
                                    <i class="far fa-file-alt"></i> Konfirmasi Pembayaran
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col col-sm-12 col-lg-6 col-md-6 mt-2">
                        <div class="table-responsive">
                            <table class="table table-bordered table-sm">
                                <thead class="table-primary">
                                    <tr class="text-center">
                                        <th>Pembayaran Iuran Bulan</th>
                                        <th>Tahun</th>
                                        <th>Biaya Iuran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_biaya_iuran = 0;
                                    @endphp
                                    @foreach ($transaksi_iuran->detailiuran as $detail_transaksi)
                                        <tr>
                                            <td>{{ \Carbon\Carbon::parse($detail_transaksi->periode_iuran_bulan)->isoFormat('MMMM') }}
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($detail_transaksi->periode_iuran_bulan)->isoFormat('Y') }}
                                            </td>
                                            @php
                                                $total_biaya_iuran += $detail_transaksi->biaya_iuran;
                                            @endphp
                                            <td>Rp. {{ $detail_transaksi->biaya_iuran }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2"><strong>Total Biaya Iuran</strong></td>
                                        <td><strong>Rp. {{ $total_biaya_iuran }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade modal-file-bukti-pembayaran" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Bukti Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-kk">
                    <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                        <object
                            data="{{ asset('storage/assets/pdf/dokumen_bukti_pembayaran/' . $transaksi_iuran->file_bukti_transaksi) }}"
                            type="application/pdf"></object>
                    </div>
                    <div class="alert alert-warning d-block d-sm-none">
                        Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a
                            href="{{ asset('storage/assets/pdf/dokumen_bukti_pembayaran/' . $transaksi_iuran->file_bukti_transaksi) }}"
                            download>sini</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js-library')
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('css-template')
    <style>
        .swal-button.swal-button--pembayaran_sukses {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #6777ef;
        }

        .swal-button.swal-button--pembayaran_sukses:hover {
            opacity: .8;
        }

        .swal-button.swal-button--pembayaran_gagal {
            box-shadow: 0 2px 6px #fd9b96;
            background-color: #fc544b;
        }

        .swal-button.swal-button--pembayaran_gagal:hover {
            opacity: .8;
        }

    </style>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $(".modal-file-kk").on("hidden.bs.modal", function() {
                $(".modal-kk").html("");
                $(".modal-kk").html(
                    "<div class='embed-responsive embed-responsive-4by3'><object data='{{ asset('storage/assets/pdf/dokumen_bukti_pembayaran/' . $transaksi_iuran->file_bukti_transaksi) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_bukti_pembayaran/' . $transaksi_iuran->file_bukti_transaksi) }}' download>sini</a></div>"
                );
            });

            $('.konfirmasi-pembayaran').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pembayaran Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: {
                        pembayaran_sukses: {
                            text: "Terima Pembayaran",
                            value: "Pembayaran Sukses",
                        },
                        pembayaran_gagal: {
                            text: "Tolak Pembayaran",
                            value: "Pembayaran Gagal",
                        },
                        cancel: "Batal",
                    },
                }).then((value) => {
                    switch (value) {
                        case "Pembayaran Sukses":
                            window.location.href = url + "/" + value +
                                "/{{ $transaksi_iuran->id_warga }}/" + {!! $transaksi_iuran->id_perumahan !!};
                            break;
                        case "Pembayaran Gagal":
                            window.location.href = url + "/" + value +
                                "/{{ $transaksi_iuran->id_warga }}/" + {!! $transaksi_iuran->id_perumahan !!};
                            break;
                        default:
                            break;
                    }
                });
            });
        });
    </script>
@endpush
