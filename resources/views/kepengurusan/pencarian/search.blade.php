@extends("layouts.app")

@section('title', 'Pencarian')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Manajemen Iuran Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('pencarian.store') }}" method="POST" novalidate>
                @csrf
                <div class="row">
                    <div class="col-12">
                        <select name="warga" class="form-control select2 @error('warga') is-invalid @enderror">
                            <option value="" selected>Daftar Pencarian Warga</option>
                            @foreach ($warga as $data_warga)
                                <option value="{{ $data_warga->id_warga }}">
                                    <div class="row">
                                        <div class="col">
                                            {{ $data_warga->nama_lengkap }}
                                        </div>
                                        <div class="col">
                                            -
                                        </div>
                                        <div class="col">
                                            {{ $data_warga->nik }}
                                        </div>
                                    </div>
                                </option>
                            @endforeach
                        </select>
                        @error('warga')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <button class="btn btn-primary mt-2">Cari Warga</button>
            </form>
        </div>
    @endsection

    @push('css-library')
        <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    @endpush

    @push('css')
    @endpush

    @push('js-library')
        <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
        <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    @endpush


    @push('js-template')
        <script>
            $(document).ready(function() {
                $('.select2').select2({
                    language: "id"
                });

                $("#datatables-warga").dataTable({
                    responsive: true,
                    paging: true,
                    ordering: true,
                    info: true,
                    filter: true,
                    length: true,
                    processing: true,
                    deferRender: true,
                    autoWidth: false,
                    language: {
                        url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                    },
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function(row) {
                                    var data = row.data();
                                    return 'Data Warga ';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                        }
                    },
                    columnDefs: [{
                            targets: -1,
                            visible: true
                        },
                        {
                            "width": "28%",
                            "targets": -1
                        },
                        {
                            orderable: false,
                            targets: -1
                        },
                    ],
                });
            });
        </script>
    @endpush
