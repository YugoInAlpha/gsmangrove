@extends("layouts.app")

@section('title', 'Pencarian')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Manajemen Iuran Green Semanggi Mangrove
    </p>
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('pencarian.search', [$warga->id_warga]) }}" method="POST">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col">
                                        <select name="id_perumahan" class="form-control select2">
                                            <option value="">Pilih Perumahan</option>
                                            @foreach ($data_perumahan->perumahan as $rumah)
                                                <option value="{{ $rumah->id_perumahan }}"
                                                    @if ($rumah->id_perumahan == $selected_perumahan->id_perumahan) selected @endif>
                                                    {{ $rumah->cluster->nama_cluster }}
                                                    {{ $rumah->blok_perumahan }}-{{ $rumah->nama_perumahan }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col">
                                        <button type="submit" class="btn btn-block btn-icon icon-left btn-success">
                                            <i class="fas fa-home"></i> Ganti Perumahan
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-reponsive-sm">
                                <table class="table table-striped table-sm w-auto">
                                    <tr>
                                        <th class="w-25">Nama Warga</th>
                                        <td class="w-50"><a
                                                href="{{ route('warga.show', $warga->id_warga) }}">{{ $warga->nama_lengkap }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Perumahan</th>
                                        <td>
                                            {{ $selected_perumahan->cluster->nama_cluster }}
                                            {{ $selected_perumahan->blok_perumahan }}-{{ $selected_perumahan->nama_perumahan }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>No Telepon</th>
                                        <td><a href="http://api.whatsapp.com/send?phone={{ $warga->nomor_telepon }}"
                                                target="_blank">{{ $warga->nomor_telepon }}</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="far fa-check-circle size-icn"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Iuran Dibayar</h4>
                    </div>
                    <div class="card-body" id="iuran_bayar">
                        0
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="far fa-times-circle size-icn"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Iuran Belum Dibayar</h4>
                    </div>
                    <div class="card-body" id="iuran_belum_bayar">
                        0
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col col-xl-12 col-lg-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive table-scroll">
                    <table class="table table-bordered">
                        @php
                            $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                            
                            $tranasksi_bulanan_sudah_terbayar = 0;
                            $count_transaksi_sudah_terbayar = 0;
                            
                            $tranasksi_bulanan_belum_terbayar = 0;
                            $count_transaksi_belum_terbayar = 0;
                            
                            $data_iuran_belum_terbayar = [];
                            
                            $tahun_sekarang = \Carbon\Carbon::now()->isoFormat('Y');
                            $bulan_sekarang = \Carbon\Carbon::now()->isoFormat('M');
                            $tahun_kedepan = date('Y', strtotime('+1 year', strtotime($tahun_sekarang)));
                            
                            for ($i = $tahun_sekarang; $i <= $tahun_kedepan; $i++) {
                                for ($y = 1; $y <= count($bulan); $y++) {
                                    if ($y <= $bulan_sekarang && $i == $tahun_sekarang) {
                                        $kalender[$i][$y] = ['status' => 'Belum Dibayar'];
                                    } else {
                                        $kalender[$i][$y] = ['status' => 'Kosong'];
                                    }
                                }
                            }
                            
                            foreach ($data_iuran as $iuran) {
                                foreach ($iuran->detailiuran as $detail_iuran) {
                                    $bulan_transaksi = \Carbon\Carbon::parse($detail_iuran->periode_iuran_bulan)->isoFormat('M');
                                    $tahun_transaksi = \Carbon\Carbon::parse($detail_iuran->periode_iuran_bulan)->isoFormat('Y');
                                    if ($tahun_transaksi >= $tahun_sekarang) {
                                        $kalender[$tahun_transaksi][$bulan_transaksi] = ['status' => $iuran->status_pembayaran];
                                    }
                                }
                            }
                            
                            ksort($kalender);
                        @endphp

                        <thead class="text-center">
                            @foreach ($kalender as $key => $data)
                                @for ($i = 0; $i < count($kalender[$key]); $i++)
                                    <th>{{ $bulan[$i] }} {{ $key }}</th>
                                @endfor
                            @endforeach
                        </thead>
                        <tbody>
                            @foreach ($kalender as $key => $data)
                                @for ($i = 1; $i <= count($kalender[$key]); $i++)
                                    @if ($kalender[$key][$i]['status'] == 'Pembayaran Sukses')
                                        @php
                                            $count_transaksi_sudah_terbayar += 1;
                                            $tranasksi_bulanan_sudah_terbayar = $count_transaksi_sudah_terbayar;
                                        @endphp
                                        <td class="table-success text-center">
                                            <i class="far fa-check-circle size-icn"></i>
                                        </td>
                                    @elseif($kalender[$key][$i]['status'] == 'Menunggu Konfirmasi')
                                        <td class="table-warning text-center">
                                            <i class="far fa-check-circle size-icn"></i>
                                        </td>
                                    @elseif ($kalender[$key][$i]['status'] == 'Belum Dibayar' || $kalender[$key][$i]['status'] == 'Pembayaran Gagal')
                                        @php
                                            $count_transaksi_belum_terbayar += 1;
                                            $tranasksi_bulanan_belum_terbayar = $count_transaksi_belum_terbayar;
                                            
                                            array_push($data_iuran_belum_terbayar, ['Tahun' => $key, 'Bulan' => $i]);
                                        @endphp
                                        <td class="table-danger text-center">
                                            <i class="far fa-times-circle size-icn"></i>
                                        </td>
                                    @else
                                        <td class="table-secondary text-center"></td>
                                    @endif
                                @endfor
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="row mt-5">
                    <div class="col-12">
                        <div style="overflow: hidden;">
                            <table class="table table-striped" id="datatables-warga">
                                <thead>
                                    <tr>
                                        <th>Tanggal Transaksi</th>
                                        <th>Periode Pembayaran</th>
                                        <th>Status</th>
                                        <th>Total Bayar</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data_iuran as $iuran)
                                        <tr>
                                            <td>{{ \Carbon\Carbon::parse($iuran->tanggal_transaksi)->isoFormat('dddd, D MMMM Y') }}
                                            </td>
                                            <td>{{ $iuran->periode_pembayaran }} Bulan</td>
                                            @php
                                                if ($iuran->status_pembayaran == 'Menunggu Konfirmasi') {
                                                    $kondisi = 'badge-warning';
                                                } elseif ($iuran->status_pembayaran == 'Pembayaran Sukses') {
                                                    $kondisi = 'badge-success';
                                                } elseif ($iuran->status_pembayaran == 'Pembayaran Gagal') {
                                                    $kondisi = 'badge-danger';
                                                }
                                            @endphp
                                            <td class="font-weight-600"><span
                                                    class="badge {{ $kondisi }}">{{ $iuran->status_pembayaran }}</span>
                                            </td>
                                            <td>Rp. {{ $iuran->total_iuran }}</td>
                                            <td>
                                                <a href="{{ route('pencarian.show', $iuran->id_transaksi_iuran) }}"
                                                    class="btn btn-sm btn-icon btn-info m-1">
                                                    <i class="far fa-eye hidden-lg"></i> Lihat
                                                </a>
                                                @if ($iuran->status_pembayaran != 'Menunggu Konfirmasi')
                                                    @php
                                                        $status = 'disabled';
                                                    @endphp
                                                @else
                                                    @php
                                                        $status = '';
                                                    @endphp
                                                @endif
                                                <a href="{{ route('pencarian.update_status_iuran', $iuran->id_transaksi_iuran) }}"
                                                    class="btn btn-sm btn-icon btn-warning konfirmasi-pembayaran m-1 {{ $status }}">
                                                    <i class="far fa-file-alt"></i> Konfirmasi Pembayaran
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('css-template')
    <style>
        .swal-button.swal-button--pembayaran_sukses {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #6777ef;
        }

        .swal-button.swal-button--pembayaran_sukses:hover {
            opacity: .8;
        }

        .swal-button.swal-button--pembayaran_gagal {
            box-shadow: 0 2px 6px #fd9b96;
            background-color: #fc544b;
        }

        .swal-button.swal-button--pembayaran_gagal:hover {
            opacity: .8;
        }

    </style>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables-warga").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "30%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });

            const mouseWheel = document.querySelector('.table-scroll');

            mouseWheel.addEventListener('wheel', function(e) {
                const race = 20;
                if (e.deltaY > 0)
                    mouseWheel.scrollLeft += race;
                else
                    mouseWheel.scrollLeft -= race;
                e.preventDefault();
            });

            document.getElementById("iuran_bayar").innerHTML = "{{ $tranasksi_bulanan_sudah_terbayar }}";
            document.getElementById("iuran_belum_bayar").innerHTML = "{{ $tranasksi_bulanan_belum_terbayar }}";

            $('.konfirmasi-pembayaran').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pembayaran Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: {
                        pembayaran_sukses: {
                            text: "Terima Pembayaran",
                            value: "Pembayaran Sukses",
                        },
                        pembayaran_gagal: {
                            text: "Tolak Pembayaran",
                            value: "Pembayaran Gagal",
                        },
                        cancel: "Batal",
                    },
                }).then((value) => {
                    switch (value) {
                        case "Pembayaran Sukses":
                            window.location.href = url + "/" + value +
                                "/{{ $warga->id_warga }}/" +
                                {{ $selected_perumahan->id_perumahan }};
                            break;
                        case "Pembayaran Gagal":
                            window.location.href = url + "/" + value +
                                "/{{ $warga->id_warga }}/" +
                                {{ $selected_perumahan->id_perumahan }};
                            break;
                        default:
                            break;
                    }
                });
            });

        });
    </script>
@endpush
