@extends("layouts.app")

@section('title', 'Ubah Warga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Warga Green Semanggi Mangrove
    </p>
    <div class="alert alert-primary alert-has-icon">
        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
        <div class="alert-body">
            <div class="alert-title">Info</div>
            Jika tidak menemukan No. KK silahkan melakukan tambah data KK di <a href="{{ route('kartu_keluarga_warga.create') }}" class="badge badge-info">link ini</a>
        </div>
    </div>
    <form action="{{ route('warga.update', $warga->id_warga) }}" method="POST" enctype="multipart/form-data" novalidate>
        @method("PUT")
        @csrf
        @include("kepengurusan.warga._form")
    </form>
@endsection
