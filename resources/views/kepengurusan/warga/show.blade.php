@extends("layouts.app")

@section('title', 'Detail Warga')
@section('content')
    <h2 class="section-title">Lihat @yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Warga Green Semanggi Mangrove
    </p>
    <div class="card author-box card-primary">
        <div class="card-body">
            <div>
                <div class="author-box-name">
                    <a href="#">{{ $warga->nama_lengkap }}</a>
                </div>
                <div class="author-box-job">No KK. {{ $warga->no_kk }}</div>
                <div class="w-100 d-sm-none"></div>
            </div>
            <div class="author-box-description">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">
                                <tr>
                                    <th>NIK</th>
                                    <td class="absorbing-column">{{ $warga->nik }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <td class="absorbing-column">{{ $warga->nama_lengkap }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <td class="absorbing-column">{{ $warga->jenis_kelamin }}</td>
                                </tr>
                                <tr>
                                    <th>Agama</th>
                                    <td class="absorbing-column">{{ $warga->agama }}</td>
                                </tr>
                                <tr>
                                    <th>No Telepon</th>
                                    <td class="absorbing-column"><a href="http://api.whatsapp.com/send?phone={{ $warga->nomor_telepon }}" target="_blank">{{ $warga->nomor_telepon }}</a></td>
                                </tr>
                                <tr>
                                    <th>Pekerjaan</th>
                                    <td class="absorbing-column">{{ $warga->pekerjaan }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat KTP</th>
                                    <td class="absorbing-column">{{ $warga->alamat_ktp }}</td>
                                </tr>
                                @foreach ($perumahan as $key => $data_perumahan)
                                    <tr>
                                        <th>Alamat Perumahan {{ $loop->iteration }}</th>
                                        <td>{{ $data_perumahan->cluster->nama_cluster }}, {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Status Kawin</th>
                                    <td class="absorbing-column">{{ $warga->status_kawin }}</td>
                                </tr>
                                <tr>
                                    <th>Tempat/Tgl. Lahir</th>
                                    <td class="absorbing-column">{{ $warga->tempat_lahir }}, {{ \Carbon\Carbon::parse($warga->tanggal_lahir)->isoFormat('D MMMM Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Gol. Darah</th>
                                    <td class="absorbing-column">{{ $warga->golongan_darah }}</td>
                                </tr>
                                <tr>
                                    <th>Status Hubungan Keluarga</th>
                                    <td class="absorbing-column">{{ $warga->status_hubungan_keluarga }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col col-sm-12 col-lg-2 col-md-2 ">
                        <strong>File Kependudukan:</strong>
                    </div>
                    <div class="col col-sm-12 col-lg-6 col-md-6">
                        <div class="btn-group btn-group-sm" role="group">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".modal-file-kk">File KK</a>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".modal-file-ktp">File KTP</a>
                            <a href="#" class="btn btn-primary @if (empty($warga->file_pernikahan)) disabled @endif" data-toggle="modal" data-target=".modal-file-pernikahan">File Pernikahan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade modal-file-kk" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Kartu Keluarga</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-kk">
                    <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                        <object data="{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->file_kk) }}" type="application/pdf"></object>
                    </div>
                    <div class="alert alert-warning d-block d-sm-none">
                        Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href="{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->file_kk) }}" download>sini</a>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-file-ktp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File KTP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-ktp">
                    <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                        <object data="{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}" type="application/pdf"></object>
                    </div>
                    <div class="alert alert-warning d-block d-sm-none">
                        Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href="{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}" download>sini</a>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-file-pernikahan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Pernikahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-pernikahan">
                    @if (!empty($warga->file_pernikahan))
                        <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                            <object data="{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->file_pernikahan) }}" type="application/pdf"></object>
                        </div>
                        <div class="alert alert-warning d-block d-sm-none">
                            Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href="{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->file_pernikahan) }}" download>sini</a>
                        </div>
                    @else
                        <div class="alert alert-danger alert-has-icon">
                            <div class="alert-icon"><i class="fas fa-praying-hands"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Mohon Maaf</div>
                                File dokumen ini tidak tersedia
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-template')
    <style>
        table {
            table-layout: auto !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        table .absorbing-column {
            width: 80% !important;
            align-content: left;
        }

    </style>
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $(".modal-file-kk").on("hidden.bs.modal", function() {
                $(".modal-kk").html("");
                $(".modal-kk").html("<div class='embed-responsive embed-responsive-4by3 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->file_kk) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->file_kk) }}' download>sini</a></div>");
            });

            $(".modal-file-ktp").on("hidden.bs.modal", function() {
                $(".modal-ktp").html("");
                $(".modal-ktp").html("<div class='embed-responsive embed-responsive-4by3 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}' download>sini</a></div>");
            });

            $(".modal-file-pernikahan").on("hidden.bs.modal", function() {
                $(".modal-pernikahan").html("@if (!empty($warga->file_pernikahan)) <div class='embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->file_pernikahan) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->file_pernikahan) }}' download>sini</a></div>@else<div class='alert alert-danger alert-has-icon'><div class='alert-icon'><i class='fas fa-praying-hands'></i></div><div class='alert-body'><div class='alert-title'>Mohon Maaf</div>File dokumen ini tidak tersedia</div></div>@endif");
            });
        });
    </script>
@endpush
