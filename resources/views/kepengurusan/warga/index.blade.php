@extends("layouts.app")

@section('title', 'Daftar Warga')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Warga Green Semanggi Mangrove
    </p>
    <div class="col">
        <form action="{{ route('warga.export_dokumen') }}" id="export_dokumen" method="POST">
            @csrf
            <div class="form-group row">
                <select class="form-control col-12 col-md-2 col-lg-2 select2 @error('nama_cluster') is-invalid @enderror" name="nama_cluster">
                    <option value="">Pilih</option>
                    <option value="semua_cluster">Semua Cluster</option>
                    @foreach ($cluster as $data_cluster)
                        <option value="{{ $data_cluster->id_cluster }}">{{ $data_cluster->nama_cluster }}</option>
                    @endforeach
                </select>
                <button type="submit" form="export_dokumen" class="btn col-12 col-md-2 col-lg-2 btn-info">
                    <i class="fas fa-file-export"></i> Export Dokumen Warga
                </button>
            </div>
            @error('nama_cluster')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </form>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <a href="{{ route('warga.create') }}" class="btn btn-icon btn-block icon-left btn-success">
                            <i class="fas fa-plus-circle"></i> Tambah Data Warga
                        </a>
                    </div>
                </div>
                <div class="col col-12 col-md-8 col-lg-8">
                    <form action="{{ route('warga.export_data') }}" id="export_data" method="POST">
                        <div class="form-group row">
                            @csrf
                            <select class="form-control col-12 col-md-3 col-lg-3 select2 @error('cluster') is-invalid @enderror" name="cluster">
                                <option value="">Pilih</option>
                                @foreach ($cluster as $data_cluster)
                                    <option value="{{ $data_cluster->id_cluster }}">{{ $data_cluster->nama_cluster }}</option>
                                @endforeach
                            </select>
                            <button type="submit" form="export_data" class="btn col-12 col-md-3 col-lg-3 btn-warning">
                                <i class="fas fa-file-export"></i> Export Data Warga
                            </button>
                        </div>
                        @error('cluster')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables-warga">
                            <thead>
                                <tr>
                                    <th>No KK</th>
                                    <th>NIK</th>
                                    <th>Nama Lengkap</th>
                                    <th>Nomor Telepon</th>
                                    <th class="noExport">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($warga as $data_warga)
                                    <tr>
                                        <td>{{ $data_warga->kartukeluarga->no_kk }}</td>
                                        <td>{{ $data_warga->nik }}</td>
                                        <td>{{ $data_warga->nama_lengkap }}</td>
                                        <td><a href="http://api.whatsapp.com/send?phone={{ $data_warga->nomor_telepon }}" target="_blank">{{ $data_warga->nomor_telepon }}</a></td>
                                        <td>
                                            <a href="{{ route('warga.show', $data_warga->id_warga) }}" class="btn btn-sm btn-icon btn-info mb-1">
                                                <i class="far fa-eye hidden-lg"></i> Lihat
                                            </a>
                                            <a href="{{ route('warga.edit', $data_warga->id_warga) }}" class="btn btn-sm btn-icon btn-warning mr-1 ml-1 mb-1">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>
                                            <form style="all: unset" action="{{ route('warga.destroy', $data_warga->id_warga) }}" id="form_delete_{{ $data_warga->id_warga }}" data-id="{{ $data_warga->nik }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirm('form_delete_{{ $data_warga->id_warga }}')" class="btn btn-sm btn-icon btn-danger mb-1"><i class="far fa-trash-alt"></i>
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endpush


@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush


@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            $("#datatables-warga").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data Warga ';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });
        });

        function deleteconfirm(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
