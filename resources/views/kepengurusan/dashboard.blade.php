@extends("layouts.app")

@section('title', 'Dashboard')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Statistik Pembayaran Iuran Bulanan | Tahun : {{ \Carbon\Carbon::now()->year }}</h4>
                </div>
                <div class="card-body">
                    <canvas id="myChart" width="100%" height="240"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Warga</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlah_warga }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-home"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Rumah</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlah_perumahan }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-user-circle"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Akun</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlah_akun }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-7 col-lg-7">
            <div class="card">
                <div class="card-header">
                    <h4>Transaksi Iuran Terbaru</h4>
                    <div class="card-header-action">
                        <a href="{{ route('iuran_warga.index') }}" class="btn btn-primary">Lihat lebih lanjut <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive table-invoice">
                        <table class="table table-striped">
                            <tr>
                                <th>Nama Warga</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                            </tr>
                           @if (!$data_transaksi->isEmpty())
                                @foreach ($data_transaksi as $transaksi)
                                    <tr>
                                        <td><a href="{{ route('pencarian.show', $transaksi->id_transaksi_iuran) }}">{{ $transaksi->warga->nama_lengkap }}</a></td>
                                        <td class="font-weight-600">{{ \Carbon\Carbon::parse($transaksi->tanggal_transaksi)->isoFormat('dddd, D MMMM Y') }}</td>
                                        @php
                                            if ($transaksi->status_pembayaran == 'Menunggu Konfirmasi') {
                                                $kondisi = 'badge-warning';
                                            } elseif ($transaksi->status_pembayaran == 'Pembayaran Sukses') {
                                                $kondisi = 'badge-success';
                                            } elseif ($transaksi->status_pembayaran == 'Pembayaran Gagal') {
                                                $kondisi = 'badge-danger';
                                            }
                                        @endphp
                                        <td class="font-weight-600"><span class="badge {{ $kondisi }}">{{ $transaksi->status_pembayaran }}</span></td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                               <th colspan="3" class="text-center">Tidak Ada Data Iuran</th>
                            </tr>
                           @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-5 col-lg-5">
            <div class="card">
                <div class="card-header">
                    <h4>Akun Terbaru</h4>
                    <div class="card-header-action">
                        <a href="{{ route('pengguna.index') }}" class="btn btn-primary">Lihat lebih lanjut <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="table-invoice">
                        <table class="table table-striped">
                            <tr>
                                <th>Nama Warga</th>
                                <th>Aksi</th>
                            </tr>
                            @if (!$data_pengguna->isEmpty())
                                @foreach ($data_pengguna as $pengguna)
                                    <tr>
                                        <td><a href="{{ route('pengguna.show', $pengguna->id_pengguna) }}">{{ $pengguna->kartukeluarga->warga->first()['nama_lengkap'] }}</a></td>
                                        <td>
                                            <a href="{{ route('pengguna.update_status_akun', $pengguna->id_pengguna) }}" class="btn btn-sm btn-icon btn-primary mr-1 ml-1 mb-1 konfirmasi-akun @if ($pengguna->status_akun === 'Terkonfirmasi') disabled @endif">
                                                <i class="far fa-file-alt"></i> Konfirmasi
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                <th colspan="3" class="text-center">Tidak Ada Data Akun</th>
                             </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Jumlah Warga Berdasarkan Umur</h4>
                </div>
                <div class="card-body">
                    <canvas id="myChart1" height="250" class="table"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Jumlah Warga Berdasarkan Agama</h4>
                </div>
                <div class="card-body">
                    <canvas id="myChart2" height="130"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Jumlah Warga Berdasarkan Jenis Kelamin</h4>
                </div>
                <div class="card-body">
                    <canvas id="myChart3" height="120"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Jumlah Warga Berdasarkan Golongan Darah</h4>
                </div>
                <div class="card-body">
                    <canvas id="myChart4" height="120"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('file-css-library')

@endpush

@push('file-css')
    <style>
        .swal-button.swal-button--pembayaran_sukses {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #6777ef;
        }

        .swal-button.swal-button--pembayaran_sukses:hover {
            opacity: .8;
        }

        .swal-button.swal-button--pembayaran_gagal {
            box-shadow: 0 2px 6px #fd9b96;
            background-color: #fc544b;
        }

        .swal-button.swal-button--pembayaran_gagal:hover {
            opacity: .8;
        }

    </style>
@endpush

@push('js-library')
    <script src="https://d3js.org/d3-color.v1.min.js"></script>
    <script src="https://d3js.org/d3-interpolate.v1.min.js"></script>
    <script src="https://d3js.org/d3-scale-chromatic.v1.min.js"></script>

    <script src="assets/modules/chart.min.js"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.konfirmasi-akun').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pengguna Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: {
                        pembayaran_sukses: {
                            text: "Konfirmasi Pengguna",
                            value: "Terkonfirmasi",
                        },
                        pembayaran_gagal: {
                            text: "Tolak Pengguna",
                            value: "Data Tidak Sesuai Ketentuan",
                        },
                        cancel: "Batal",
                    },
                }).then((value) => {
                    switch (value) {
                        case "Terkonfirmasi":
                            window.location.href = url + "/" + value;
                            break;
                        case "Data Tidak Sesuai Ketentuan":
                            window.location.href = url + "/" + value;
                            break;
                        default:
                            break;
                    }
                });
            });

        });
    </script>
    <script>
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    data: {!! json_encode($data_bayar_iuran_bulan) !!},
                    label: 'Pemasukan Iuran',
                    borderColor: '#6777ef',
                    pointBackgroundColor: '#ffffff',
                    borderWidth: 2.5,
                }],
                labels: {!! json_encode($label_bulan) !!},
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'top',
                    display: true,
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
            }
        });
    </script>

    <script>
        function calculatePoint(i, intervalSize, colorRangeInfo) {
            var {
                colorStart,
                colorEnd,
                useEndAsStart
            } = colorRangeInfo;
            return (useEndAsStart ?
                (colorEnd - (i * intervalSize)) :
                (colorStart + (i * intervalSize)));
        }

        /* Must use an interpolated color scale, which has a range of [0, 1] */
        function interpolateColors(dataLength, colorScale, colorRangeInfo) {
            var {
                colorStart,
                colorEnd
            } = colorRangeInfo;
            var colorRange = colorEnd - colorStart;
            var intervalSize = colorRange / dataLength;
            var i, colorPoint;
            var colorArray = [];

            for (i = 0; i < dataLength; i++) {
                colorPoint = calculatePoint(i, intervalSize, colorRangeInfo);
                colorArray.push(colorScale(colorPoint));
            }

            return colorArray;
        }
        /* Set up Chart.js Pie Chart */
        function createChart(chartId, chartData, colorScale, colorRangeInfo) {
            /* Grab chart element by id */
            const chartElement = document.getElementById(chartId);

            const dataLength = chartData.data.length;

            /* Create color array */
            var COLORS = interpolateColors(dataLength, colorScale, colorRangeInfo);

            /* Create chart */
            const myChart = new Chart(chartElement, {
                type: 'doughnut',
                data: {
                    labels: chartData.labels,
                    datasets: [{
                        backgroundColor: COLORS,
                        hoverBackgroundColor: COLORS,
                        data: chartData.data
                    }],
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                    },
                    hover: {
                        onHover: function(e) {
                            var point = this.getElementAtEvent(e);
                            e.target.style.cursor = point.length ? 'pointer' : 'default';
                        },
                    },
                }
            });

            return myChart;
        }
    </script>

    <script>
        createChart('myChart1', {
            labels: {!! json_encode($label_umur) !!},
            data: {!! json_encode($data_umur) !!},
        }, d3.interpolateSinebow, {
            colorStart: 0.30,
            colorEnd: 1,
            useEndAsStart: false,
        });
    </script>

    <script>
        createChart('myChart2', {
            labels: {!! json_encode($label_agama) !!},
            data: {!! json_encode($total_agama) !!},
        }, d3.interpolateCool, {
            colorStart: 0,
            colorEnd: 0.65,
            useEndAsStart: true,
        });
    </script>

    <script>
        createChart('myChart3', {
            labels: {!! json_encode($label_jenis_kelamin) !!},
            data: {!! json_encode($total_jenis_kelamin) !!},
        }, d3.interpolateSinebow, {
            colorStart: 0.1,
            colorEnd: 1,
            useEndAsStart: true,
        });
    </script>

    <script>
        createChart('myChart4', {
            labels: {!! json_encode($label_golongan_darah) !!},
            data: {!! json_encode($total_golongan_darah) !!},
        }, d3.interpolateInferno, {
            colorStart: 0.45,
            colorEnd: 0.75,
            useEndAsStart: true,
        });
    </script>
@endpush
