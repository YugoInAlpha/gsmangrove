@extends("layouts.app")

@section('title', 'Tambah Cluster')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Cluster Green Semanggi Mangrove
    </p>
    <form action="{{ route('cluster.store') }}" method="POST" novalidate>
        @csrf
        @include("kepengurusan.cluster._form")
    </form>
@endsection
