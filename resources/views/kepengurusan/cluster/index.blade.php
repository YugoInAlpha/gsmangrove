@extends("layouts.app")

@section('title', 'Daftar Cluster')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Cluster Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="btn-group mb-3 btn-group-sm text-white" role="group">
                <a href="{{ route('perumahan.index') }}" class="btn btn-primary">Perumahan</a>
                <a href="{{ route('cluster.index') }}" class="btn btn-primary">Cluster</a>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('cluster.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Cluster
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables">
                            <thead>
                                <tr>
                                    <th>Cluster</th>
                                    <th>Biaya Iuran</th>
                                    <th>Biaya Iuran Tidak Ditempati</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cluster as $data_cluster)
                                    <tr>
                                        <td>{{ $data_cluster->nama_cluster }}</td>
                                        <td>{{ $data_cluster->biaya_iuran }}</td>
                                        <td>{{ $data_cluster->biaya_iuran_tidak_ditempati }}</td>
                                        <td>{{ $data_cluster->status_cluster }}</td>
                                        <td>
                                            <a href="{{ route('cluster.edit', $data_cluster->id_cluster) }}" class="btn btn-sm btn-icon icon-left btn-warning m-1" data-toggle="tooltip" data-placement="top" title="Ubah">
                                                <i class="far fa-edit "></i> Ubah
                                            </a>
                                            <form style="all: unset" action="{{ route('cluster.destroy', $data_cluster->id_cluster) }}" id="form_delete_cluster{{ $data_cluster->id_cluster }}" data-id="{{ $data_cluster->nama_cluster }}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="button" onclick="deleteconfirmCluster('form_delete_cluster{{ $data_cluster->id_cluster }}')" class="btn btn-sm btn-icon icon-left btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="far fa-trash-alt"></i>Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "20%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });
        });

        function deleteconfirmCluster(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
