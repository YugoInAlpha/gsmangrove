@extends("layouts.app")

@section('title', 'Ubah Cluster')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Cluster Green Semanggi Mangrove
    </p>
    <form action="{{ route('cluster.update', $cluster->id_cluster) }}" method="POST" novalidate>
        @method("PUT")
        @csrf
        @include("kepengurusan.cluster._form")
    </form>
@endsection
