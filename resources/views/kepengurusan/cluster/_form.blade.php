<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class=" form-group">
                    <label>Nama Cluster</label>
                    <input type="text" name="nama_cluster" class="form-control @error('nama_cluster') is-invalid @enderror" value="{{ old('nama_cluster', $cluster->nama_cluster) }}">
                    @error('nama_cluster')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Biaya Iuran Cluster</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text @error('biaya_iuran') border border-danger @enderror">
                                <strong>Rp.</strong>
                            </div>
                        </div>
                        <input type="text" name="biaya_iuran" class="form-control @error('biaya_iuran') is-invalid @enderror" value="{{ old('biaya_iuran', $cluster->biaya_iuran) }}">
                        @error('biaya_iuran')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label>Biaya Iuran Tidak Ditempati Cluster</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text @error('biaya_iuran_tidak_ditempati') border border-danger @enderror">
                                <strong>Rp.</strong>
                            </div>
                        </div>
                        <input type="text" name="biaya_iuran_tidak_ditempati" class="form-control @error('biaya_iuran_tidak_ditempati') is-invalid @enderror" value="{{ old('biaya_iuran_tidak_ditempati', $cluster->biaya_iuran_tidak_ditempati) }}">
                        @error('biaya_iuran_tidak_ditempati')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                @if ($model_form['model'] == 'edit')
                    <div class="form-group">
                        <label>Status Cluster</label>
                        <select name="status_cluster" class="form-control select2 @error('status_cluster') is-invalid @enderror">
                            <option value="">Pilih</option>
                            <option value="Aktif" @if (old('status_cluster', $cluster->status_cluster) === 'Aktif') selected @endif>Aktif</option>
                            <option value="Tidak Aktif" @if (old('status_cluster', $cluster->status_cluster) === 'Tidak Aktif') selected @endif>Tidak AKtif</option>
                        </select>
                        @error('status_cluster')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="card-footer text-left">
                <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
            </div>
        </div>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/cleave-js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('assets/modules/cleave-js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-pwstrength/jquery.pwstrength.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endpush
