@extends("layouts.app")

@section('title', 'Tambah Pengguna')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Tambah Data Pengguna Green Semanggi Mangrove
    </p>
    <form action="{{ route('pengguna.store') }}" method="POST" novalidate>
        @csrf
        @include("kepengurusan.pengguna._form")
    </form>
@endsection
