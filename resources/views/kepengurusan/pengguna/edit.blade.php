@extends("layouts.app")

@section('title', 'Ubah Pengguna')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Form Ubah Data Pengguna Green Semanggi Mangrove
    </p>
    <form action="{{ route('pengguna.update', $pengguna->id_pengguna) }}" method="POST" novalidate>
        @method("PUT")
        @csrf
        @include("kepengurusan.pengguna._form")
    </form>
@endsection
