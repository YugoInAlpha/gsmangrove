@extends("layouts.app")

@section('title', 'Detail Warga')
@section('content')
    <h2 class="section-title">Lihat @yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Data Warga Green Semanggi Mangrove
    </p>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Warga Terdaftar</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlah_warga }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card author-box card-primary">
        <div class="card-body">
            <div>
                <div class="author-box-name">
                    <a href="{{ route('warga.show', $warga->id_warga) }}">{{ $warga->nama_lengkap }}</a>
                </div>
                <div class="author-box-job">No KK. {{ $warga->no_kk }}</div>
                <div class="w-100 d-sm-none"></div>
            </div>
            <div class="author-box-description">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">
                                <tr>
                                    <th>NIK</th>
                                    <td class="absorbing-column">{{ $warga->nik }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <td class="absorbing-column">{{ $warga->nama_lengkap }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <td class="absorbing-column">{{ $warga->jenis_kelamin }}</td>
                                </tr>
                                <tr>
                                    <th>Agama</th>
                                    <td class="absorbing-column">{{ $warga->agama }}</td>
                                </tr>
                                <tr>
                                    <th>No Telepon</th>
                                    <td class="absorbing-column">{{ $warga->nomor_telepon }}</td>
                                </tr>
                                <tr>
                                    <th>Pekerjaan</th>
                                    <td class="absorbing-column">{{ $warga->pekerjaan }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat KTP</th>
                                    <td class="absorbing-column">{{ $warga->alamat_ktp }}</td>
                                </tr>
                                @foreach ($warga->kartukeluarga->perumahan as $key => $data_perumahan)
                                    <tr>
                                        <th>Alamat Perumahan {{ $key + 1 }}</th>
                                        <td>{{ $data_perumahan->cluster->nama_cluster }}, {{ $data_perumahan->blok_perumahan }}-{{ $data_perumahan->nama_perumahan }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Status Kawin</th>
                                    <td class="absorbing-column">{{ $warga->status_kawin }}</td>
                                </tr>
                                <tr>
                                    <th>Tempat/Tgl. Lahir</th>
                                    <td class="absorbing-column">{{ $warga->tempat_lahir }}, {{ \Carbon\Carbon::parse($warga->tanggal_lahir)->isoFormat('D MMMM Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Gol. Darah</th>
                                    <td class="absorbing-column">{{ $warga->golongan_darah }}</td>
                                </tr>
                                <tr>
                                    <th>Status Hubungan Keluarga</th>
                                    <td class="absorbing-column">{{ $warga->status_hubungan_keluarga }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="{{ route('pengguna.update_status_akun', $pengguna->id_pengguna) }}" class="btn btn-icon btn-primary mr-1 ml-1 mb-1 konfirmasi-akun @if ($pengguna->status_akun === 'Terkonfirmasi') disabled @endif">
                        <i class="far fa-file-alt"></i> Konfirmasi Akun
                    </a>
                </div>
                <div class="row mt-4">
                    <div class="col col-sm-12 col-lg-2 col-md-2 ">
                        <strong>File Kependudukan:</strong>
                    </div>
                    <div class="col col-sm-12 col-lg-6 col-md-6">
                        <div class="btn-group btn-group-sm" role="group">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".modal-file-kk">File KK</a>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".modal-file-ktp">File KTP</a>
                            <a href="#" class="btn btn-primary @if (empty($warga->kartukeluarga->file_pernikahan)) disabled @endif" data-toggle="modal" data-target=".modal-file-pernikahan">File Pernikahan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    <div class="modal fade modal-file-kk" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Kartu Keluarga</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-kk">
                    <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                        <object data="{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->kartukeluarga->file_kk) }}" type="application/pdf"></object>
                    </div>
                    <div class="alert alert-warning d-block d-sm-none">
                        Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href="{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->kartukeluarga->file_kk) }}" download>sini</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-file-ktp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File KTP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-ktp">
                    <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                        <object data="{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}" type="application/pdf"></object>
                    </div>
                    <div class="alert alert-warning d-block d-sm-none">
                        Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href="{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}" download>sini</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-file-pernikahan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">File Pernikahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-pernikahan">
                    @if (!empty($warga->kartukeluarga->file_pernikahan))
                        <div class="embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block">
                            <object data="{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->kartukeluarga->file_pernikahan) }}" type="application/pdf"></object>
                        </div>
                        <div class="alert alert-warning d-block d-sm-none">
                            Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href="{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->kartukeluarga->file_pernikahan) }}" download>sini</a>
                        </div>
                    @else
                        <div class="alert alert-danger alert-has-icon">
                            <div class="alert-icon"><i class="fas fa-praying-hands"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Mohon Maaf</div>
                                File dokumen ini tidak tersedia
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')

@endpush
@push('js-library')
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush
@push('css-template')
    <style>
        table {
            table-layout: auto !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        table .absorbing-column {
            width: 80% !important;
            align-content: left;
        }

    </style>

    <style>
        .swal-button.swal-button--pembayaran_sukses {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #6777ef;
        }

        .swal-button.swal-button--pembayaran_sukses:hover {
            opacity: .8;
        }

        .swal-button.swal-button--pembayaran_gagal {
            box-shadow: 0 2px 6px #fd9b96;
            background-color: #fc544b;
        }

        .swal-button.swal-button--pembayaran_gagal:hover {
            opacity: .8;
        }

    </style>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $(".modal-file-kk").on("hidden.bs.modal", function() {
                $(".modal-kk").html("");
                $(".modal-kk").html("<div class='embed-responsive embed-responsive-4by3 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->kartukeluarga->file_kk) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_kk/' . $warga->kartukeluarga->file_kk) }}' download>sini</a></div>");
            });

            $(".modal-file-ktp").on("hidden.bs.modal", function() {
                $(".modal-ktp").html("");
                $(".modal-ktp").html("<div class='embed-responsive embed-responsive-4by3 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_ktp/' . $warga->file_ktp) }}' download>sini</a></div>");
            });

            $(".modal-file-pernikahan").on("hidden.bs.modal", function() {
                $(".modal-pernikahan").html("@if (!empty($warga->kartukeluarga->file_pernikahan)) <div class='embed-responsive embed-responsive-16by9 d-none d-lg-block d-xl-block'><object data='{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->kartukeluarga->file_pernikahan) }}' type='application/pdf'></object></div><div class='alert alert-warning d-block d-sm-none'>Mohon Maaf. File Tidak Dapat Ditampilkan, Silahkan Unduh di <a href='{{ asset('storage/assets/pdf/dokumen_pernikahan/' . $warga->kartukeluarga->file_pernikahan) }}' download>sini</a></div>@else<div class='alert alert-danger alert-has-icon'><div class='alert-icon'><i class='fas fa-praying-hands'></i></div><div class='alert-body'><div class='alert-title'>Mohon Maaf</div>File dokumen ini tidak tersedia</div></div>@endif");
            });


            $('.konfirmasi-akun').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pengguna Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: {
                        pembayaran_sukses: {
                            text: "Konfirmasi Pengguna",
                            value: "Terkonfirmasi",
                        },
                        pembayaran_gagal: {
                            text: "Tolak Pengguna",
                            value: "Data Tidak Sesuai Ketentuan",
                        },
                        cancel: "Batal",
                    },
                }).then((value) => {
                    switch (value) {
                        case "Terkonfirmasi":
                            window.location.href = url + "/" + value;
                            break;
                        case "Data Tidak Sesuai Ketentuan":
                            window.location.href = url + "/" + value;
                            break;
                        default:
                            break;
                    }
                });
            });

        });
    </script>
@endpush
