@extends("layouts.app")

@section('title', 'Daftar Pengguna')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Pengelolaan Pengguna Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <a href="{{ route('pengguna.create') }}" class="btn btn-block btn-icon icon-left btn-success">
                        <i class="fas fa-plus-circle"></i> Tambah Data Pengguna
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables">
                            <thead>
                                <tr>
                                    <th>No KK</th>
                                    <th>Nama Pengguna</th>
                                    <th>Peran</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pengguna as $data_pengguna)
                                    <tr>
                                        <td>{{ $data_pengguna->kartukeluarga->no_kk }}</td>
                                        <td>{{ $data_pengguna->nama_pengguna }}</td>
                                        <td>{{ $data_pengguna->peran }}</td>
                                        <td>{{ $data_pengguna->status_pengguna }}</td>
                                        <td>
                                            @if ($data_pengguna->nama_pengguna !== 'role-Administrator')
                                                <a href="{{ route('pengguna.update_status_akun', $data_pengguna->id_pengguna) }}" class="btn btn-sm btn-icon @if($data_pengguna->status_akun === 'Data Tidak Sesuai Ketentuan') btn-danger @else btn-primary @endif mr-1 ml-1 mb-1 konfirmasi-akun @if ($data_pengguna->status_akun === 'Terkonfirmasi') disabled @elseif($data_pengguna->status_akun === 'Data Tidak Sesuai Ketentuan') btn-danger @endif">
                                                    <i class="far fa-file-alt"></i> Konfirmasi Akun
                                                </a>
                                                <a href="{{ route('pengguna.show', $data_pengguna->id_pengguna) }}" class="btn btn-sm btn-icon btn-info mr-1 ml-1 mb-1">
                                                    <i class="far fa-eye hidden-lg"></i> Lihat
                                                </a>
                                                <a href="{{ route('pengguna.edit', $data_pengguna->id_pengguna) }}" class="btn btn-sm btn-icon btn-warning mr-1 ml-1 mb-1">
                                                    <i class="far fa-edit "></i> Ubah
                                                </a>
                                                <form style="all: unset" action="{{ route('pengguna.destroy', $data_pengguna->id_pengguna) }}" id="form_delete_{{ $data_pengguna->id_pengguna }}" data-id="{{ $data_pengguna->nama_pengguna }}" method="POST">
                                                    @method("DELETE")
                                                    @csrf
                                                    <button type="button" onclick="deleteconfirm('form_delete_{{ $data_pengguna->id_pengguna }}')" class="btn btn-sm btn-icon btn-danger mr-1 ml-1 mb-1"><i class="far fa-trash-alt"></i>
                                                        Hapus
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('css-template')
    <style>
        .swal-button.swal-button--pembayaran_sukses {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #6777ef;
        }
        .swal-button.swal-button--pembayaran_sukses:hover {
            opacity: .8;
        }
        .swal-button.swal-button--pembayaran_gagal {
            box-shadow: 0 2px 6px #fd9b96;
            background-color: #fc544b;
        }
        .swal-button.swal-button--pembayaran_gagal:hover {
            opacity: .8;
        }
    </style>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data Pengguna';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "40%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });

            $('.konfirmasi-akun').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pengguna Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: {
                        pembayaran_sukses: {
                            text: "Konfirmasi Pengguna",
                            value: "Terkonfirmasi",
                        },
                        pembayaran_gagal: {
                            text: "Tolak Pengguna",
                            value: "Data Tidak Sesuai Ketentuan",
                        },
                        cancel: "Batal",
                    },
                }).then((value) => {
                    switch (value) {
                        case "Terkonfirmasi":
                            window.location.href = url + "/" + value;
                            break;
                        case "Data Tidak Sesuai Ketentuan":
                            window.location.href = url + "/" + value;
                            break;
                        default:
                            break;
                    }
                });
            });

        });

        function deleteconfirm(form) {
            var id = $('#' + form).attr('data-id');
            swal({
                title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
                text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                icon: 'warning',
                buttons: ["Batal", "Ya, Hapus Data!"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + form).submit();
                }
            });
        }
    </script>
@endpush
