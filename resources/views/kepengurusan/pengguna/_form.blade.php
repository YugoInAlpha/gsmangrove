<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>No. KK <strong class="text-danger">*</strong></label>
                    <select name="no_kk" id="no_kk" class="form-control select2 @error('no_kk') is-invalid @enderror @error('id_kartu_keluarga') is-invalid @enderror">
                        <option value="">Pilih</option>
                        @foreach ($kartu_keluarga as $data_kk)
                            <option value="{{ $data_kk->no_kk }}" data-id="{{ $data_kk->id_kartu_keluarga }}" @if (old('no_kk', $pengguna->kartukeluarga->no_kk ?? '') == $data_kk->no_kk) selected @endif>{{ $data_kk->no_kk }}</option>
                        @endforeach
                    </select>
                    <input type="hidden" id="id_kartu_keluarga" class="form-control @error('id_kartu_keluarga') is-invalid @enderror" name="id_kartu_keluarga" value="{{ old('id_kartu_keluarga', $pengguna->id_kartu_keluarga) }}" readonly>
                    <input type="hidden" id="old_id_kartu_keluarga" class="form-control @error('old_id_kartu_keluarga') is-invalid @enderror" name="old_id_kartu_keluarga" value="{{ old('old_id_kartu_keluarga', $pengguna->id_kartu_keluarga) }}" readonly>
                    @error('no_kk')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                    @error('id_kartu_keluarga')
                        <div class="invalid-feedback">
                            sudah terdapat akun yang menggunakan No. KK ini
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Nama Pengguna <strong class="text-danger">*</strong></label>
                    <input type="text" id="nama_pengguna" class="form-control  @error('nama_pengguna') is-invalid @enderror" name="nama_pengguna" value="{{ old('nama_pengguna', $pengguna->nama_pengguna) }}">
                    <div class="invalid-feedback">
                        @error('nama_pengguna')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label class="d-block">Kata Sandi @if ($model_form['model'] == 'edit') (opsional) @else <strong class="text-danger">*</strong>@endif</label>
                    <div class="input-group" id="show_hide_password">
                        <div class="input-group-append">
                            <div class="input-group-text @error('kata_sandi') border border-danger @enderror">
                                <a href="" tabindex="-1"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <input type="password" id="kata_sandi" class="form-control pwstrength @error('kata_sandi') is-invalid @enderror" data-indicator="pwindicator" name="kata_sandi" value="{{ old('kata_sandi') }}">
                        <div class="invalid-feedback">
                            @error('kata_sandi')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    @if ($model_form['model'] == 'edit')
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="ganti_password" class="custom-control-input" tabindex="3" id="ganti_password" @if (old('ganti_password') == 'on') checked @endif>
                            <label class=" custom-control-label" for="ganti_password"><strong>Setuju</strong> ubah kata sandi pengguna</label>
                        </div>
                    @endif
                </div>

                <div class=" form-group">
                    <label>Peran</label>
                    <select name="peran" class="form-control select2 @error('peran') is-invalid @enderror">
                        <option value="">Pilih</option>
                        <option value="Warga" @if (old('peran', $pengguna->peran) == 'Warga') selected @enderror>Warga</option>
                        <option value="Pihak Kepengurusan GSM" @if (old('peran', $pengguna->peran) == 'Pihak Kepengurusan GSM') selected @enderror>Pihak Kepengurusan GSM</option>
                    </select>
                    @error('peran')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="row">
                    <div id="form_jabatan" class="form-group col-12">
                        <label class="form-label">Jabatan <strong class="text-danger">*</strong></label>
                        @php
                            $jabatan = ['Warga', 'Ketua RT', 'Wakil Ketua RT', 'Sekretaris', 'Bendahara', 'Sarana & prasarana', 'Keamanan & Ketertiban', 'Kebersihan Lingkungan', 'Pemuda & olahraga', 'Koordinator Cluster'];
                        @endphp
                        <select name="jabatan" id="jabatan" class="form-control select2 @error('jabatan') is-invalid @enderror">
                            <option value="">Pilih</option>
                            @php
                                if ($model_form['model'] == 'edit') {
                                    $match_data = false;
                                } else {
                                    $match_data = true;
                                }
                            @endphp
                            @foreach ($jabatan as $data_jabatan)
                                <option value="{{ $data_jabatan }}" @if (old('jabatan', $pengguna->jabatan) == $data_jabatan) selected @php $match_data = true; @endphp @endif>{{ $data_jabatan }}</option>
                            @endforeach
                            @if ($match_data == false)
                                <option value="Lain-lain" @if (old('jabatan') == 'Lain-lain' || $match_data == false) selected @endif>Lain-lain</option>
                            @else
                                <option value="Lain-lain" @if (old('jabatan') == 'Lain-lain') selected @endif>Lain-lain</option>
                            @endif
                        </select>
                        <div class="invalid-feedback">
                            @error('jabatan')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="form-group col-12 d-none" id="form_input_lain">
                        <label class="form-label">jabatan Lain <strong class="text-danger">*</strong></label>
                        <input type="text" name="jabatan_lain" id="jabatan_lain" class="form-control @error('jabatan_lain') is-invalid @enderror" value="{{ old('jabatan_lain', $pengguna->jabatan) }}" placeholder="Silahkan Mengisi Data jabatan Lain">
                        <div class="invalid-feedback">
                            @error('jabatan_lain')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>

                @if ($model_form['model'] == 'edit')
                    <div class="form-group">
                        <label>Status Pengguna</label>
                        <select name="status_pengguna" class="form-control select2 @error('status_pengguna') is-invalid @enderror">
                            <option value="">Pilih</option>
                            <option value="Aktif" @if (old('status_pengguna', $pengguna->status_pengguna) === 'Aktif') selected @endif>Aktif</option>
                            <option value="Tidak Aktif" @if (old('status_pengguna', $pengguna->status_pengguna) === 'Tidak Aktif') selected @endif>Tidak AKtif</option>
                        </select>
                        @error('status_pengguna')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="card-footer text-left">
                <button type="submit" class="btn {{ $model_form['btn'] }}">{{ $model_form['submit'] }}</button>
            </div>
        </div>
    </div>
</div>

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/cleave-js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('assets/modules/cleave-js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-pwstrength/jquery.pwstrength.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });

            $('#ganti_password').click(function() {
                if ($(this).prop('checked')) {
                    var nama_pengguna = $("#nama_pengguna").val();
                    $("#kata_sandi").val(nama_pengguna);
                } else {
                    $("#kata_sandi").val('');
                }
            });

            var id_kartu_keluarga = $('#no_kk').find(':selected').data('id');
            $("#id_kartu_keluarga").val(id_kartu_keluarga);

            $('#no_kk').on('change', function() {
                var id_kartu_keluarga = $(this).find(':selected').data('id');
                $("#id_kartu_keluarga").val(id_kartu_keluarga);
            });

            var data = $('#jabatan option:selected').filter(':selected').val();
            if (data === 'Lain-lain') {
                $('#form_input_lain').removeClass('d-none');
                $('#form_jabatan').addClass('col-md-6 col-lg-6');
            } else {
                $('#form_input_lain').addClass('d-none');
                $('#form_jabatan').removeClass('col-md-6 col-lg-6');
            }

            $('#jabatan').change(function() {
                if ($(this).val() === 'Lain-lain') {
                    $('#form_input_lain').removeClass('d-none');
                    $('#form_jabatan').addClass('col-md-6 col-lg-6');
                } else {
                    $('#form_input_lain').addClass('d-none');
                    $('#form_jabatan').removeClass('col-md-6 col-lg-6');
                }
            });
        });
    </script>
@endpush
