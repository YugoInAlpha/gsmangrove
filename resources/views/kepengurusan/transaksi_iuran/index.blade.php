@extends("layouts.app")

@section('title', 'Daftar Transaksi Iuran')
@section('content')
    <h2 class="section-title">@yield("title")</h2>
    <p class="section-lead">
        Halaman Manajemen Iuran Green Semanggi Mangrove
    </p>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div style="overflow: hidden;">
                        <table class="table table-striped" id="datatables-warga">
                            <thead>
                                <tr>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Warga</th>
                                    <th>Periode Pembayaran</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transaksi_iuran as $data_transaksi)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($data_transaksi->tanggal_transaksi)->isoFormat('dddd, D MMMM Y') }}
                                        </td>
                                        <td>{{ $data_transaksi->warga->nama_lengkap }}</td>
                                        <td>{{ $data_transaksi->periode_pembayaran }} Bulan</td>
                                        @php
                                            if ($data_transaksi->status_pembayaran == 'Menunggu Konfirmasi') {
                                                $kondisi = 'badge-warning';
                                            } elseif ($data_transaksi->status_pembayaran == 'Pembayaran Sukses') {
                                                $kondisi = 'badge-success';
                                            } elseif ($data_transaksi->status_pembayaran == 'Pembayaran Gagal') {
                                                $kondisi = 'badge-danger';
                                            }
                                        @endphp
                                        <td class="font-weight-600"><span
                                                class="badge {{ $kondisi }}">{{ $data_transaksi->status_pembayaran }}</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('iuran_warga.show', $data_transaksi->id_transaksi_iuran) }}"
                                                class="btn btn-sm btn-icon btn-info m-1">
                                                <i class="far fa-eye hidden-lg"></i> Lihat
                                            </a>
                                            @if ($data_transaksi->status_pembayaran != 'Menunggu Konfirmasi')
                                                @php
                                                    $status = 'disabled';
                                                @endphp
                                            @else
                                                @php
                                                    $status = '';
                                                @endphp
                                            @endif
                                            <a href="{{ route('konfirmasi_iuran_warga.edit', [$data_transaksi->id_transaksi_iuran, $data_transaksi->id_warga]) }}"
                                                class="btn btn-sm btn-icon btn-warning konfirmasi-pembayaran m-1 {{ $status }}">
                                                <i class="far fa-file-alt"></i> Konfirmasi Pembayaran
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
@endpush

@push('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
@endpush

@push('css-template')
    <style>
        .swal-button.swal-button--pembayaran_sukses {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #6777ef;
        }

        .swal-button.swal-button--pembayaran_sukses:hover {
            opacity: .8;
        }

        .swal-button.swal-button--pembayaran_gagal {
            box-shadow: 0 2px 6px #fd9b96;
            background-color: #fc544b;
        }

        .swal-button.swal-button--pembayaran_gagal:hover {
            opacity: .8;
        }

    </style>
@endpush

@push('js-template')
    <script>
        $(document).ready(function() {
            $("#datatables-warga").dataTable({
                responsive: true,
                paging: true,
                ordering: true,
                info: true,
                filter: true,
                length: true,
                processing: true,
                deferRender: true,
                autoWidth: false,
                language: {
                    url: "{{ asset('assets/modules/datatables/lang/id.json') }}"
                },
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Data Transaksi Iuran ';
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll()
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: true
                    },
                    {
                        "width": "28%",
                        "targets": -1
                    },
                    {
                        orderable: false,
                        targets: -1
                    },
                ],
            });

            $('.konfirmasi-pembayaran').on('click', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Apakah Anda Yakin Mengkonfirmasi Pembayaran Ini ?',
                    text: 'Anda Tidak Akan Dapat Mengembalikannya!',
                    icon: 'warning',
                    buttons: {
                        pembayaran_sukses: {
                            text: "Terima Pembayaran",
                            value: "Pembayaran Sukses",
                        },
                        pembayaran_gagal: {
                            text: "Tolak Pembayaran",
                            value: "Pembayaran Gagal",
                        },
                        cancel: "Batal",
                    },
                }).then((value) => {
                    switch (value) {
                        case "Pembayaran Sukses":
                            window.location.href = url + "/" + value;
                            break;
                        case "Pembayaran Gagal":
                            window.location.href = url + "/" + value;
                            break;
                        default:
                            break;
                    }
                });
            });
        });
    </script>
@endpush
