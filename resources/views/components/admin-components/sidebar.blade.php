@if (Auth::user()->peran == 'Warga' || session()->get('view_data.0.view') == 'Warga')
    @php
        $informasi = ['dashboardV2'];
        $transaksi_iuran = ['transaksi_iuran.index', 'transaksi_iuran.show', 'transaksi_iuran.create', 'transaksi_iuran_bulan'];
        
        $perumahan = ['rumah.index', 'rumah.create', 'rumah.edit', 'rumah.show'];
        $anggota_keluarga = ['anggota_keluarga.index', 'anggota_keluarga.show', 'anggota_keluarga.edit', 'anggota_keluarga.create', 'kartu_keluarga.edit'];
    @endphp
    <div class="position-fixed main-sidebar sidebar-style-1">
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="#">{{ config('app.name') }}</a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="index.html">{{ substr(config('app.name'), 0, 3) }}</a>
            </div>
            <ul class="sidebar-menu">
                <li class="{{ set_active($informasi[0]) }}">
                    <a class="nav-link" href="{{ route($informasi[0]) }}"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                </li>
                <li class="menu-header">Pengelolaan Data Diri</li>
                <li class="{{ set_active($perumahan) }}">
                    <a class="nav-link" href="{{ route($perumahan[0]) }}"><i class="fas fa-home"></i></i><span>Daftar Perumahan</span></a>
                </li>
                <li class="{{ set_active($anggota_keluarga) }}">
                    <a class="nav-link" href="{{ route($anggota_keluarga[0]) }}"><i class="fas fa-users"></i><span>Anggota Keluarga</span></a>
                </li>
                <li class="menu-header">Pengelolaan Keuangan Iuran</li>
                <li class="{{ set_active($transaksi_iuran) }}">
                    <a class="nav-link" href="{{ route($transaksi_iuran[0]) }}"><i class="fas fa-money-bill"></i><span>Transaksi Iuran</span></a>
                </li>
            </ul>
        </aside>
    </div>
@else
    @php
        $informasi = ['dashboardV1'];
        
        $kartu_keluarga = ['kartu_keluarga_warga.index', 'kartu_keluarga_warga.create', 'kartu_keluarga_warga.show', 'kartu_keluarga_warga.edit'];
        $warga = ['warga.index', 'warga.show', 'warga.create', 'warga.edit', 'warga.create_spesifik'];
        $perumahan = ['perumahan.index', 'perumahan.show', 'perumahan.create', 'perumahan.edit', 'cluster.index', 'cluster.create', 'cluster.edit', 'rumah_warga.create', 'rumah_warga.edit'];
        
        $pencarian_iuran = ['pencarian.index', 'pencarian.search', 'pencarian.show'];
        $metode_pembayaran = ['metode_pembayaran.index', 'metode_pembayaran.create', 'metode_pembayaran.edit'];
        $iuran = ['iuran_warga.index', 'iuran_warga.show'];
        $laporan_iuran = ['laporan_iuran.index', 'laporan_iuran.laporan_bulanan', 'laporan_iuran.laporan_tahunan'];
        
        $pengguna = ['pengguna.index', 'pengguna.show', 'pengguna.create', 'pengguna.edit'];
    @endphp

    <div class="position-fixed main-sidebar sidebar-style-1">
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="#">{{ config('app.name') }}</a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="index.html">{{ substr(config('app.name'), 0, 3) }}</a>
            </div>
            <ul class="sidebar-menu">
                <li class="{{ set_active($informasi[0]) }}">
                    <a class="nav-link" href="{{ route($informasi[0]) }}"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                </li>
                <li class="menu-header">Pengelolaan Data Warga</li>
                <li class="{{ set_active($kartu_keluarga) }}">
                    <a class="nav-link" href="{{ route($kartu_keluarga[0]) }}"><i class="fas fa-file"></i><span>Daftar Kartu Keluarga</span></a>
                </li>
                <li class="{{ set_active($warga) }}">
                    <a class="nav-link" href="{{ route($warga[0]) }}"><i class="fas fa-users"></i><span>Daftar Warga</span></a>
                </li>
                <li class="{{ set_active($perumahan) }}">
                    <a class="nav-link" href="{{ route('perumahan.index') }}"><i class="fas fa-home"></i><span>Daftar Perumahan</span></a>
                </li>
                <li class="menu-header">Pengelolaan Keuangan Iuran</li>
                <li class="{{ set_active($pencarian_iuran) }}">
                    <a class="nav-link" href="{{ route($pencarian_iuran[0]) }}"><i class="fas fa-search"></i><span>Pencarian</span></a>
                </li>
                <li class="{{ set_active($metode_pembayaran) }}">
                    <a class="nav-link" href="{{ route($metode_pembayaran[0]) }}"><i class="fas fa-money-check"></i><span>Metode Pembayaran</span></a>
                </li>
                <li class="{{ set_active($iuran) }}">
                    <a class="nav-link" href="{{ route($iuran[0]) }}"><i class="fas fa-money-bill"></i><span>Daftar Transaksi Iuran</span></a>
                </li>
                <li class="{{ set_active($laporan_iuran) }}">
                    <a class="nav-link" href="{{ route($laporan_iuran[0]) }}"><i class="fas fa-file-alt"></i><span>Laporan Transaksi Iuran</span></a>
                </li>
                <li class="menu-header">Manajemen Akun</li>
                <li class="{{ set_active($pengguna) }}">
                    <a class="nav-link" href="{{ route($pengguna[0]) }}"><i class="fas fa-user-circle"></i><span>Pengguna Aplikasi</span></a>
                </li>
            </ul>
        </aside>
    </div>
@endif
