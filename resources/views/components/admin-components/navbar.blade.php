<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <div class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
        </ul>
    </div>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <div class="d-sm-none d-lg-inline-block">Hi,@auth {{ auth()->user()->nama_pengguna }} @else Pengguna @endauth</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-title">Akses : {{ auth()->user()->peran }}</div>
                <a href="{{ route('profil.index', auth()->user()->id_pengguna) }}" class="dropdown-item has-icon">
                    <i class="far fa-user"></i> Profil
                </a>
                @if (auth()->user()->peran == 'Pihak Kepengurusan GSM')
                    <a href="{{ route('akses.kepengurusan') }}" class="dropdown-item has-icon">
                        <i class="fas fa-user-tie"></i> Tampilan Kepengurusan
                    </a>
                    <a href="{{ route('akses.warga') }}" class="dropdown-item has-icon">
                        <i class="fas fa-user-alt"></i> Tampilan Warga
                    </a>
                @endif
                <div class="dropdown-divider"></div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> Logout
                    </a>
                </form>

            </div>
        </li>
    </ul>
</nav>
