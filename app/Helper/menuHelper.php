<?php
use Illuminate\Support\Facades\Route;

if (!function_exists('set_Active')) {
    function set_Active($uri, $output = 'active')
    {
        if (is_array($uri)) {
            foreach ($uri as $i) {
                if (Route::is($i)) {
                    return $output;
                }
            }
        } else {
            if (Route::is($uri)) {
                return $output;
            }
        }
    }
}