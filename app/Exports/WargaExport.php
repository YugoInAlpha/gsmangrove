<?php

namespace App\Exports;

use App\Models\perumahanwargaModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class WargaExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping, WithColumnFormatting, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $id_cluster;
    private $blok_perumahan;

    public function __construct(int $id_cluster, string $blok_perumahan)
    {
        $this->id_cluster = $id_cluster;
        $this->blok_perumahan = $blok_perumahan;
    }

    public function headings(): array
    {
        return [
            'Alamat Rumah',
            'No KK',
            'NIK',
            'Nama Lengkap',
            'Nomor Telepon',
            'Jenis Kelamin',
            'Agama',
            'Tempat Lahir',
            'Tanggal Lahir',
            'Golongan Darah',
            'Status Kawin',
            'Status Hubungan Keluarga',
            'File Kartu Keluarga',
            'File KTP',
            'File Pernikahan',
        ];
    }

    public function collection()
    {
        try {
            $warga = perumahanwargaModel::join("kartu_keluarga", "perumahan_warga.id_kartu_keluarga", "kartu_keluarga.id_kartu_keluarga")
                ->join("warga", "kartu_keluarga.id_kartu_keluarga", "warga.id_kartu_keluarga")
                ->join("perumahan", "perumahan_warga.id_perumahan", "perumahan.id_perumahan")
                ->join("cluster", "cluster.id_cluster", "perumahan.id_cluster")
                ->where("cluster.id_cluster", "=", $this->id_cluster)
                ->where("perumahan.blok_perumahan", "=", $this->blok_perumahan)
                ->where("warga.status_hubungan_keluarga", "Kepala Keluarga")->get();

            return $warga;
        } catch (\Throwable$th) {
            return redirect()->back();
        }
    }

    public function map($warga): array
    {
        return [
            $warga->nama_cluster . ' ' . $warga->blok_perumahan . '-' . $warga->nama_perumahan,
            (string) $warga->no_kk . " ",
            (string) $warga->nik . " ",
            $warga->nama_lengkap,
            (string) $warga->nomor_telepon . " ",
            $warga->jenis_kelamin,
            $warga->agama,
            $warga->tempat_lahir,
            $warga->tanggal_lahir,
            $warga->golongan_darah,
            $warga->status_kawin,
            $warga->status_hubungan_keluarga,
            $warga->file_kk,
            $warga->file_ktp,
            $warga->file_pernikahan,
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function title(): string
    {
        return 'Blok ' . $this->blok_perumahan;
    }
}
