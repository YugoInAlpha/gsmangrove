<?php

namespace App\Exports;

use App\Models\perumahanModel;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class WargaMultiExport implements WithMultipleSheets
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $id_cluster;

    public function __construct(int $id_cluster)
    {
        $this->id_cluster = $id_cluster;
    }

    public function sheets(): array
    {
        try {
            $sheets = [];
            $blok = perumahanModel::select("blok_perumahan")->where("id_cluster", $this->id_cluster)->distinct()->get();

            foreach ($blok as $data_blok) {
                $sheets[] = new WargaExport($this->id_cluster, $data_blok->blok_perumahan);
            }

            return $sheets;
        } catch (\Throwable$th) {
            return redirect()->back();
        }
    }
}
