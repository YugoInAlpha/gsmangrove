<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class KepengurusanAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->peran == "Pihak Kepengurusan GSM" || auth()->user()->peran == "Administrator") {
            return $next($request);
        }
        return redirect()->back()->withToastWarning('Maaf, Anda Tidak Memiliki Akses');
    }
}
