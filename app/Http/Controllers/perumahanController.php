<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use App\Models\perumahanModel;
use Illuminate\Http\Request;

class perumahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cluster = clusterModel::get();
        $perumahan = perumahanModel::with("cluster")->get();
        return view("kepengurusan.perumahan.index", compact("cluster", "perumahan"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perumahan = new perumahanModel();
        $cluster = clusterModel::get();

        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("kepengurusan.perumahan.create", compact("perumahan", "cluster", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $periode_kontrak = null;
        $periode_waktu = null;
        $tanggal_mulai_kontrak = null;
        $tanggal_akhir_kontrak = null;

        if ($request->kepemilikan_rumah == "Lain-lain") {
            $request->validate([
                "id_cluster" => "required",
                "blok_rumah" => "required|max:10",
                "nomor_rumah" => "required|max:10",
                "kepemilikan_rumah" => "required",
                "lain_lain" => "required",
            ]);

            $kepemilikan_rumah = $request->lain_lain;
        } elseif ($request->kepemilikan_rumah == "Kontrak/Sewa") {
            $request->validate([
                "id_cluster" => "required",
                "blok_rumah" => "required|max:10",
                "nomor_rumah" => "required|max:10",
                "kepemilikan_rumah" => "required",
                "tanggal_mulai_kontrak_sewa" => "required",
                "periode_kontrak_sewa" => "required",
                "periode_waktu" => "required",
            ]);
            $kepemilikan_rumah = $request->kepemilikan_rumah;

            $periode_kontrak = $request->periode_kontrak_sewa;
            $periode_waktu = $request->periode_waktu;
            $tanggal_mulai_kontrak = $request->tanggal_mulai_kontrak_sewa;

            $tanggal_akhir_kontrak = date('Y-m-d', strtotime('+' . $periode_kontrak . ' ' . $periode_waktu, strtotime($tanggal_mulai_kontrak)));

        } else {
            $request->validate([
                "id_cluster" => "required",
                "blok_rumah" => "required|max:10",
                "nomor_rumah" => "required|max:10",
                "kepemilikan_rumah" => "required",
            ]);

            $kepemilikan_rumah = $request->kepemilikan_rumah;
        }

        $perumahan = [
            "id_cluster" => $request->id_cluster,
            "blok_perumahan" => $request->blok_rumah,
            "nama_perumahan" => $request->nomor_rumah,
            "kepemilikan_rumah" => $kepemilikan_rumah,
            "periode_kontrak" => $periode_kontrak,
            "periode_waktu" => $periode_waktu,
            "tanggal_mulai_kontrak" => $tanggal_mulai_kontrak,
            "tanggal_akhir_kontrak" => $tanggal_akhir_kontrak,
        ];

        perumahanModel::create($perumahan);

        return redirect()->route("perumahan.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(perumahanModel $perumahan)
    {
        $cluster = clusterModel::get();

        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("kepengurusan.perumahan.edit", compact("perumahan", "cluster", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, perumahanModel $perumahan)
    {
        $periode_kontrak = null;
        $periode_waktu = null;
        $tanggal_mulai_kontrak = null;
        $tanggal_akhir_kontrak = null;

        if ($request->kepemilikan_rumah == "Lain-lain") {
            $request->validate([
                "id_cluster" => "required",
                "blok_rumah" => "required|max:10",
                "nomor_rumah" => "required|max:10",
                "kepemilikan_rumah" => "required",
                "status_perumahan" => "required|in:Aktif,Tidak Aktif",
                "lain_lain" => "required",
            ]);

            $kepemilikan_rumah = $request->lain_lain;
        } elseif ($request->kepemilikan_rumah == "Kontrak/Sewa") {
            $request->validate([
                "id_cluster" => "required",
                "blok_rumah" => "required|max:10",
                "nomor_rumah" => "required|max:10",
                "kepemilikan_rumah" => "required",
                "tanggal_mulai_kontrak_sewa" => "required",
                "periode_kontrak_sewa" => "required",
                "periode_waktu" => "required",
            ]);
            $kepemilikan_rumah = $request->kepemilikan_rumah;

            $periode_kontrak = $request->periode_kontrak_sewa;
            $periode_waktu = $request->periode_waktu;
            $tanggal_mulai_kontrak = $request->tanggal_mulai_kontrak_sewa;

            $tanggal_akhir_kontrak = date('Y-m-d', strtotime('+' . $periode_kontrak . ' ' . $periode_waktu, strtotime($tanggal_mulai_kontrak)));

        } else {
            $request->validate([
                "id_cluster" => "required",
                "blok_rumah" => "required|max:10",
                "nomor_rumah" => "required|max:10",
                "status_perumahan" => "required|in:Aktif,Tidak Aktif",
                "kepemilikan_rumah" => "required",
            ]);

            $kepemilikan_rumah = $request->kepemilikan_rumah;
        }

        $perumahan->update([
            "id_cluster" => $request->id_cluster,
            "blok_perumahan" => $request->blok_rumah,
            "nama_perumahan" => $request->nomor_rumah,
            "kepemilikan_rumah" => $kepemilikan_rumah,
            "periode_kontrak" => $periode_kontrak,
            "periode_waktu" => $periode_waktu,
            "tanggal_mulai_kontrak" => $tanggal_mulai_kontrak,
            "tanggal_akhir_kontrak" => $tanggal_akhir_kontrak,
            "status_perumahan" => $request->status_perumahan,
        ]);

        return redirect()->route("perumahan.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(perumahanModel $perumahan)
    {
        try {
            $perumahan->delete();
        } catch (\Throwable$th) {
            return redirect()->route("perumahan.index")->withToastWarning('Masih terdapat Warga yang menggunakan Perumahan');
        }
        return redirect()->route("perumahan.index")->withToastSuccess('Berhasil Menghapus Data');
    }
}
