<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use App\Models\perumahanModel;
use App\Models\perumahanwargaModel;
use Illuminate\Http\Request;

class rumahwargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $rumah = new perumahanModel();
        $perumahan = perumahanModel::join("cluster", "perumahan.id_cluster", "=", "cluster.id_cluster")
            ->where("cluster.status_cluster", "Aktif")
            ->where("perumahan.kepemilikan_rumah", "=", "Kosong")->get();
        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("kepengurusan.kartu_keluarga.rumah.create", compact("id", "rumah", "perumahan", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $query_perumahanwarga = perumahanwargaModel::query();

        $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
        $cari_cluster = clusterModel::findOrFail($cari_perumahan->id_cluster);

        if ($request->kepemilikan_rumah == "Lain-lain") {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
                "lain_lain" => "required",
            ]);

            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
                "periode_kontrak" => null,
                "periode_waktu" => null,
                "tanggal_mulai_kontrak" => null,
                "tanggal_akhir_kontrak" => null,
            ];

            $cari_perumahan->update($perumahan);

            $query_perumahanwarga->create([
                "id_perumahan" => $request->id_perumahan,
                "id_kartu_keluarga" => $id,
            ]);
        } else if ($request->kepemilikan_rumah == "Kontrak/Sewa") {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
                "tanggal_mulai_kontrak_sewa" => "required",
                "periode_kontrak_sewa" => "required",
                "periode_waktu" => "required",
            ]);

            $tanggal_mulai_kontrak = $request->tanggal_mulai_kontrak_sewa;
            $tanggal_akhir_kontrak = date('Y-m-d', strtotime('+' . $request->periode_kontrak_sewa . ' ' . $request->periode_waktu, strtotime($tanggal_mulai_kontrak)));
            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
                "periode_kontrak" => $request->periode_kontrak_sewa,
                "periode_waktu" => $request->periode_waktu,
                "tanggal_mulai_kontrak" => $tanggal_mulai_kontrak,
                "tanggal_akhir_kontrak" => $tanggal_akhir_kontrak,
            ];

            $cari_perumahan->update($perumahan);

            $query_perumahanwarga->create([
                "id_perumahan" => $request->id_perumahan,
                "id_kartu_keluarga" => $id,
            ]);
        } else {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
            ]);

            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
            ];

            $cari_perumahan->update($perumahan);

            $query_perumahanwarga->create([
                "id_perumahan" => $request->id_perumahan,
                "id_kartu_keluarga" => $id,
            ]);
        }

        if (auth()->user()->peran == "Warga" || auth()->user()->peran == "Pihak Kepengurusan GSM") {
            session()->push('kepala_keluarga.0.data_perumahan', [
                "id_cluster" => $cari_cluster->id_cluster,
                "nama_cluster" => $cari_cluster->nama_cluster,
                "id_perumahan" => $cari_perumahan->id_perumahan,
                "nama_perumahan" => $cari_perumahan->nama_perumahan,
                "status_dashboard" => "Tidak Aktif",
            ]);
        }

        return redirect()->route("kartu_keluarga_warga.show", $id)->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $id_kk)
    {
        try {
            perumahanwargaModel::where("id_perumahan", "=", $id)->where("id_kartu_keluarga", "=", $id_kk)->delete();

            $perumahan = perumahanModel::findorFail($id);
            $perumahan->update([
                "kepemilikan_rumah" => "Kosong",
                "periode_kontrak" => null,
                "periode_waktu" => null,
                "tanggal_mulai_kontrak" => null,
                "tanggal_akhir_kontrak" => null,
            ]);

            if (auth()->user()->peran == "Warga" || auth()->user()->peran == "Pihak Kepengurusan GSM") {
                foreach (session()->get('kepala_keluarga.0.data_perumahan') as $key => $data) {
                    if ($id == session()->get("kepala_keluarga.0.data_perumahan." . $key . ".id_perumahan")) {
                        session()->forget("kepala_keluarga.0.data_perumahan." . $key);
                    }
                }
            }
        } catch (\Throwable $th) {
            return redirect()->route("kartu_keluarga_warga.show", $id_kk)->withToastWarning('Terdapat Kesalahan Saat Menghapus Data');
        }
        return redirect()->route("kartu_keluarga_warga.show", $id_kk)->withToastSuccess('Berhasil Menghapus Data');
    }
}
