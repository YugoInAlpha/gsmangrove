<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use App\Models\kartukeluargaModel;
use App\Models\perumahanModel;
use Illuminate\Http\Request;

class rumahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perumahan = kartukeluargaModel::with("perumahan", "perumahan.cluster")->where("id_kartu_keluarga", session()->get('auth_data.0.id_kartu_keluarga'))->first();

        return view("pengguna.rumah.index", compact("perumahan"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rumah = new perumahanModel();
        $perumahan = perumahanModel::with(["cluster" => function ($query) {
            $query->where("status_cluster", "Aktif");
        }])->where("kepemilikan_rumah", "Kosong")->get();
        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("pengguna.rumah.create", compact("rumah", "perumahan", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->kepemilikan_rumah == "Lain-lain") {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
                "lain_lain" => "required",
            ]);

            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
                "periode_kontrak" => null,
                "periode_waktu" => null,
                "tanggal_mulai_kontrak" => null,
                "tanggal_akhir_kontrak" => null,
            ];

            $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
            $cari_perumahan->update($perumahan);

            $kartu_keluarga = kartukeluargaModel::findOrFail(auth()->user()->id_kartu_keluarga);
            $cari_cluster = clusterModel::findOrFail($cari_perumahan->id_cluster);

            $kartu_keluarga->perumahan()->attach($request->id_perumahan);

            session()->push('auth_data.0.data_perumahan', [
                "id_cluster" => $cari_cluster->id_cluster,
                "nama_cluster" => $cari_cluster->nama_cluster,
                "id_perumahan" => $cari_perumahan->id_perumahan,
                "nama_perumahan" => $cari_perumahan->nama_perumahan,
                "blok_perumahan" => $cari_perumahan->blok_perumahan,
                "status_dashboard" => "Tidak Aktif",
            ]);

        } else if ($request->kepemilikan_rumah == "Kontrak/Sewa") {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
                "tanggal_mulai_kontrak_sewa" => "required",
                "periode_kontrak_sewa" => "required",
                "periode_waktu" => "required",
            ]);

            $tanggal_mulai_kontrak = $request->tanggal_mulai_kontrak_sewa;
            $tanggal_akhir_kontrak = date('Y-m-d', strtotime('+' . $request->periode_kontrak_sewa . ' ' . $request->periode_waktu, strtotime($tanggal_mulai_kontrak)));
            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
                "periode_kontrak" => $request->periode_kontrak_sewa,
                "periode_waktu" => $request->periode_waktu,
                "tanggal_mulai_kontrak" => $tanggal_mulai_kontrak,
                "tanggal_akhir_kontrak" => $tanggal_akhir_kontrak,
            ];

            $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
            $cari_perumahan->update($perumahan);

            $kartu_keluarga = kartukeluargaModel::findOrFail(auth()->user()->id_kartu_keluarga);
            $cari_cluster = clusterModel::findOrFail($cari_perumahan->id_cluster);

            $kartu_keluarga->perumahan()->attach($request->id_perumahan);

            session()->push('auth_data.0.data_perumahan', [
                "id_cluster" => $cari_cluster->id_cluster,
                "nama_cluster" => $cari_cluster->nama_cluster,
                "id_perumahan" => $cari_perumahan->id_perumahan,
                "blok_perumahan" => $cari_perumahan->blok_perumahan,
                "nama_perumahan" => $cari_perumahan->nama_perumahan,
                "status_dashboard" => "Tidak Aktif",
            ]);
        } else {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
            ]);

            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
            ];

            $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
            $cari_perumahan->update($perumahan);

            $kartu_keluarga = kartukeluargaModel::findOrFail(auth()->user()->id_kartu_keluarga);
            $cari_cluster = clusterModel::findOrFail($cari_perumahan->id_cluster);

            $kartu_keluarga->perumahan()->attach($request->id_perumahan);

            session()->push('auth_data.0.data_perumahan', [
                "id_cluster" => $cari_cluster->id_cluster,
                "nama_cluster" => $cari_cluster->nama_cluster,
                "id_perumahan" => $cari_perumahan->id_perumahan,
                "nama_perumahan" => $cari_perumahan->nama_perumahan,
                "blok_perumahan" => $cari_perumahan->blok_perumahan,
                "status_dashboard" => "Tidak Aktif",
            ]);
        }

        return redirect()->route("rumah.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rumah = perumahanModel::with("cluster")->where("id_perumahan", $id)->firstOrFail();

        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];
        return view("pengguna.rumah.edit", compact("rumah", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->kepemilikan_rumah == "Lain-lain") {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
                "lain_lain" => "required",
            ]);

            $perumahan = [
                "kepemilikan_rumah" => $request->lain_lain,
                "periode_kontrak" => null,
                "periode_waktu" => null,
                "tanggal_mulai_kontrak" => null,
                "tanggal_akhir_kontrak" => null,
            ];

            $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
            $cari_perumahan->update($perumahan);

        } else if ($request->kepemilikan_rumah == "Kontrak/Sewa") {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
                "tanggal_mulai_kontrak_sewa" => "required",
                "periode_kontrak_sewa" => "required",
                "periode_waktu" => "required",
            ]);

            $tanggal_mulai_kontrak = $request->tanggal_mulai_kontrak_sewa;
            $tanggal_akhir_kontrak = date('Y-m-d', strtotime('+' . $request->periode_kontrak_sewa . ' ' . $request->periode_waktu, strtotime($tanggal_mulai_kontrak)));
            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
                "periode_kontrak" => $request->periode_kontrak_sewa,
                "periode_waktu" => $request->periode_waktu,
                "tanggal_mulai_kontrak" => $tanggal_mulai_kontrak,
                "tanggal_akhir_kontrak" => $tanggal_akhir_kontrak,
            ];

            $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
            $cari_perumahan->update($perumahan);

        } else {
            $request->validate([
                "id_perumahan" => "required",
                "kepemilikan_rumah" => "required",
            ]);

            $perumahan = [
                "kepemilikan_rumah" => $request->kepemilikan_rumah,
            ];

            $cari_perumahan = perumahanModel::findOrFail($request->id_perumahan);
            $cari_perumahan->update($perumahan);

        }

        return redirect()->route("rumah.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kartu_keluarga = kartukeluargaModel::findOrFail(auth()->user()->id_kartu_keluarga);
            $kartu_keluarga->perumahan()->detach($id);

            $cari_perumahan = perumahanModel::findOrFail($id);

            $perumahan = [
                "kepemilikan_rumah" => "Kosong",
                "periode_kontrak" => null,
                "periode_waktu" => null,
                "tanggal_mulai_kontrak" => null,
                "tanggal_akhir_kontrak" => null,
            ];

            $cari_perumahan->update($perumahan);

            for ($i = 0; $i < count(session()->get('auth_data.0.data_perumahan')); $i++) {
                if ($id == session()->get("auth_data.0.data_perumahan." . $i . ".id_perumahan")) {
                    session()->forget("auth_data.0.data_perumahan." . $i);
                }
            }
            return redirect()->route("rumah.index")->withToastSuccess('Berhasil Menghapus Data');
        } catch (\Throwable$th) {
            return redirect()->route("rumah.index")->withToastWarning('Terdapat kesalahan saat menghapus data');
        }
    }
}
