<?php

namespace App\Http\Controllers;

use App\Models\kartukeluargaModel;
use App\Models\penggunaModel;
use App\Models\perumahanModel;
use App\Models\perumahanwargaModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class registrasiController extends Controller
{
    public $dataRegistrasi = [];

    public function form_1()
    {
        $title_form = "Formulir-1";
        $form_register = "registrasi_form_1";

        return view("auth.registrasi", compact("form_register", "title_form"));
    }

    public function store_1(Request $request)
    {
        $request->validate([
            "no_kk" => "required|max:16|min:16|unique:kartu_keluarga,no_kk",
            "nama_pengguna" => "required|unique:pengguna,nama_pengguna",
            "kata_sandi" => "required",
            "jumlah_perumahan" => "required",
        ]);

        if (empty(session()->get("data_registrasi"))) {
            session()->push("data_registrasi", [
                "no_kk" => $request->no_kk,
                "nama_pengguna" => $request->nama_pengguna,
                "kata_sandi" => $request->kata_sandi,
                "jumlah_perumahan" => $request->jumlah_perumahan,
            ]);
        } else {
            session()->put("data_registrasi.0.no_kk", $request->no_kk);
            session()->put("data_registrasi.0.nama_pengguna", $request->nama_pengguna);
            session()->put("data_registrasi.0.kata_sandi", $request->kata_sandi);
            session()->put("data_registrasi.0.jumlah_perumahan", $request->jumlah_perumahan);
        }

        return redirect()->route("registrasi.form_2");
    }

    public function form_2()
    {
        $title_form = "Formulir-2";
        $form_register = "registrasi_form_2";

        $perumahan = perumahanModel::join("cluster", "perumahan.id_cluster", "=", "cluster.id_cluster")
            ->where("cluster.status_cluster", "Aktif")
            ->where("perumahan.kepemilikan_rumah", "=", "Kosong")->get();

        return view("auth.registrasi", compact("perumahan", "form_register", "title_form"));
    }

    public function store_2(Request $request)
    {
        if (session()->get('data_registrasi.0.jumlah_perumahan') == "1") {
            if ($request->kepemilikan_rumah[0] == "Lain-lain") {
                $request->validate([
                    "rumah.0" => "required",
                    "kepemilikan_rumah.0" => "required",
                    "lain_lain.0" => "required",
                ]);
            } else if ($request->kepemilikan_rumah[0] == "Kontrak/Sewa") {
                $request->validate([
                    "rumah.0" => "required",
                    "kepemilikan_rumah.0" => "required",
                    "tanggal_mulai_kontrak_sewa.0" => "required",
                    "periode_kontrak_sewa.0" => "required",
                    "periode_waktu.0" => "required",
                ]);
            } else {
                $request->validate([
                    "rumah.0" => "required",
                    "kepemilikan_rumah.0" => "required",
                ]);
            }
        } else {
            for ($i = 0; $i < session()->get('data_registrasi.0.jumlah_perumahan'); $i++) {
                if ($request->kepemilikan_rumah[$i] == "Lain-lain") {
                    for ($j = 0; $j < session()->get('data_registrasi.0.jumlah_perumahan'); $j++) {
                        if ($i !== $j) {
                            $request->validate([
                                "rumah.$i" => "required|different:rumah.$j",
                                "kepemilikan_rumah.$i" => "required",
                                "lain_lain.$i" => "required",
                            ]);
                        }
                    }
                } else if ($request->kepemilikan_rumah[$i] == "Kontrak/Sewa") {
                    for ($k = 0; $k < session()->get('data_registrasi.0.jumlah_perumahan'); $k++) {
                        if ($i !== $k) {
                            $request->validate([
                                "rumah.$i" => "required|different:rumah.$k",
                                "kepemilikan_rumah.$i" => "required",
                                "tanggal_mulai_kontrak_sewa.$i" => "required",
                                "periode_kontrak_sewa.$i" => "required",
                                "periode_waktu.$i" => "required",
                            ]);
                        }
                    }
                } else {
                    for ($l = 0; $l < session()->get('data_registrasi.0.jumlah_perumahan'); $l++) {
                        if ($i !== $l) {
                            $request->validate([
                                "rumah.$i" => "required|different:rumah.$l",
                                "kepemilikan_rumah.$i" => "required",
                            ]);
                        }
                    }
                }
            }
        }

        if (empty(session()->get("data_registrasi.0.data_perumahan"))) {
            for ($m = 0; $m < session()->get('data_registrasi.0.jumlah_perumahan'); $m++) {
                session()->push('data_registrasi.0.data_perumahan', [
                    "id_perumahan" => $request->rumah[$m],
                    "kepemilikan_rumah" => $request->kepemilikan_rumah[$m],
                    "lain_lain" => $request->lain_lain[$m],
                    "tanggal_mulai_kontrak_sewa" => $request->tanggal_mulai_kontrak_sewa[$m],
                    "periode_kontrak_sewa" => $request->periode_kontrak_sewa[$m],
                    "periode_waktu" => $request->periode_waktu[$m],
                ]);
            }
        } else {
            for ($m = 0; $m < session()->get('data_registrasi.0.jumlah_perumahan'); $m++) {
                session()->put("data_registrasi.0.data_perumahan." . $m . ".id_perumahan", $request->rumah[$m]);
                session()->put("data_registrasi.0.data_perumahan." . $m . ".kepemilikan_rumah", $request->kepemilikan_rumah[$m]);
                session()->put("data_registrasi.0.data_perumahan." . $m . ".lain_lain", $request->lain_lain[$m]);
                session()->put("data_registrasi.0.data_perumahan." . $m . ".tanggal_mulai_kontrak_sewa", $request->tanggal_mulai_kontrak_sewa[$m]);
                session()->put("data_registrasi.0.data_perumahan." . $m . ".periode_kontrak_sewa", $request->periode_kontrak_sewa[$m]);
                session()->put("data_registrasi.0.data_perumahan." . $m . ".periode_waktu", $request->periode_waktu[$m]);
            }
        }

        return redirect()->route("registrasi.form_3");
    }

    public function form_3()
    {
        $title_form = "Formulir-3";
        $form_register = "registrasi_form_3";

        return view("auth.registrasi", compact("form_register", "title_form"));
    }

    public function store_3(Request $request)
    {
        if ($request->agama == "Lain-lain") {
            $request->validate([
                "nik" => "required|max:16|min:16|unique:warga,nik",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "agama_lain" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required",
                "pekerjaan" => "required|max:50",
            ]);
        } else {
            $request->validate([
                "nik" => "required|max:16|min:16|unique:warga,nik",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required",
                "pekerjaan" => "required|max:50",
            ]);
        }

        $data_registrasi = session()->get("data_registrasi.0");

        if (!empty($data_registrasi)) {
            session()->put("data_registrasi.0.nik", $request->nik);
            session()->put("data_registrasi.0.nama_lengkap", $request->nama_lengkap);
            session()->put("data_registrasi.0.alamat_ktp", $request->alamat_ktp);
            session()->put("data_registrasi.0.jenis_kelamin", $request->jenis_kelamin);
            session()->put("data_registrasi.0.agama", ucfirst($request->agama));
            session()->put("data_registrasi.0.agama_lain", ucfirst($request->agama_lain));
            session()->put("data_registrasi.0.tempat_lahir", $request->tempat_lahir);
            session()->put("data_registrasi.0.tanggal_lahir", $request->tanggal_lahir);
            session()->put("data_registrasi.0.golongan_darah", $request->golongan_darah);
            session()->put("data_registrasi.0.nomor_telepon", $request->nomor_telepon);
            session()->put("data_registrasi.0.status_kawin", $request->status_kawin);
            session()->put("data_registrasi.0.pekerjaan", $request->pekerjaan);
        }

        return redirect()->route("registrasi.form_4");
    }

    public function form_4()
    {
        $title_form = "Formulir-4";
        $form_register = "registrasi_form_4";

        return view("auth.registrasi", compact("form_register", "title_form"));
    }

    public function store_4(Request $request)
    {
        $request->validate([
            "file_kk" => "required|mimes:jpeg,png,jpg,pdf",
            "file_ktp" => "required|mimes:jpeg,png,jpg,pdf",
            "file_pernikahan" => "nullable|mimes:jpeg,png,jpg,pdf",
        ]);

        //Insert Data Kartu_Keluarga
        if ($request->hasFile('file_pernikahan')) {
            $extension = $request->file('file_pernikahan')->getClientOriginalExtension();
            $file_nama_pernikahan = 'file_pernikahan_' . time() . '.' . $extension;
            $request->file('file_pernikahan')->storeAs('public/assets/pdf/dokumen_pernikahan', $file_nama_pernikahan);
        } else {
            $file_nama_pernikahan = null;
        }

        if ($request->hasFile('file_kk')) {
            $extension = $request->file('file_kk')->getClientOriginalExtension();
            $file_nama_kk = 'file_kk_' . time() . '.' . $extension;
            $request->file('file_kk')->storeAs('public/assets/pdf/dokumen_kk', $file_nama_kk);

            $kartu_keluarga = [
                "no_kk" => session()->get("data_registrasi.0.no_kk"),
                "file_kk" => $file_nama_kk,
                "file_pernikahan" => $file_nama_pernikahan,
            ];

            $data_kartu_keluarga = kartukeluargaModel::create($kartu_keluarga);
            $id_kartu_keluarga = $data_kartu_keluarga->id_kartu_keluarga;

            //Insert Data Pengguna
            $pengguna = [
                "id_kartu_keluarga" => $id_kartu_keluarga,
                "nama_pengguna" => session()->get("data_registrasi.0.nama_pengguna"),
                "kata_sandi" => Hash::make(session()->get("data_registrasi.0.kata_sandi")),
            ];

            penggunaModel::create($pengguna);
        }

        //Insert Data Warga
        if ($request->hasFile('file_ktp')) {
            $extension = $request->file('file_ktp')->getClientOriginalExtension();
            $file_nama_ktp = 'file_ktp_' . time() . '.' . $extension;
            $request->file('file_ktp')->storeAs('public/assets/pdf/dokumen_ktp', $file_nama_ktp);

            if (session()->get("data_registrasi.0.agama") == "Lain-lain") {
                $agama = session()->get("data_registrasi.0.agama_lain");
            } else {
                $agama = session()->get("data_registrasi.0.agama");
            }
            $warga = [
                "nik" => session()->get("data_registrasi.0.nik"),
                "id_kartu_keluarga" => $id_kartu_keluarga,
                "nama_lengkap" => session()->get("data_registrasi.0.nama_lengkap"),
                "alamat_ktp" => session()->get("data_registrasi.0.alamat_ktp"),
                "jenis_kelamin" => session()->get("data_registrasi.0.jenis_kelamin"),
                "agama" => $agama,
                "tempat_lahir" => session()->get("data_registrasi.0.tempat_lahir"),
                "tanggal_lahir" => session()->get("data_registrasi.0.tanggal_lahir"),
                "golongan_darah" => session()->get("data_registrasi.0.golongan_darah"),
                "nomor_telepon" => session()->get("data_registrasi.0.nomor_telepon"),
                "status_kawin" => session()->get("data_registrasi.0.status_kawin"),
                "pekerjaan" => session()->get("data_registrasi.0.pekerjaan"),
                "status_hubungan_keluarga" => "Kepala Keluarga",
                "file_ktp" => $file_nama_ktp,
            ];

            wargaModel::create($warga);
        }

        //Insert Data Perumahan_Warga
        for ($i = 0; $i < session()->get('data_registrasi.0.jumlah_perumahan'); $i++) {
            $id_perumahan = session()->get("data_registrasi.0.data_perumahan." . $i . ".id_perumahan");

            $perumahan_warga = [
                "id_perumahan" => $id_perumahan,
                "id_kartu_keluarga" => $id_kartu_keluarga,
            ];
            perumahanwargaModel::create($perumahan_warga);
        }

        //Update Data Perumahan
        for ($i = 0; $i < session()->get('data_registrasi.0.jumlah_perumahan'); $i++) {
            $id_perumahan = session()->get("data_registrasi.0.data_perumahan." . $i . ".id_perumahan");

            if (session()->get("data_registrasi.0.data_perumahan." . $i . ".kepemilikan_rumah") == "Kontrak/Sewa") {
                $tanggal_mulai_kontrak = session()->get("data_registrasi.0.data_perumahan." . $i . ".tanggal_mulai_kontrak_sewa");
                $tanggal_akhir_kontrak = date('Y-m-d', strtotime('+' . session()->get("data_registrasi.0.data_perumahan." . $i . ".periode_kontrak_sewa") . ' ' . session()->get("data_registrasi.0.data_perumahan." . $i . ".periode_waktu"), strtotime($tanggal_mulai_kontrak)));
                $perumahan = [
                    "kepemilikan_rumah" => session()->get("data_registrasi.0.data_perumahan." . $i . ".kepemilikan_rumah"),
                    "periode_kontrak" => session()->get("data_registrasi.0.data_perumahan." . $i . ".periode_kontrak_sewa"),
                    "periode_waktu" => session()->get("data_registrasi.0.data_perumahan." . $i . ".periode_waktu"),
                    "tanggal_mulai_kontrak" => $tanggal_mulai_kontrak,
                    "tanggal_akhir_kontrak" => $tanggal_akhir_kontrak,
                ];

                $cari_perumahan = perumahanModel::find($id_perumahan);
                $cari_perumahan->update($perumahan);
            } else if (session()->get("data_registrasi.0.data_perumahan." . $i . ".kepemilikan_rumah") == "Lain-lain") {

                $perumahan = [
                    "kepemilikan_rumah" => session()->get("data_registrasi.0.data_perumahan." . $i . ".lain_lain"),
                    "periode_kontrak" => null,
                    "periode_waktu" => null,
                    "tanggal_mulai_kontrak" => null,
                    "tanggal_akhir_kontrak" => null,
                ];

                $cari_perumahan = perumahanModel::find($id_perumahan);
                $cari_perumahan->update($perumahan);
            } else {
                $perumahan = [
                    "kepemilikan_rumah" => session()->get("data_registrasi.0.data_perumahan." . $i . ".kepemilikan_rumah"),
                ];

                $cari_perumahan = perumahanModel::find($id_perumahan);
                $cari_perumahan->update($perumahan);
            }
        }
        session()->forget("data_registrasi");
        return redirect()->route("login")->with('pesan_register', 'Akun Berhasil Dibuat, Untuk Mempercepat Proses Konfirmasi, Silahkan Menghubungi Pihak Kepengurusan')->withToastSuccess('Berhasil Membuat Akun');
    }
}
