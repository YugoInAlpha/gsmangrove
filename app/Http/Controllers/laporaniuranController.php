<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use App\Models\iuranModel;
use App\Models\perumahanModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class laporaniuranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cluster = clusterModel::get();

        return view("kepengurusan.laporan_iuran.index", compact("cluster"));
    }

    public function laporan_tahunan(Request $request)
    {
        $request->validate([
            "tahun" => "required",
            "cluster" => "required",
        ]);

        $data_laporan = [];

        $periode_tahun = $request->tahun;
        $bulan_sekarang = Carbon::now()->isoFormat('M');
        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $tanggal_awal = Carbon::create(Carbon::now()->year, 1, 1);
        $tanggal_akhir = Carbon::create(Carbon::now()->addYears(1), 1, 1);

        if ($request->cluster == "semua_cluster") {
            $data_iuran = iuranModel::with(["detailiuran" => function ($query) use ($periode_tahun) {
                $query->whereYear('periode_iuran_bulan', $periode_tahun)->get();
            }, "warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga")->first();
            }, "perumahan", "perumahan.cluster"])->whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->get();

            $rumah = perumahanModel::with(["cluster", "kartukeluarga", "kartukeluarga.warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga");
            }])->get();

        } else {
            $cluster = $request->cluster;

            $data_iuran = iuranModel::with(["detailiuran" => function ($query) use ($periode_tahun) {
                $query->whereYear('periode_iuran_bulan', $periode_tahun)->get();
            }, "warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga")->first();
            }, "perumahan", "perumahan.cluster"])->whereHas("perumahan.cluster", function ($query) use ($cluster) {
                $query->where("id_cluster", $cluster);
            })->whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->get();

            $rumah = perumahanModel::with(["kartukeluarga", "kartukeluarga.warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga");
            }, "cluster"])->whereHas("cluster", function ($query) use ($cluster) {
                $query->where("id_cluster", $cluster);
            })->get();
        }

        for ($i = 1; $i <= count($bulan); $i++) {
            $iuran[$i] = null;
        }

        foreach ($rumah as $key => $data_rumah) {
            $data_laporan[$periode_tahun][$data_rumah->cluster->nama_cluster][$data_rumah->blok_perumahan][$data_rumah->nama_perumahan] = [
                'id_warga' => isset($data_rumah->kartukeluarga->first()->warga) ? $data_rumah->kartukeluarga->first()->warga->first()->id_warga : "Kosong",
                'nama_warga' => isset($data_rumah->kartukeluarga->first()->warga) ? $data_rumah->kartukeluarga->first()->warga->first()->nama_lengkap : "Kosong",
                'perumahan' => $data_rumah->cluster->nama_cluster . ', ' . $data_rumah->blok_perumahan . ' - ' . $data_rumah->nama_perumahan,
                'status_rumah' => $data_rumah->kepemilikan_rumah,
                'iuran' => $iuran,
            ];
        }

        foreach ($data_iuran as $key_iuran => $iuran) {
            foreach ($iuran->detailiuran as $key_detail_iuran => $detail_iuran) {
                $bulan_transaksi = Carbon::parse($detail_iuran->periode_iuran_bulan)->isoFormat('M');

                if ($iuran->status_pembayaran == "Pembayaran Sukses") {
                    $data_laporan[$periode_tahun][$iuran->perumahan->cluster->nama_cluster][$iuran->perumahan->blok_perumahan][$iuran->perumahan->nama_perumahan]["iuran"][$bulan_transaksi] = [
                        "tanggal_transaksi" => $iuran->tanggal_transaksi,
                        "periode" => $detail_iuran->periode_iuran_bulan,
                        "status_iuran" => $iuran->status_pembayaran,
                        "biaya_iuran" => $detail_iuran->biaya_iuran,
                    ];
                } elseif($iuran->status_pembayaran == "Menunggu Konfirmasi") {
                    $data_laporan[$periode_tahun][$iuran->perumahan->cluster->nama_cluster][$iuran->perumahan->blok_perumahan][$iuran->perumahan->nama_perumahan]["iuran"][$bulan_transaksi] = [
                        "tanggal_transaksi" => $iuran->tanggal_transaksi,
                        "periode" => $detail_iuran->periode_iuran_bulan,
                        "status_iuran" => $iuran->status_pembayaran,
                        "biaya_iuran" => 0,
                    ];
                }

                if ($iuran->status_pembayaran == "Pembayaran Gagal") {
                    $data_laporan[$periode_tahun][$iuran->perumahan->cluster->nama_cluster][$iuran->perumahan->blok_perumahan][$iuran->perumahan->nama_perumahan]["iuran"][$bulan_transaksi] = [
                        "tanggal_transaksi" => $iuran->tanggal_transaksi,
                        "periode" => $detail_iuran->periode_iuran_bulan,
                        "status_iuran" => "Belum Dibayar",
                        "biaya_iuran" => 0,
                    ];
                }
            }
        }

        $data_blok_perumahan = perumahanModel::select("blok_perumahan")->distinct()->get();

        return view("kepengurusan.laporan_iuran.laporan_tahunan", compact("data_laporan", "bulan", "data_blok_perumahan"));
    }

    public function laporan_tahunan_blok(Request $request)
    {

    }

    public function laporan_bulanan(Request $request)
    {
        $request->validate([
            "bulan" => "required",
            "cluster" => "required",
        ]);

        $data_laporan = [];

        $bulan_awal = Carbon::create(Carbon::now()->year, $request->bulan, 1);
        $target = $bulan_awal->clone()->addMonth(2);

        $bulan = Carbon::parse($target)->isoFormat("M");
        $tahun = Carbon::now()->isoFormat("Y");

        $hari_target = Carbon::parse($target->endOfMonth())->isoFormat("D");
        $awal_target = $bulan_awal;
        $akhir_target = Carbon::create(Carbon::parse($target)->isoFormat("Y"), $bulan, $hari_target);

        $tanggal_awal = Carbon::create(Carbon::parse(Carbon::now()->subYears(1))->isoFormat("Y"), 01, 01);
        $tanggal_akhir = Carbon::create(Carbon::parse(Carbon::now()->addYears(1))->isoFormat("Y"), 01, 01);

        if ($request->cluster == "semua_cluster") {
            $data_iuran = iuranModel::with(["detailiuran" => function ($query) use ($awal_target, $akhir_target) {
                $query->whereBetween('periode_iuran_bulan', [$awal_target, $akhir_target])->get();
            }, "warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga")->first();
            }, "perumahan", "perumahan.cluster"])->whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->get();

            $rumah = perumahanModel::with(["cluster", "kartukeluarga", "kartukeluarga.warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga");
            }])->get();

        } else {
            $cluster = $request->cluster;

            $data_iuran = iuranModel::with(["detailiuran" => function ($query) use ($awal_target, $akhir_target) {
                $query->whereBetween('periode_iuran_bulan', [$awal_target, $akhir_target])->get();
            }, "warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga")->first();
            }, "perumahan", "perumahan.cluster"])->whereHas("perumahan.cluster", function ($query) use ($cluster) {
                $query->where("id_cluster", $cluster);
            })->whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->get();

            $rumah = perumahanModel::with(["kartukeluarga", "kartukeluarga.warga" => function ($query) {
                $query->where("status_hubungan_keluarga", "Kepala Keluarga");
            }, "cluster"])->whereHas("cluster", function ($query) use ($cluster) {
                $query->where("id_cluster", $cluster);
            })->get();
        }

        $period = \Carbon\CarbonPeriod::create($awal_target, '1 month', $akhir_target);
        $bulan = [];
        foreach ($period as $date) {
            $bulan[] = $date->format("Y-m");
        }

        for ($i = 0; $i < count($bulan); $i++) {
            $month = Carbon::parse($bulan[$i])->isoFormat("M");
            $iuran[$month] = null;
        }

        foreach ($rumah as $key => $data_rumah) {
            $data_laporan[$tahun][$data_rumah->cluster->nama_cluster][$data_rumah->blok_perumahan][$data_rumah->nama_perumahan] = [
                'id_warga' => isset($data_rumah->kartukeluarga->first()->warga) ? $data_rumah->kartukeluarga->first()->warga->first()->id_warga : "Kosong",
                'nama_warga' => isset($data_rumah->kartukeluarga->first()->warga) ? $data_rumah->kartukeluarga->first()->warga->first()->nama_lengkap : "Kosong",
                'perumahan' => $data_rumah->cluster->nama_cluster . ', ' . $data_rumah->blok_perumahan . ' - ' . $data_rumah->nama_perumahan,
                'status_rumah' => $data_rumah->kepemilikan_rumah,
                'iuran' => $iuran,
            ];
        }

        foreach ($data_iuran as $key_iuran => $iuran) {
            foreach ($iuran->detailiuran as $key_detail_iuran => $detail_iuran) {
                $bulan_transaksi = Carbon::parse($detail_iuran->periode_iuran_bulan)->isoFormat('M');

                if ($iuran->status_pembayaran == "Pembayaran Sukses") {
                    $data_laporan[$tahun][$iuran->perumahan->cluster->nama_cluster][$iuran->perumahan->blok_perumahan][$iuran->perumahan->nama_perumahan]["iuran"][$bulan_transaksi] = [
                        "tanggal_transaksi" => $iuran->tanggal_transaksi,
                        "periode" => $detail_iuran->periode_iuran_bulan,
                        "status_iuran" => $iuran->status_pembayaran,
                        "biaya_iuran" => $detail_iuran->biaya_iuran,
                    ];
                } else {
                    $data_laporan[$tahun][$iuran->perumahan->cluster->nama_cluster][$iuran->perumahan->blok_perumahan][$iuran->perumahan->nama_perumahan]["iuran"][$bulan_transaksi] = [
                        "tanggal_transaksi" => $iuran->tanggal_transaksi,
                        "periode" => $detail_iuran->periode_iuran_bulan,
                        "status_iuran" => $iuran->status_pembayaran,
                        "biaya_iuran" => 0,
                    ];
                }
                if ($bulan_transaksi <= $bulan && $iuran->status_pembayaran == "Pembayaran Gagal") {
                    $data_laporan[$tahun][$iuran->perumahan->cluster->nama_cluster][$iuran->perumahan->blok_perumahan][$iuran->perumahan->nama_perumahan]["iuran"][$bulan_transaksi] = [
                        "tanggal_transaksi" => $iuran->tanggal_transaksi,
                        "periode" => $detail_iuran->periode_iuran_bulan,
                        "status_iuran" => "Belum Dibayar",
                        "biaya_iuran" => 0,
                    ];
                }
            }
        }


        return view("kepengurusan.laporan_iuran.laporan_bulanan", compact("data_laporan", "bulan"));
    }
}
