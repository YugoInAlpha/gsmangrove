<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use App\Models\iuranModel;
use App\Models\penggunaModel;
use App\Models\perumahanModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class dashboardController extends Controller
{
    public function dashboardV1()
    {
        $query_warga = wargaModel::query();
        $query_pengguna = penggunaModel::query();
        $query_perumahan = perumahanModel::query();
        $query_iuran = iuranModel::query();

        $jumlah_warga = $query_warga->clone()->count();
        $jumlah_akun = $query_pengguna->clone()->count();
        $jumlah_perumahan = $query_perumahan->clone()->count();

        $data_pengguna = $query_pengguna->clone()->whereHas(
            'kartukeluarga', function ($query) {
                $query->select('id_kartu_keluarga');
            }
        )->whereHas(
            'kartukeluarga.warga', function ($query) {
                $query->select('id_kartu_keluarga', 'id_warga', 'nama_lengkap')->where("status_hubungan_keluarga", "Kepala Keluarga");
            }
        )->orderBy("pengguna.id_pengguna", "DESC")->where('peran', "!=", "Administrator")->with("kartukeluarga.warga")->with("kartukeluarga")->paginate(4);

        $data_transaksi = $query_iuran->clone()->with("warga")->orderBy("tanggal_transaksi", "DESC")->orderBy("id_transaksi_iuran", "DESC")->paginate(6);

        $tahun_sekarang = date('Y', strtotime(Carbon::now()));
        $label_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        for ($bulan = 1; $bulan <= count($label_bulan); $bulan++) {
            $grafik_iuran = $query_iuran->clone()->whereMonth("tanggal_transaksi", $bulan)->whereYear("tanggal_transaksi", $tahun_sekarang)->where("status_pembayaran", "=", "Pembayaran Sukses")->sum("total_iuran");
            $data_bayar_iuran_bulan[] = (int) $grafik_iuran;
        }

        $data_jenis_kelamin = $query_warga->clone()->select("jenis_kelamin", DB::raw("COUNT(jenis_kelamin) as total"))->groupBy('jenis_kelamin')->get()->toArray();
        if (!empty($data_jenis_kelamin)) {
            foreach ($data_jenis_kelamin as $i) {
                $label_jenis_kelamin[] = $i['jenis_kelamin'];
                $total_jenis_kelamin[] = $i['total'];
            }
        } else {
            $label_jenis_kelamin[] = "";
            $total_jenis_kelamin[] = 0;
        }

        $data_agama = $query_warga->clone()->select("agama", DB::raw("COUNT(agama) as total"))->groupBy('agama')->get()->toArray();
        if (!empty($data_agama)) {
            foreach ($data_agama as $i) {
                $label_agama[] = $i['agama'];
                $total_agama[] = $i['total'];
            }
        } else {
            $label_agama[] = "";
            $total_agama[] = 0;
        }

        $label_umur = [
            '0-5 tahun',
            '5-11 tahun',
            '12-16 tahun',
            '17-25 tahun',
            '26-35 tahun',
            '36-45 tahun',
            '46-55 tahun',
            '56-65 tahun',
            '65 tahun',
        ];

        $data_umur = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        $grafik_umur = $query_warga->clone()->get();

        if (!empty($grafik_umur)) {
            foreach ($grafik_umur as $umur) {
                $tanggal_lahir = new Carbon($umur->tanggal_lahir);
                $umur_warga = $tanggal_lahir->diffInYears();

                if ($umur_warga >= 0 && $umur_warga <= 5) { //  'Masa balita (0-5 tahun)'
                    $data_umur[0] += 1;
                } elseif ($umur_warga >= 5 && $umur_warga <= 11) { //  'Masa kanak- kanak (5-11 tahun)'
                    $data_umur[1] += 1;
                } elseif ($umur_warga >= 12 && $umur_warga <= 16) { //  'Masa remaja awal (12-16 tahun)',
                    $data_umur[2] += 1;
                } elseif ($umur_warga >= 17 && $umur_warga <= 25) { //  'Masa remaja akhir (17-25 tahun)'
                    $data_umur[3] += 1;
                } elseif ($umur_warga >= 26 && $umur_warga <= 35) { // 'Masa dewasa awal (26-35 tahun)'
                    $data_umur[4] += 1;
                } elseif ($umur_warga >= 36 && $umur_warga <= 45) { //  'Masa dewasa akhir (36-45 tahun)'
                    $data_umur[5] += 1;
                } elseif ($umur_warga >= 46 && $umur_warga <= 55) { //  'Masa Lansia Awal (46-55 tahun)'
                    $data_umur[6] += 1;
                } elseif ($umur_warga >= 56 && $umur_warga <= 65) { //  'Masa lansia akhir (56-65 tahun)'
                    $data_umur[7] += 1;
                } elseif ($umur_warga > 65) { // 'Masa manula (> 65 tahun)'
                    $data_umur[8] += 1;
                }
            }
        }

        $data_golongan_darah = $query_warga->clone()->select("golongan_darah", DB::raw("COUNT(golongan_darah) as total"))->groupBy('golongan_darah')->get()->toArray();

        if (!empty($data_golongan_darah)) {
            foreach ($data_golongan_darah as $i) {
                $label_golongan_darah[] = $i['golongan_darah'];
                $total_golongan_darah[] = $i['total'];
            }
        } else {
            $label_golongan_darah[] = "";
            $total_golongan_darah[] = 0;
        }

        return view("kepengurusan.dashboard", compact("label_golongan_darah", "total_golongan_darah", "label_umur", "data_umur", "label_agama", "total_agama", "label_jenis_kelamin", "total_jenis_kelamin", "label_bulan", "data_bayar_iuran_bulan", "data_transaksi", "data_pengguna", "jumlah_warga", "jumlah_akun", "jumlah_perumahan"));
    }

    public function dashboardV2()
    {
        $id_cluster = null;
        $cluster = null;
        $id_perumahan = null;
        $judul_dashboard = null;
        $kondisi_aktif = null;
        $biaya_iuran = null;

        if (!empty(session()->get('auth_data.0.data_perumahan'))) {
            $id_cluster = session()->get('auth_data.0.data_perumahan.0.id_cluster');
            $cluster = clusterModel::find($id_cluster);
            $judul_dashboard = session()->get('auth_data.0.data_perumahan.0.nama_cluster') . " " . session()->get('auth_data.0.data_perumahan.0.nama_perumahan');
            $kondisi_aktif = false;

            for ($i = 0; $i < count(session()->get('auth_data.0.data_perumahan')); $i++) {
                if (session()->get('auth_data.0.data_perumahan.' . $i . '.status_dashboard') == "Aktif") {
                    $kondisi = true;
                } else {
                    $kondisi = false;
                }
            }

            if ($kondisi == false) {
                session()->put('auth_data.0.data_perumahan.0.status_dashboard', "Aktif");
            }

            for ($i = 0; $i < count(session()->get('auth_data.0.data_perumahan')); $i++) {
                if (session()->get('auth_data.0.data_perumahan.' . $i . '.status_dashboard') == 'Aktif') {
                    $id_cluster = session()->get('auth_data.0.data_perumahan.' . $i . '.id_cluster');
                    $id_perumahan = session()->get('auth_data.0.data_perumahan.' . $i . '.id_perumahan');
                    $perumahan = perumahanModel::findOrFail($id_perumahan);
                    $judul_dashboard = session()->get('auth_data.0.data_perumahan.' . $i . '.nama_cluster') . " " . session()->get('auth_data.0.data_perumahan.' . $i . '.nama_perumahan');

                    if ($perumahan->kepemilikan_rumah == "Tidak Ditempati") {
                        $biaya_iuran = $cluster->biaya_iuran_tidak_ditempati;
                    } else {
                        $biaya_iuran = $cluster->biaya_iuran;
                    }
                }
            }
        }

        $data_iuran = iuranModel::with("detailiuran")->orderBy("tanggal_transaksi", "DESC")
            ->where("transaksi_iuran.id_warga", "=", session()->get('auth_data.0.id_warga'))
            ->where("transaksi_iuran.id_perumahan", "=", $id_perumahan)->get();

        $warga = count(wargaModel::where("id_kartu_keluarga", "=", Auth::user()->id_kartu_keluarga)->get());

        return view("pengguna.dashboard", compact("judul_dashboard", "warga", "id_perumahan", "data_iuran", "biaya_iuran"));
    }

    public function daftar_rumah_warga(Request $request)
    {
        $request->validate([
            "data_perumahan" => ["required"],
        ]);

        for ($i = 0; $i < count(session()->get('auth_data.0.data_perumahan')); $i++) {
            if ($i == $request->data_perumahan) {
                session()->put("auth_data.0.data_perumahan." . $i . ".status_dashboard", "Aktif");
            } else {
                session()->put("auth_data.0.data_perumahan." . $i . ".status_dashboard", "Tidak Aktif");
            }
        }

        return redirect()->route("dashboardV2");
    }

    public function akses_kepengurusan()
    {
        session()->put("view_data.0.view", "Kepengurusan");

        return redirect()->back();
    }

    public function akses_warga()
    {
        session()->put("view_data.0.view", "Warga");

        return redirect()->back();
    }
}
