<?php

namespace App\Http\Controllers;

use App\Models\kartukeluargaModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class KKwargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kartu_keluarga = kartukeluargaModel::withCount('perumahan')->withCount('warga')->where("id_kartu_keluarga", "!=", "1")->get();

        return view("kepengurusan.kartu_keluarga.index", compact("kartu_keluarga"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kartu_keluarga = new kartukeluargaModel();

        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("kepengurusan.kartu_keluarga.create", compact("kartu_keluarga", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "no_kk" => "required|max:16|min:16|unique:kartu_keluarga,no_kk",
            "file_kk" => "required|mimes:jpeg,png,jpg,pdf",
            "file_pernikahan" => "nullable|mimes:jpeg,png,jpg,pdf",
        ]);

        if ($request->hasFile('file_kk')) {
            $extension = $request->file('file_kk')->getClientOriginalExtension();
            $file_nama_kk = 'file_kk_' . time() . '.' . $extension;
            $request->file('file_kk')->storeAs('public/assets/pdf/dokumen_kk/', $file_nama_kk);
        }

        if ($request->hasFile('file_pernikahan')) {
            $extension = $request->file('file_pernikahan')->getClientOriginalExtension();
            $file_nama_pernikahan = 'file_pernikahan_' . time() . '.' . $extension;
            $request->file('file_pernikahan')->storeAs('public/assets/pdf/dokumen_pernikahan/', $file_nama_pernikahan);
        } else {
            $file_nama_pernikahan = null;
        }

        $kartu_keluarga = [
            "no_kk" => $request->no_kk,
            "file_kk" => $file_nama_kk,
            "file_pernikahan" => $file_nama_pernikahan,
        ];

        kartukeluargaModel::create($kartu_keluarga);

        return redirect()->route("kartu_keluarga_warga.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query_kartu_keluarga = kartukeluargaModel::query();
        $kk = $query_kartu_keluarga->with("perumahan", "warga", 'perumahan.cluster')->withCount("perumahan", "warga")->findOrFail($id);

        return view("kepengurusan.kartu_keluarga.show", compact("kk"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(kartukeluargaModel $kartu_keluarga)
    {
        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("kepengurusan.kartu_keluarga.edit", compact("kartu_keluarga", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kartukeluargaModel $kartu_keluarga)
    {
        $request->validate([
            "id_kartu_keluarga" => "required",
            "no_kk" => "required|max:16|min:16",
            "file_kk" => "mimes:jpeg,png,jpg,pdf",
            "file_pernikahan" => "nullable|mimes:jpeg,png,jpg,pdf",
        ]);

        if ($request->hasFile('file_kk')) {
            $extension = $request->file('file_kk')->getClientOriginalExtension();
            $file_nama_kk = 'file_kk_' . time() . '.' . $extension;

            Storage::delete('public/assets/pdf/dokumen_kk/' . $file_nama_kk);
            $request->file('file_kk')->storeAs('public/assets/pdf/dokumen_kk', $file_nama_kk);
        } else {
            $file_nama_kk = $kartu_keluarga->file_kk;
        }

        if ($request->hasFile('file_pernikahan')) {
            $extension = $request->file('file_pernikahan')->getClientOriginalExtension();
            $file_nama_pernikahan = 'file_pernikahan_' . time() . '.' . $extension;

            Storage::delete('public/assets/pdf/dokumen_pernikahan/' . $file_nama_pernikahan);
            $request->file('file_pernikahan')->storeAs('public/assets/pdf/dokumen_pernikahan', $file_nama_pernikahan);
        } else {
            $file_nama_pernikahan = $kartu_keluarga->file_pernikahan;
        }

        $kartu_keluarga->update([
            "no_kk" => $request->no_kk,
            "file_kk" => $file_nama_kk,
            "file_pernikahan" => $file_nama_pernikahan,
        ]);

        return redirect()->route("kartu_keluarga_warga.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(kartukeluargaModel $kartu_keluarga)
    {
        try {
            $file_nama_kk = $kartu_keluarga->file_kk;
            $file_nama_pernikahan = $kartu_keluarga->file_pernikahan;
            $kartu_keluarga->delete();

            Storage::delete('public/assets/pdf/dokumen_kk/' . $file_nama_kk);
            Storage::delete('public/assets/pdf/dokumen_pernikahan/' . $file_nama_pernikahan);
        } catch (Exception $e) {
            return redirect()->route("kartu_keluarga_warga.index")->withToastWarning('Masih terdapat warga/perumahan di data Kartu Keluarga');
        }
        return redirect()->route("kartu_keluarga_warga.index")->withToastSuccess('Berhasil Menghapus Data');
    }
}
