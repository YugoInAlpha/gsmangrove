<?php

namespace App\Http\Controllers;

use App\Exports\WargaMultiExport;
use App\Models\clusterModel;
use App\Models\kartukeluargaModel;
use App\Models\perumahanModel;
use App\Models\perumahanwargaModel;
use App\Models\wargaModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use ZipArchive;

class wargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warga = wargaModel::with("kartukeluarga")->get();
        $cluster = clusterModel::get();

        return view("kepengurusan.warga.index", compact('warga', 'cluster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_kk = null)
    {
        $warga = new wargaModel();
        $kartu_keluarga = kartukeluargaModel::where("id_kartu_keluarga", "!=", "1")->get();

        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("kepengurusan.warga.create", compact("id_kk", "kartu_keluarga", "warga", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->agama == "Lain-lain") {
            $request->validate([
                "id_kartu_keluarga" => "required",
                "no_kk" => "required|max:16|min:16",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "agama_lain" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required|in:A,B,AB,O",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required|in:Menikah,Belum Menikah",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required|in:Kepala Keluarga,Suami,Istri,Anak,Menantu,Cucu,Orang Tua,Mertua,Famili Lain,Pembantu,Lainnya",
                "file_ktp" => "required|mimes:jpeg,png,jpg,pdf",
            ]);
            $agama = $request->agama_lain;

        } else {
            $request->validate([
                "id_kartu_keluarga" => "required",
                "no_kk" => "required|max:16|min:16",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required|in:A,B,AB,O",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required|in:Menikah,Belum Menikah",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required|in:Kepala Keluarga,Suami,Istri,Anak,Menantu,Cucu,Orang Tua,Mertua,Famili Lain,Pembantu,Lainnya",
                "file_ktp" => "required|mimes:jpeg,png,jpg,pdf",
            ]);

            $agama = $request->agama;
        }

        $kepala_keluarga = wargaModel::where("id_kartu_keluarga", "=", $request->id_kartu_keluarga)
            ->where("status_hubungan_keluarga", "=", "Kepala Keluarga")->get()->first();

        if (!empty($kepala_keluarga) && $request->status_hubungan_keluarga == "Kepala Keluarga") {

            return redirect()->back()->withInput()->withToastWarning('Mohon Maaf hanya diperbolehkan 1 Kepala Keluarga');
        } else {
            if ($request->hasFile('file_ktp')) {
                $extension = $request->file('file_ktp')->getClientOriginalExtension();
                $file_nama_ktp = 'file_ktp_' . time() . '.' . $extension;

                $warga = [
                    "nik" => $request->nik,
                    "id_kartu_keluarga" => $request->id_kartu_keluarga,
                    "nama_lengkap" => $request->nama_lengkap,
                    "alamat_ktp" => $request->alamat_ktp,
                    "jenis_kelamin" => $request->jenis_kelamin,
                    "agama" => ucfirst($agama),
                    "tempat_lahir" => $request->tempat_lahir,
                    "tanggal_lahir" => $request->tanggal_lahir,
                    "golongan_darah" => $request->golongan_darah,
                    "nomor_telepon" => $request->nomor_telepon,
                    "status_kawin" => $request->status_kawin,
                    "pekerjaan" => $request->pekerjaan,
                    "status_hubungan_keluarga" => $request->status_hubungan_keluarga,
                    "file_ktp" => $file_nama_ktp,
                ];

                wargaModel::create($warga);

                $request->file('file_ktp')->storeAs('public/assets/pdf/dokumen_ktp/', $file_nama_ktp);
            }
            return redirect()->route("warga.index")->withToastSuccess('Berhasil Menyimpan Data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warga = wargaModel::join("kartu_keluarga", "warga.id_kartu_keluarga", "=", "kartu_keluarga.id_kartu_keluarga")
            ->where("warga.id_warga", "=", $id)->firstOrFail();

        $id_kk = $warga->id_kartu_keluarga;

        $perumahan = perumahanModel::with("cluster")->whereHas('kartukeluarga', function ($query) use ($id_kk) {
            $query->where("perumahan_warga.id_kartu_keluarga", $id_kk);
        })->get();

        return view("kepengurusan.warga.show", compact("warga", "perumahan"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(wargaModel $warga)
    {
        $id_kk = null;
        $kartu_keluarga = kartukeluargaModel::where("id_kartu_keluarga", "!=", "1")->get();

        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("kepengurusan.warga.edit", compact("id_kk", "warga", "kartu_keluarga", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, wargaModel $warga)
    {
        if ($request->agama == "Lain-lain") {
            $request->validate([
                "id_kartu_keluarga" => "required",
                "no_kk" => "required|max:16|min:16",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "agama_lain" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required|in:A,B,AB,O",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required|in:Menikah,Belum Menikah",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required|in:Kepala Keluarga,Suami,Istri,Anak,Menantu,Cucu,Orang Tua,Mertua,Famili Lain,Pembantu,Lainnya",
                "file_ktp" => "nullable|mimes:jpeg,png,jpg,pdf",
            ]);
        } else {
            $request->validate([
                "id_kartu_keluarga" => "required",
                "no_kk" => "required|max:16|min:16",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required|in:A,B,AB,O",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required|in:Menikah,Belum Menikah",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required|in:Kepala Keluarga,Suami,Istri,Anak,Menantu,Cucu,Orang Tua,Mertua,Famili Lain,Pembantu,Lainnya",
                "file_ktp" => "nullable|mimes:jpeg,png,jpg,pdf",
            ]);
        }

        $kepala_keluarga = wargaModel::where("id_kartu_keluarga", "=", $request->id_kartu_keluarga)
            ->where("status_hubungan_keluarga", "=", "Kepala Keluarga")->get()->first();

        if (!empty($kepala_keluarga) && $request->status_hubungan_keluarga == "Kepala Keluarga" && $kepala_keluarga->id_warga != $warga->id_warga) {

            return redirect()->back()->withInput()->withToastWarning('Mohon Maaf hanya diperbolehkan 1 Kepala Keluarga');
        } else {
            if ($request->hasFile('file_ktp')) {
                $extension = $request->file('file_ktp')->getClientOriginalExtension();
                $old_file_nama_ktp = $warga->file_ktp;
                $file_nama_ktp = 'file_ktp_' . time() . '.' . $extension;

                $warga->update([
                    "nik" => $request->nik,
                    "id_kartu_keluarga" => $request->id_kartu_keluarga,
                    "nama_lengkap" => $request->nama_lengkap,
                    "alamat_ktp" => $request->alamat_ktp,
                    "jenis_kelamin" => $request->jenis_kelamin,
                    "agama" => $request->agama,
                    "tempat_lahir" => $request->tempat_lahir,
                    "tanggal_lahir" => $request->tanggal_lahir,
                    "golongan_darah" => $request->golongan_darah,
                    "nomor_telepon" => $request->nomor_telepon,
                    "status_kawin" => $request->status_kawin,
                    "pekerjaan" => $request->pekerjaan,
                    "status_hubungan_keluarga" => $request->status_hubungan_keluarga,
                    "file_ktp" => $file_nama_ktp,
                ]);

                Storage::delete('public/assets/pdf/dokumen_ktp/' . $old_file_nama_ktp);
                $request->file('file_ktp')->storeAs('public/assets/pdf/dokumen_ktp/', $file_nama_ktp);
            } else {
                $warga->update([
                    "nik" => $request->nik,
                    "id_kartu_keluarga" => $request->id_kartu_keluarga,
                    "nama_lengkap" => $request->nama_lengkap,
                    "alamat_ktp" => $request->alamat_ktp,
                    "jenis_kelamin" => $request->jenis_kelamin,
                    "agama" => $request->agama,
                    "tempat_lahir" => $request->tempat_lahir,
                    "tanggal_lahir" => $request->tanggal_lahir,
                    "golongan_darah" => $request->golongan_darah,
                    "nomor_telepon" => $request->nomor_telepon,
                    "status_kawin" => $request->status_kawin,
                    "pekerjaan" => $request->pekerjaan,
                    "status_hubungan_keluarga" => $request->status_hubungan_keluarga,
                ]);
            }
        }

        return redirect()->route("warga.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(wargaModel $warga)
    {
        try {
            Storage::delete('public/assets/pdf/dokumen_ktp/' . $warga->file_ktp);
            $warga->delete();

        } catch (Exception $e) {
            return redirect()->route("warga.index")->withToastWarning('Mohon Maaf Masih terdapat data transaksi di warga');
        }
        return redirect()->route("warga.index")->withToastSuccess('Berhasil Menghapus Data');
    }

    public function export_dokumen_warga(Request $request)
    {
        $request->validate([
            "nama_cluster" => "required",
        ]);

        $zip = new ZipArchive;

        $query_cluster = clusterModel::query();
        $query_perumahan = perumahanModel::query();

        $time = Carbon::now()->isoFormat("D-M-Y");
        $filename = 'Data_warga.zip';

        File::delete(public_path($filename));

        if ($request->nama_cluster == "semua_cluster") {
            $cluster = $query_cluster->clone()->get();
        } else {
            $cluster = $query_cluster->clone()->where("id_cluster", $request->nama_cluster)->get();
        }
        if ($zip->open(public_path($filename), ZipArchive::CREATE) === true) {
            foreach ($cluster as $data_cluster) {
                $zip->addEmptyDir('cluster ' . $data_cluster->nama_cluster);
                $blok = $query_perumahan->clone()->select("blok_perumahan")->where("id_cluster", $data_cluster->id_cluster)->distinct()->get();
                foreach ($blok as $data_blok) {
                    $zip->addEmptyDir('Cluster ' . $data_cluster->nama_cluster . '/Blok ' . $data_blok->blok_perumahan);
                    $perumahan = $query_perumahan->clone()->where("blok_perumahan", $data_blok->blok_perumahan)->where("id_cluster", $data_cluster->id_cluster)->get();
                    foreach ($perumahan as $data_perumahan) {
                        $zip->addEmptyDir('Cluster ' . $data_cluster->nama_cluster . '/Blok ' . $data_blok->blok_perumahan . '/Perumahan ' . $data_perumahan->blok_perumahan . '-' . $data_perumahan->nama_perumahan);
                        $warga = perumahanwargaModel::join("kartu_keluarga", "perumahan_warga.id_kartu_keluarga", "kartu_keluarga.id_kartu_keluarga")
                            ->join("warga", "kartu_keluarga.id_kartu_keluarga", "warga.id_kartu_keluarga")
                            ->join("perumahan", "perumahan_warga.id_perumahan", "perumahan.id_perumahan")
                            ->join("cluster", "cluster.id_cluster", "perumahan.id_cluster")
                            ->where("warga.status_hubungan_keluarga", "=", "Kepala Keluarga")
                            ->where("cluster.id_cluster", "=", $data_cluster->id_cluster)
                            ->where("perumahan.blok_perumahan", "=", $data_blok->blok_perumahan)
                            ->get();

                        $zip->addEmptyDir('Cluster ' . $data_cluster->nama_cluster . '/Blok ' . $data_blok->blok_perumahan . '/Perumahan ' . $data_perumahan->blok_perumahan . '-' . $data_perumahan->nama_perumahan . '/file_pernikahan');
                        $zip->addEmptyDir('Cluster ' . $data_cluster->nama_cluster . '/Blok ' . $data_blok->blok_perumahan . '/Perumahan ' . $data_perumahan->blok_perumahan . '-' . $data_perumahan->nama_perumahan . '/file_kartu_keluarga');
                        $zip->addEmptyDir('Cluster ' . $data_cluster->nama_cluster . '/Blok ' . $data_blok->blok_perumahan . '/Perumahan ' . $data_perumahan->blok_perumahan . '-' . $data_perumahan->nama_perumahan . '/file_ktp');

                        foreach ($warga as $data_warga) {
                            $file_kk = public_path('storage/assets/pdf/dokumen_kk/' . $data_warga->file_kk);
                            $file_ktp = public_path('storage/assets/pdf/dokumen_ktp/' . $data_warga->file_ktp);
                            $file_pernikahan = public_path('storage/assets/pdf/dokumen_pernikahan/' . $data_warga->file_pernikahan);
                            if (file_exists($file_kk) && is_file($file_kk)) {
                                $zip->addFile(public_path('storage/assets/pdf/dokumen_kk/' . $data_warga->file_kk),
                                    'Cluster ' . $data_warga->nama_cluster . '/Blok ' . $data_warga->blok_perumahan . '/Perumahan ' . $data_warga->blok_perumahan . '-' . $data_warga->nama_perumahan . '/file_kartu_keluarga' . '/' . $data_warga->file_kk);
                            }

                            if (file_exists($file_ktp) && is_file($file_ktp)) {
                                $zip->addFile(public_path('storage/assets/pdf/dokumen_ktp/' . $data_warga->file_ktp),
                                    'Cluster ' . $data_warga->nama_cluster . '/Blok ' . $data_warga->blok_perumahan . '/Perumahan ' . $data_warga->blok_perumahan . '-' . $data_warga->nama_perumahan . '/file_ktp' . '/' . $data_warga->file_ktp);
                            }

                            if (file_exists($file_pernikahan) && is_file($file_pernikahan)) {
                                $zip->addFile(public_path('storage/assets/pdf/dokumen_pernikahan/' . $data_warga->file_pernikahan),
                                    'Cluster ' . $data_warga->nama_cluster . '/Blok ' . $data_warga->blok_perumahan . '/Perumahan ' . $data_warga->blok_perumahan . '-' . $data_warga->nama_perumahan . '/file_pernikahan' . '/' . $data_warga->file_pernikahan);
                            }
                        }
                    }
                }
            }
            $zip->close();
        }
        return response()->download(public_path($filename));
    }

    public function export_data_warga(Request $request)
    {

        $request->validate([
            "cluster" => "required",
        ]);
        try {
            return Excel::download(new WargaMultiExport($request->cluster), 'Data_Warga.xlsx');
        } catch (\Throwable$th) {
            return redirect()->back()->withToastWarning('Terjadi Kesalahan Export Data Warga');
        }
    }
}
