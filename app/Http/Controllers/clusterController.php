<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use Illuminate\Http\Request;

class clusterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cluster = clusterModel::get();

        return view("kepengurusan.cluster.index", compact("cluster"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cluster = new clusterModel();
        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("kepengurusan.cluster.create", compact("cluster", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "nama_cluster" => "required|unique:cluster,nama_cluster|max:50",
            "biaya_iuran" => "required|numeric",
            "biaya_iuran_tidak_ditempati" => "required|numeric",
        ]);

        $cluster = [
            "nama_cluster" => $request->nama_cluster,
            "biaya_iuran" => $request->biaya_iuran,
            "biaya_iuran_tidak_ditempati" => $request->biaya_iuran_tidak_ditempati,
        ];

        clusterModel::create($cluster);

        return redirect()->route("cluster.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(clusterModel $cluster)
    {
        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("kepengurusan.cluster.edit", compact("cluster", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, clusterModel $cluster)
    {
        $request->validate([
            "nama_cluster" => "required|max:50",
            "biaya_iuran" => "required|numeric",
            "biaya_iuran_tidak_ditempati" => "required|numeric",
            "status_cluster" => "required|in:Aktif,Tidak Aktif",
        ]);

        $cluster->update([
            "nama_cluster" => $request->nama_cluster,
            "biaya_iuran" => $request->biaya_iuran,
            "biaya_iuran_tidak_ditempati" => $request->biaya_iuran_tidak_ditempati,
            "status_cluster" => $request->status_cluster,
        ]);

        return redirect()->route("cluster.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(clusterModel $cluster)
    {
        try {
            $cluster->delete();
        } catch (\Throwable$th) {
            return redirect()->route("cluster.index")->withToastWarning('Masih terdapat Perumahan yang menggunakan Cluster');
        }
        return redirect()->route("cluster.index")->withToastSuccess('Berhasil Menghapus Data');
    }
}
