<?php

namespace App\Http\Controllers;

use App\Models\iuranModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class iuranwargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi_iuran = iuranModel::with("warga")->orderBy('id_transaksi_iuran', 'DESC')->get();

        return view("kepengurusan.transaksi_iuran.index", compact("transaksi_iuran"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_iuran = iuranModel::with("warga", "perumahan", "perumahan.cluster", "detailiuran")->findOrFail($id);

        return view("kepengurusan.transaksi_iuran.show", compact("transaksi_iuran"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status_pembayaran = $request->segment(4);
        $id_transaksi_iuran = $request->segment(2);
        $id_warga = $request->segment(3);

        if ($id_warga) {
            $tanggal_sekarang = date('Y-m-d', strtotime('-1 year', strtotime(Carbon::create(Carbon::now()->year, 1, 1))));
            $tanggal_kedepan = date('Y-m-d', strtotime('+2 year', strtotime($tanggal_sekarang)));

            $update_data_transaksi = iuranModel::with("detailiuran")->findOrFail($id_transaksi_iuran);
            $transaksi_iuran = iuranModel::with("detailiuran")->where("id_warga", $id_warga)->whereBetween('tanggal_transaksi', [$tanggal_sekarang, $tanggal_kedepan])->get();

            $status = null;
            foreach ($transaksi_iuran as $old_data_transaksi) {
                $status = $this->cek_transaksi($old_data_transaksi, $update_data_transaksi);
                if ($status == 1) {
                    break;
                }
            }

            if ($status == 0 || $status_pembayaran != "Pembayaran Sukses") {
                $update_data_transaksi->update([
                    "status_pembayaran" => $status_pembayaran,
                ]);

                return redirect()->back()->withToastSuccess('Berhasil Merubah Status Pembayaran');
            } else {
                return redirect()->back()->withToastWarning('Terdapat pembayaran iuran di bulan yang sama');
            }
        }
    }

    public function cek_transaksi($old_data_transaksi, $new_data_transaksi)
    {
        $status = 0;
        foreach ($old_data_transaksi->detailiuran as $old_data_detail_transaksi) {
            if ($old_data_transaksi->status_pembayaran == "Pembayaran Sukses") {
                foreach ($new_data_transaksi->detailiuran as $data_detail_update) {
                    if ($old_data_detail_transaksi->periode_iuran_bulan == $data_detail_update->periode_iuran_bulan && $old_data_detail_transaksi->id_perumahan == $new_data_transaksi->id_perumahan) {
                        $status = 1;
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
