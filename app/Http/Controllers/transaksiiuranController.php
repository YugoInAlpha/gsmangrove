<?php

namespace App\Http\Controllers;

use App\Models\detailiuranModel;
use App\Models\iuranModel;
use App\Models\kartukeluargaModel;
use App\Models\metodepembayaranModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class transaksiiuranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warga = wargaModel::find(session()->get("auth_data.0.id_warga"));

        if (empty($warga)) {
            $warga = wargaModel::where("id_kartu_keluarga", session()->get('auth_data.0.id_kartu_keluarga'))->where("status_hubungan_keluarga", "Kepala Keluarga")->firstOrFail();
        }

        $transaksi_iuran = iuranModel::with("perumahan", "perumahan.cluster")->where("id_warga", $warga->id_warga)->get();

        return view("pengguna.transaksi_iuran.index", compact("transaksi_iuran"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warga = wargaModel::find(session()->get('auth_data.0.id_warga'));

        if (empty($warga)) {
            $warga = wargaModel::where("id_kartu_keluarga", session()->get('auth_data.0.id_kartu_keluarga'))->where("status_hubungan_keluarga", "Kepala Keluarga")->get()->firstOrFail();
        }

        $perumahan = kartukeluargaModel::with("perumahan", "perumahan.cluster")->findOrFail($warga->id_kartu_keluarga);
        return view("pengguna.transaksi_iuran.create", compact("warga", "perumahan"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "id_warga" => ["required"],
            "id_perumahan" => ["required"],
            "nik" => ["required"],
            "tanggal_mulai" => ["required"],
            "periode_pembayaran" => ["required"],
            "biaya_iuran" => ["required"],
            "total_biaya_iuran" => ["required"],
            "id_metode_pembayaran" => ["required"],
            "file_bukti_pembayaran" => ["required"],
            "catatan" => ["nullable"],
        ]);

        if ($request->hasFile('file_bukti_pembayaran')) {
            $extension = $request->file('file_bukti_pembayaran')->getClientOriginalExtension();
            $file_nama_bukti = 'file_bukti_pembayaran_' . time() . '.' . $extension;

            $tanggal_sekarang = Carbon::now();

            $transaksi_iuran = [
                "id_metode_pembayaran" => $request->id_metode_pembayaran,
                "id_perumahan" => $request->id_perumahan,
                "id_warga" => $request->id_warga,
                "tanggal_transaksi" => $tanggal_sekarang,
                "periode_pembayaran" => $request->periode_pembayaran,
                "total_iuran" => $request->total_biaya_iuran,
                "file_bukti_transaksi" => $file_nama_bukti,
                "catatan" => $request->catatan,
            ];

            $data_transaksi_iuran = iuranModel::create($transaksi_iuran);

            $bulan_sekarang = $request->tanggal_mulai;
            if ($request->id_warga) {
                $tanggal_sekarang = date('Y-m-d', strtotime('-1 year', strtotime(Carbon::create(Carbon::now()->year, 1, 1))));
                $tanggal_kedepan = date('Y-m-d', strtotime('+2 year', strtotime($tanggal_sekarang)));

                $loop_transaksi_iuran = iuranModel::with("detailiuran")->where("id_warga", $request->id_warga)->whereBetween('tanggal_transaksi', [$tanggal_sekarang, $tanggal_kedepan])->get();

                if (!empty($data_transaksi_iuran)) {
                    foreach ($loop_transaksi_iuran as $old_data_transaksi_iuran) {
                        $status = $this->cek_transaksi($old_data_transaksi_iuran, $data_transaksi_iuran, $bulan_sekarang);
                        if ($status == 1) {
                            break;
                        }
                    }
                }

                if ($status == 0) {
                    $request->file('file_bukti_pembayaran')->storeAs('public/assets/pdf/dokumen_bukti_pembayaran', $file_nama_bukti);

                    for ($i = 0; $i < $data_transaksi_iuran->periode_pembayaran; $i++) {
                        $bulan_loop = date('Y-m-d', strtotime('+' . $i . ' months', strtotime($bulan_sekarang)));
                        $detail_transaksi_iuran = [
                            "id_transaksi_iuran" => $data_transaksi_iuran->id_transaksi_iuran,
                            "periode_iuran_bulan" => $bulan_loop,
                            "deskripsi_iuran" => "Pembayaran iuran " . $bulan_loop,
                            "biaya_iuran" => $request->biaya_iuran,
                        ];
                        detailiuranModel::create($detail_transaksi_iuran);
                    }

                    return redirect()->route("transaksi_iuran.index")->withToastSuccess('Berhasil Menyimpan Data');
                } else {
                    $delete_tranasksi_iuran = iuranModel::findOrFail($data_transaksi_iuran->id_transaksi_iuran);
                    $delete_tranasksi_iuran->delete();

                    return redirect()->back()->withInput()->withToastWarning('Terdapat pembayaran iuran di bulan dan tahun yang sama');
                }
            }
        }
    }

    public function cek_transaksi($old_transaksi_iuran, $new_transaksi_iuran, $bulan_sekarang)
    {
        $status = 0;
        foreach ($old_transaksi_iuran->detailiuran as $old_detail_iuran) {
            if ($old_transaksi_iuran->id_perumahan == $new_transaksi_iuran->id_perumahan) {
                if ($old_transaksi_iuran->status_pembayaran == "Pembayaran Sukses" || $old_transaksi_iuran->status_pembayaran == "Menunggu Konfirmasi") {
                    for ($i = 0; $i < $new_transaksi_iuran->periode_pembayaran; $i++) {
                        $bulan_loop = date('Y-m-d', strtotime('+' . $i . ' months', strtotime($bulan_sekarang)));
                        if ($old_detail_iuran->periode_iuran_bulan == $bulan_loop) {
                            $status = 1;
                        }
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_iuran = iuranModel::with("perumahan", "perumahan.cluster", "detailiuran")->findOrFail($id);

        return view("pengguna.transaksi_iuran.show", compact("transaksi_iuran"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getRekening(Request $request)
    {
        $metode_pembayaran = metodepembayaranModel::where("id_cluster", $request->get("id_cluster"))->where("status_rekening", "Aktif")->get();
        return response()->json($metode_pembayaran);
    }
}
