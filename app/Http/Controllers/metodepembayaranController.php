<?php

namespace App\Http\Controllers;

use App\Models\clusterModel;
use App\Models\metodepembayaranModel;
use Illuminate\Http\Request;

class metodepembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metode_pembayaran = metodepembayaranModel::with("cluster")->get();

        return view("kepengurusan.metode_pembayaran.index", compact("metode_pembayaran"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $metode_pembayaran = new metodepembayaranModel();
        $cluster = clusterModel::where("status_cluster", "Aktif")->get();
        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];
        return view("kepengurusan.metode_pembayaran.create", compact("metode_pembayaran", "cluster", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "id_cluster" => "required",
            "nama_rekening" => "required",
            "nomor_rekening" => "required",
            "rekening_atas_nama" => "required",
        ]);

        $metode_pembayaran = [
            "id_cluster" => $request->id_cluster,
            "nama_rekening" => $request->nama_rekening,
            "nomor_rekening" => $request->nomor_rekening,
            "rekening_atas_nama" => $request->rekening_atas_nama,
        ];

        metodepembayaranModel::create($metode_pembayaran);

        return redirect()->route("metode_pembayaran.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(metodepembayaranModel $metode_pembayaran)
    {
        $cluster = clusterModel::get();

        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("kepengurusan.metode_pembayaran.edit", compact("metode_pembayaran", "cluster", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, metodepembayaranModel $metode_pembayaran)
    {
        $request->validate([
            "id_cluster" => "required",
            "nama_rekening" => "required",
            "nomor_rekening" => "required",
            "rekening_atas_nama" => "required",
            "status_rekening" => "required",
        ]);

        $metode_pembayaran->update([
            "id_cluster" => $request->id_cluster,
            "nama_rekening" => $request->nama_rekening,
            "nomor_rekening" => $request->nomor_rekening,
            "rekening_atas_nama" => $request->rekening_atas_nama,
            "status_rekening" => $request->status_rekening,
        ]);

        return redirect()->route("metode_pembayaran.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(metodepembayaranModel $metode_pembayaran)
    {
        try {
            $metode_pembayaran->delete();

        } catch (\Throwable$th) {
            return redirect()->route("metode_pembayaran.index")->withToastWarning('Terdapat data yang menggunakan metode pembayaran ini');
        }
        return redirect()->route("metode_pembayaran.index")->withToastSuccess('Berhasil Menghapus Data');
    }
}
