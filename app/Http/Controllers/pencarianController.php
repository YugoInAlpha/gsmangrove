<?php

namespace App\Http\Controllers;

use App\Models\iuranModel;
use App\Models\kartukeluargaModel;
use App\Models\perumahanModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class pencarianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warga = wargaModel::where("warga.status_hubungan_keluarga", "=", "Kepala Keluarga")->get();

        return view("kepengurusan.pencarian.search", compact("warga"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "warga" => "required",
        ]);

        return redirect()->route("pencarian.search", $request->warga)->withToastSuccess('Berhasil Menemukan Data Warga');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_iuran = iuranModel::with("warga", "perumahan", "perumahan.cluster", "detailiuran")->findOrFail($id);

        return view("kepengurusan.pencarian.show", compact("transaksi_iuran"));
    }

    public function result_search_post($id, Request $request)
    {
        return redirect()->route("pencarian.search_rumah", ["warga" => $id, "id_perumahan" => $request->id_perumahan]);
    }

    public function result_search($id, Request $request)
    {
        $id_perumahan = null;
        $alamat = null;
        $data_perumahan = null;

        $warga = wargaModel::with("kartukeluarga")->where("warga.status_hubungan_keluarga", "Kepala Keluarga")->findOrFail($id);

        if (!empty($request->id_perumahan)) {
            $data_perumahan = kartukeluargaModel::with("perumahan", "perumahan.cluster")->findOrFail($warga->id_kartu_keluarga);
            $selected_perumahan = perumahanModel::with("cluster")->findOrFail($request->id_perumahan);

        } else {
            $data_perumahan = kartukeluargaModel::with("perumahan", "perumahan.cluster")->findOrFail($warga->id_kartu_keluarga);
            $selected_perumahan = $data_perumahan->perumahan->first();
        }
        $data_iuran = iuranModel::with("detailiuran")->orderBy("tanggal_transaksi", "DESC")
            ->where("id_warga", $warga->id_warga)
            ->where("id_perumahan", $selected_perumahan->id_perumahan)->get();

        return view("kepengurusan.pencarian.index", compact("warga", "data_perumahan", "data_iuran", "selected_perumahan"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status_iuran(Request $request)
    {
        $status_pembayaran = $request->segment(3);
        $id_transaksi_iuran = $request->segment(2);
        $id_warga = $request->segment(4);
        $id_perumahan = $request->segment(5);

        if ($id_warga) {
            $tanggal_sekarang = date('Y-m-d', strtotime('-1 year', strtotime(Carbon::create(Carbon::now()->year, 1, 1))));
            $tanggal_kedepan = date('Y-m-d', strtotime('+2 year', strtotime($tanggal_sekarang)));

            $update_data_transaksi = iuranModel::with("detailiuran")->findOrFail($id_transaksi_iuran);
            $transaksi_iuran = iuranModel::with("detailiuran")->where("id_warga", $id_warga)->whereBetween('tanggal_transaksi', [$tanggal_sekarang, $tanggal_kedepan])->get();

            $status = null;
            foreach ($transaksi_iuran as $old_data_transaksi) {
                $status = $this->cek_transaksi($old_data_transaksi, $update_data_transaksi, $id_perumahan);
                if ($status == 1) {
                    break;
                }
            }

            if ($status == 0 || $status_pembayaran != "Pembayaran Sukses") {
                $update_data_transaksi->update([
                    "status_pembayaran" => $status_pembayaran,
                ]);

                return redirect()->back()->withToastSuccess('Berhasil Merubah Status Pembayaran');
            } else {
                return redirect()->back()->withToastWarning('Terdapat pembayaran iuran di bulan yang sama');
            }
        }
    }

    public function cek_transaksi($old_data_transaksi, $new_data_transaksi)
    {
        $status = 0;
        foreach ($old_data_transaksi->detailiuran as $old_data_detail_transaksi) {
            if ($old_data_transaksi->status_pembayaran == "Pembayaran Sukses") {
                foreach ($new_data_transaksi->detailiuran as $data_detail_update) {
                    if ($old_data_detail_transaksi->periode_iuran_bulan === $data_detail_update->periode_iuran_bulan && $old_data_detail_transaksi->id_perumahan == $new_data_transaksi->id_perumahan) {
                        $status = 1;
                    }
                }
            }
        }

        return $status;
    }

}
