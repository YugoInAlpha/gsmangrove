<?php

namespace App\Http\Controllers;

use App\Models\penggunaModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class profilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pengguna = penggunaModel::findOrFail($request->segment(2));
        $warga = wargaModel::with("kartukeluarga", "kartukeluarga.perumahan", "kartukeluarga.perumahan.cluster")
            ->where("id_kartu_keluarga", $pengguna->id_kartu_keluarga)->where("status_hubungan_keluarga", "Kepala Keluarga")->get()->first();

        return view("kepengurusan.profil.index", compact("warga", "pengguna"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, penggunaModel $pengguna)
    {
        if (!empty($request->ubah_password)) {
            $request->validate([
                "nama_pengguna" => "required",
                "kata_sandi" => "required|required_with:konfirmasi_kata_sandi|same:konfirmasi_kata_sandi",
                "konfirmasi_kata_sandi" => "required",
            ]);

            $pengguna->update([
                "nama_pengguna" => $request->nama_pengguna,
                "kata_sandi" => Hash::make($request->kata_sandi),
                "konfirmasi_kata_sandi" => $request->konfirmasi_kata_sandi,
            ]);
        } else {
            $request->validate([
                "nama_pengguna" => "required",
            ]);

            $pengguna->update([
                "nama_pengguna" => $request->nama_pengguna,
            ]);
        }

        return redirect()->back()->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
