<?php

namespace App\Http\Controllers;

use App\Models\penggunaModel;
use App\Models\wargaModel;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class authController extends Controller
{
    public function login()
    {
        session()->forget("data_registrasi");
        return view("auth.login");
    }

    public function store(Request $request)
    {
        $request->validate([
            "nama_pengguna" => ["required"],
            "kata_sandi" => ["required"],
        ]);

        $pengguna = penggunaModel::select("nama_pengguna", "status_akun", "status_pengguna")->where("nama_pengguna", $request->nama_pengguna)->first();

        if (!$pengguna) {
            return redirect()->back()->withErrors(["nama_pengguna" => "Nama Pengguna Tidak Diketahui"]);
        } elseif ($pengguna["status_akun"] != "Terkonfirmasi") {
            return redirect()->route('login')->withToastInfo('Mohon Maaf, Akun Sedang Proses Konfirmasi');
        } elseif ($pengguna["status_pengguna"] != "Aktif") {
            return redirect()->route('login')->withToastInfo('Mohon Maaf, Akun Anda Belum Diaktifkan');
        }

        if (auth()->attempt(['nama_pengguna' => $request->nama_pengguna, 'password' => $request->kata_sandi, 'status_akun' => "Terkonfirmasi", "status_pengguna" => "Aktif"])) {
            if (auth()->user()->peran == "Warga") {
                $id_kk = auth()->user()->id_kartu_keluarga;
                $data_kepala_keluarga = wargaModel::with(["kartukeluarga", "kartukeluarga.perumahan", "kartukeluarga.perumahan.cluster"])->where("status_hubungan_keluarga", "Kepala Keluarga")->where("id_kartu_keluarga", $id_kk)->get()->firstOrFail();

                session()->push('auth_data', [
                    "id_warga" => $data_kepala_keluarga->id_warga,
                    "id_kartu_keluarga" => $data_kepala_keluarga->id_kartu_keluarga,
                ]);

                foreach ($data_kepala_keluarga->kartukeluarga->perumahan as $data_rumah) {
                    session()->push('auth_data.0.data_perumahan', [
                        "id_cluster" => $data_rumah->id_cluster,
                        "nama_cluster" => $data_rumah->cluster->nama_cluster,
                        "id_perumahan" => $data_rumah->id_perumahan,
                        "nama_perumahan" => $data_rumah->nama_perumahan,
                        "blok_perumahan" => $data_rumah->blok_perumahan,
                        "status_dashboard" => "Tidak Aktif",
                    ]);
                }

                if (session()->get("auth_data.0.data_perumahan.0")) {
                    session()->put("auth_data.0.data_perumahan.0.status_dashboard", "Aktif");
                }

                return redirect(RouteServiceProvider::HOME)->withToastSuccess('Anda Berhasil Masuk');
            } else {
                if (auth()->user()->peran == "Pihak Kepengurusan GSM") {
                    $id_kk = auth()->user()->id_kartu_keluarga;
                    $data_kepala_keluarga = wargaModel::with(["kartukeluarga", "kartukeluarga.perumahan", "kartukeluarga.perumahan.cluster"])->where("status_hubungan_keluarga", "Kepala Keluarga")->where("id_kartu_keluarga", $id_kk)->get()->firstOrFail();

                    session()->push('auth_data', [
                        "id_warga" => $data_kepala_keluarga->id_warga,
                        "id_kartu_keluarga" => $data_kepala_keluarga->id_kartu_keluarga,
                    ]);

                    foreach ($data_kepala_keluarga->kartukeluarga->perumahan as $data_rumah) {
                        session()->push('auth_data.0.data_perumahan', [
                            "id_cluster" => $data_rumah->id_cluster,
                            "nama_cluster" => $data_rumah->cluster->nama_cluster,
                            "id_perumahan" => $data_rumah->id_perumahan,
                            "nama_perumahan" => $data_rumah->nama_perumahan,
                            "blok_perumahan" => $data_rumah->blok_perumahan,
                            "status_dashboard" => "Tidak Aktif",
                        ]);
                    }

                    if (session()->get("auth_data.0.data_perumahan.0")) {
                        session()->put("auth_data.0.data_perumahan.0.status_dashboard", "Aktif");
                    }
                }

                session()->push("view_data", [
                    "view" => "Kepengurusan",
                    "id_kartu_keluarga" => auth()->user()->id_kartu_keluarga,
                ]);

                return redirect(RouteServiceProvider::HOME_KEPENGURUSAN)->withToastSuccess('Anda Berhasil Masuk');
            }
        } else {
            return redirect()->route('login')->withToastWarning('Pastikan Nama Pengguna/Kata Sandi Betul');
        }
    }

    public function logout()
    {
        auth()->logout();
        session()->forget('auth_data');
        session()->forget('view_data');
        session()->flush();

        return redirect()->route("login")->withToastSuccess('Akun Berhasil Dikeluarkan');
    }
}
