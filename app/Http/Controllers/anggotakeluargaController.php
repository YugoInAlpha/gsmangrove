<?php

namespace App\Http\Controllers;

use App\Models\kartukeluargaModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class anggotakeluargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kartu_keluarga = kartukeluargaModel::findOrFail(auth()->user()->id_kartu_keluarga);
        $warga = wargaModel::where("id_kartu_keluarga", auth()->user()->id_kartu_keluarga)->get();

        return view("pengguna.anggota_keluarga.index", compact("kartu_keluarga", "warga"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warga = new wargaModel();
        $kartu_keluarga = kartukeluargaModel::findOrFail(auth()->user()->id_kartu_keluarga);

        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("pengguna.anggota_keluarga.create", compact("kartu_keluarga", "warga", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->agama == "Lain-lain") {
            $request->validate([
                "id_kartu_keluarga" => "required",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "agama_lain" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required",
                "file_ktp" => "required|mimes:jpeg,png,jpg,pdf",
            ]);
        } else {
            $request->validate([
                "id_kartu_keluarga" => "required",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required",
                "file_ktp" => "required|mimes:jpeg,png,jpg,pdf",
            ]);
        }

        $kepala_keluarga = wargaModel::where("id_kartu_keluarga", "=", $request->id_kartu_keluarga)->where("status_hubungan_keluarga", "=", "Kepala Keluarga")->get()->first();

        if (!empty($kepala_keluarga) && $request->status_hubungan_keluarga == "Kepala Keluarga") {
            return redirect()->back()->withInput()->withToastWarning('Maaf hanya diperbolehkan 1 Kepala Keluarga');
        } else {
            if ($request->hasFile('file_ktp')) {
                $extension = $request->file('file_ktp')->getClientOriginalExtension();
                $file_nama_ktp = 'file_ktp_' . time() . '.' . $extension;

                $warga = [
                    "nik" => $request->nik,
                    "id_kartu_keluarga" => $request->id_kartu_keluarga,
                    "nama_lengkap" => $request->nama_lengkap,
                    "alamat_ktp" => $request->alamat_ktp,
                    "jenis_kelamin" => $request->jenis_kelamin,
                    "agama" => $request->agama,
                    "tempat_lahir" => $request->tempat_lahir,
                    "tanggal_lahir" => $request->tanggal_lahir,
                    "golongan_darah" => $request->golongan_darah,
                    "nomor_telepon" => $request->nomor_telepon,
                    "status_kawin" => $request->status_kawin,
                    "pekerjaan" => $request->pekerjaan,
                    "status_hubungan_keluarga" => $request->status_hubungan_keluarga,
                    "file_ktp" => $file_nama_ktp,
                ];

                wargaModel::create($warga);

                $request->file('file_ktp')->storeAs('public/assets/pdf/dokumen_ktp/', $file_nama_ktp);
            }

            return redirect()->route("anggota_keluarga.index")->withToastSuccess('Berhasil Menyimpan Data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warga = wargaModel::with("kartukeluarga", "kartukeluarga.perumahan.cluster")->where("id_warga", $id)->get()->firstOrFail();

        return view("pengguna.anggota_keluarga.show", compact("warga"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(wargaModel $warga)
    {
        $kartu_keluarga = kartukeluargaModel::where("id_kartu_keluarga", "=", $warga->id_kartu_keluarga)->firstOrFail();

        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("pengguna.anggota_keluarga.edit", compact("warga", "kartu_keluarga", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, wargaModel $warga)
    {
        if ($request->agama == "Lain-lain") {
            $request->validate([
                "no_kk" => "required|max:16|min:16",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "agama_lain" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required|in:Kepala Keluarga,Suami,Istri,Anak,Menantu,Cucu,Orang Tua,Mertua,Famili Lain,Pembantu,Lainnya",
                "file_ktp" => "nullable|mimes:jpeg,png,jpg,pdf",
            ]);

            $agama = $request->agama_lain;
        } else {
            $request->validate([
                "no_kk" => "required|max:16|min:16",
                "nik" => "required|max:16|min:16",
                "nama_lengkap" => "required|max:100",
                "alamat_ktp" => "required",
                "jenis_kelamin" => "required",
                "agama" => "required",
                "tempat_lahir" => "required|max:50",
                "tanggal_lahir" => "required",
                "golongan_darah" => "required",
                "nomor_telepon" => "required|max:15",
                "status_kawin" => "required",
                "pekerjaan" => "required|max:50",
                "status_hubungan_keluarga" => "required|in:Kepala Keluarga,Suami,Istri,Anak,Menantu,Cucu,Orang Tua,Mertua,Famili Lain,Pembantu,Lainnya",
                "file_ktp" => "nullable|mimes:jpeg,png,jpg,pdf",
            ]);
            $agama = $request->agama;
        }

        $kepala_keluarga = wargaModel::where("id_kartu_keluarga", "=", $request->id_kartu_keluarga)->where("status_hubungan_keluarga", "=", "Kepala Keluarga")->get()->first();

        if (!empty($kepala_keluarga) && $request->status_hubungan_keluarga == "Kepala Keluarga" && $kepala_keluarga->id_warga != $warga->id_warga) {
            return redirect()->back()->withInput()->withToastWarning('Maaf hanya diperbolehkan 1 Kepala Keluarga');
        } else {
            if ($request->hasFile('file_ktp')) {
                $extension = $request->file('file_ktp')->getClientOriginalExtension();
                $old_file_nama_ktp = $warga->file_ktp;
                $file_nama_ktp = 'file_ktp_' . time() . '.' . $extension;

                $warga->update([
                    "nik" => $request->nik,
                    "no_kk" => $request->no_kk,
                    "nama_lengkap" => $request->nama_lengkap,
                    "alamat_ktp" => $request->alamat_ktp,
                    "jenis_kelamin" => $request->jenis_kelamin,
                    "agama" => $agama,
                    "tempat_lahir" => $request->tempat_lahir,
                    "tanggal_lahir" => $request->tanggal_lahir,
                    "golongan_darah" => $request->golongan_darah,
                    "nomor_telepon" => $request->nomor_telepon,
                    "status_kawin" => $request->status_kawin,
                    "pekerjaan" => $request->pekerjaan,
                    "status_hubungan_keluarga" => $request->status_hubungan_keluarga,
                    "file_ktp" => $file_nama_ktp,
                ]);

                Storage::delete('public/assets/pdf/dokumen_ktp/' . $old_file_nama_ktp);
                $request->file('file_ktp')->storeAs('public/assets/pdf/dokumen_ktp/', $file_nama_ktp);
            } else {
                $warga->update([
                    "nik" => $request->nik,
                    "no_kk" => $request->no_kk,
                    "nama_lengkap" => $request->nama_lengkap,
                    "alamat_ktp" => $request->alamat_ktp,
                    "jenis_kelamin" => $request->jenis_kelamin,
                    "agama" => $agama,
                    "tempat_lahir" => $request->tempat_lahir,
                    "tanggal_lahir" => $request->tanggal_lahir,
                    "golongan_darah" => $request->golongan_darah,
                    "nomor_telepon" => $request->nomor_telepon,
                    "status_kawin" => $request->status_kawin,
                    "pekerjaan" => $request->pekerjaan,
                    "status_hubungan_keluarga" => $request->status_hubungan_keluarga,
                ]);
            }
        }

        return redirect()->route("anggota_keluarga.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    public function edit_kartu_keluarga(kartukeluargaModel $kartu_keluarga)
    {
        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];
        return view("pengguna.anggota_keluarga.edit_kk", compact("kartu_keluarga", "model_form"));
    }

    public function update_kartu_keluarga(Request $request, kartukeluargaModel $kartu_keluarga)
    {
        $request->validate([
            "id_kartu_keluarga" => "required",
            "no_kk" => "required|max:16|min:16",
            "file_kk" => "mimes:jpeg,png,jpg,pdf",
            "file_pernikahan" => "nullable|mimes:jpeg,png,jpg,pdf",
        ]);

        if ($request->hasFile('file_kk')) {
            $extension = $request->file('file_kk')->getClientOriginalExtension();
            $file_nama_kk = 'file_kk_' . time() . '.' . $extension;

            Storage::delete('public/assets/pdf/dokumen_kk/' . $file_nama_kk);
            $request->file('file_kk')->storeAs('public/assets/pdf/dokumen_kk', $file_nama_kk);
        } else {
            $file_nama_kk = $kartu_keluarga->file_kk;
        }

        if ($request->hasFile('file_pernikahan')) {
            $extension = $request->file('file_pernikahan')->getClientOriginalExtension();
            $file_nama_pernikahan = 'file_pernikahan_' . time() . '.' . $extension;

            Storage::delete('public/assets/pdf/dokumen_pernikahan/' . $file_nama_pernikahan);
            $request->file('file_pernikahan')->storeAs('public/assets/pdf/dokumen_pernikahan', $file_nama_pernikahan);
        } else {
            $file_nama_pernikahan = $kartu_keluarga->file_pernikahan;
        }

        $kartu_keluarga->update([
            "no_kk" => $request->no_kk,
            "file_kk" => $file_nama_kk,
            "file_pernikahan" => $file_nama_pernikahan,
        ]);

        return redirect()->route("anggota_keluarga.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(wargaModel $warga)
    {
        try {
            $file_nama_ktp = $warga->file_ktp;
            $warga->delete();
            Storage::delete('public/assets/pdf/dokumen_ktp/' . $file_nama_ktp);
        } catch (\Throwable$th) {
            return redirect()->route("anggota_keluarga.index")->withToastWarning('Terdapat data transaksi yang dilakukan warga');
        }
        return redirect()->route("anggota_keluarga.index")->withToastSuccess('Berhasil Menghapus Data');
    }
}
