<?php

namespace App\Http\Controllers;

use App\Models\kartukeluargaModel;
use App\Models\penggunaModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class penggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengguna = penggunaModel::with("kartukeluarga")->get();

        return view("kepengurusan.pengguna.index", compact("pengguna"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pengguna = new penggunaModel();
        $kartu_keluarga = kartukeluargaModel::where("id_kartu_keluarga", "!=", "1")->get();

        $model_form = [
            "model" => "create",
            "submit" => "Simpan",
            "btn" => "btn-success",
        ];

        return view("kepengurusan.pengguna.create", compact("pengguna", "kartu_keluarga", "model_form"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->jabatan == "Lain-lain") {
            $request->validate([
                "no_kk" => "required|max:16|min:16",
                "id_kartu_keluarga" => "required|unique:pengguna,id_kartu_keluarga",
                "nama_pengguna" => "required",
                "jabatan" => "required",
                "jabatan_lain" => "required",
                "kata_sandi" => "required",
                "peran" => "required",
            ]);
            $jabatan = $request->jabatan_lain;
        } else {
            $request->validate([
                "no_kk" => "required|max:16|min:16",
                "id_kartu_keluarga" => "required|unique:pengguna,id_kartu_keluarga",
                "nama_pengguna" => "required",
                "jabatan" => "required",
                "kata_sandi" => "required",
                "peran" => "required",
            ]);
            $jabatan = $request->jabatan;
        }

        $pengguna = [
            "id_kartu_keluarga" => $request->id_kartu_keluarga,
            "nama_pengguna" => $request->nama_pengguna,
            "nama_lengkap" => $request->nama_lengkap,
            "kata_sandi" => Hash::make($request->kata_sandi),
            "peran" => $request->peran,
            "jabatan" => $jabatan,
        ];

        penggunaModel::create($pengguna);

        return redirect()->route("pengguna.index")->withToastSuccess('Berhasil Menyimpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengguna = penggunaModel::findOrFail($id);

        $query_warga = wargaModel::query();
        $warga = wargaModel::with("kartukeluarga", "kartukeluarga.perumahan", "kartukeluarga.perumahan.cluster")
            ->where("id_kartu_keluarga", $pengguna->id_kartu_keluarga)
            ->where("status_hubungan_keluarga", "Kepala Keluarga")
            ->first();

        $jumlah_warga = $query_warga->clone()->where("id_kartu_keluarga", $pengguna->id_kartu_keluarga)->count();

        if (empty($warga)) {
            return redirect()->back()->withToastWarning('Tidak Ada Data Yang Ditampilkan');
        }
        return view("kepengurusan.pengguna.show", compact("pengguna", "warga", "jumlah_warga"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengguna = penggunaModel::with("kartukeluarga")->findOrFail($id);
        $kartu_keluarga = kartukeluargaModel::where("id_kartu_keluarga", "!=", "1")->get();
        $model_form = [
            "model" => "edit",
            "submit" => "Ubah",
            "btn" => "btn-warning",
        ];

        return view("kepengurusan.pengguna.edit", compact("pengguna", "kartu_keluarga", "model_form"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, penggunaModel $pengguna)
    {
        if ($request->jabatan == "Lain-lain") {
            $jabatan = $request->jabatan_lain;
        } else {
            $jabatan = $request->jabatan;
        }

        if (!empty($request->ganti_password)) {
            $request->validate([
                "no_kk" => "required|max:16|min:16",
                "id_kartu_keluarga" => "required|unique:pengguna,id_kartu_keluarga," . $request->old_id_kartu_keluarga . ",id_kartu_keluarga",
                "nama_pengguna" => "required",
                "kata_sandi" => "required",
                "peran" => "required",
                "status_pengguna" => "required",
            ]);

            $pengguna->update([
                "id_kartu_keluarga" => $request->id_kartu_keluarga,
                "nama_pengguna" => $request->nama_pengguna,
                "kata_sandi" => Hash::make($request->kata_sandi),
                "jabatan" => $jabatan,
                "peran" => $request->peran,
                "status_pengguna" => $request->status_pengguna,
            ]);

        } else {
            $request->validate([
                "no_kk" => "required|max:16|min:16",
                "id_kartu_keluarga" => "required|unique:pengguna,id_kartu_keluarga," . $request->old_id_kartu_keluarga . ",id_kartu_keluarga",
                "nama_pengguna" => "required",
                "peran" => "required",
                "status_pengguna" => "required",
            ]);

            $pengguna->update([
                "id_kartu_keluarga" => $request->id_kartu_keluarga,
                "nama_pengguna" => $request->nama_pengguna,
                "peran" => $request->peran,
                "jabatan" => $jabatan,
                "status_pengguna" => $request->status_pengguna,
            ]);
        }

        return redirect()->route("pengguna.index")->withToastSuccess('Berhasil Mengubah Data');
    }

    public function update_status_akun(Request $request)
    {
        $id_pengguna = $request->segment(2);
        $status_akun = $request->segment(3);

        if ($id_pengguna) {
            $update_pengguna = penggunaModel::findOrFail($id_pengguna);

            $update_pengguna->update([
                "status_akun" => $status_akun,
            ]);

            return redirect()->back()->withToastSuccess('Berhasil Mengubah Status');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(penggunaModel $pengguna)
    {
        try {
            $pengguna->delete();
        } catch (\Throwable$th) {
            return redirect()->route("pengguna.index")->withToastWarning('Terdapat kesalahan saat menghapus data');
        }
        return redirect()->route("pengguna.index")->withToastSuccess('Berhasil Menghapus Data');
    }
}
