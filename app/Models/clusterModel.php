<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class clusterModel extends Model
{
    use HasFactory;

    protected $table = "cluster";
    protected $primaryKey = "id_cluster";
    public $timestamps = false;

    protected $fillable = [
        "nama_cluster",
        "biaya_iuran",
        "biaya_iuran_tidak_ditempati",
        "status_cluster",
    ];

    public function perumahan()
    {
        return $this->hasMany(perumahanModel::class, "id_cluster", "id_cluster");
    }

    public function metodepembayaran()
    {
        return $this->hasMany(metodepembayaranModel::class, "id_cluster", "id_cluster");
    }
}
