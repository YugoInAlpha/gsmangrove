<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kartukeluargaModel extends Model
{
    use HasFactory;

    protected $table = "kartu_keluarga";
    protected $primaryKey = "id_kartu_keluarga";
    public $timestamps = false;

    protected $fillable = [
        "no_kk",
        "file_kk",
        "file_pernikahan",
    ];

    public function pengguna()
    {
        return $this->hasMany(penggunaModel::class, "id_kartu_keluarga", "id_kartu_keluarga");
    }

    public function warga()
    {
        return $this->hasMany(wargaModel::class, "id_kartu_keluarga", "id_kartu_keluarga");
    }

    public function perumahan()
    {
        return $this->belongsToMany(perumahanModel::class, "perumahan_warga", "id_kartu_keluarga", "id_perumahan");
    }
}
