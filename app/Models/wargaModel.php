<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class wargaModel extends Model
{
    use HasFactory;

    protected $table = "warga";
    protected $primaryKey = "id_warga";
    public $timestamps = false;

    protected $fillable = [
        "nik",
        "id_kartu_keluarga",
        "nama_lengkap",
        "alamat_ktp",
        "jenis_kelamin",
        "agama",
        "tempat_lahir",
        "tanggal_lahir",
        "golongan_darah",
        "nomor_telepon",
        "status_kawin",
        "pekerjaan",
        "status_hubungan_keluarga",
        "file_ktp",
    ];

    public function getAgeAttribute()
    {
        return $this->tanggal_lahir->diffInYears(Carbon::now());
    }

    public function kartukeluarga()
    {
        return $this->belongsTo(kartukeluargaModel::class, "id_kartu_keluarga", "id_kartu_keluarga");
    }

    public function iuran()
    {
        return $this->hasMany(iuranModel::class, "id_warga", "id_warga");
    }
}
