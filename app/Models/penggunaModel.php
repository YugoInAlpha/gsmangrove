<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class penggunaModel extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = "pengguna";
    protected $primaryKey = "id_pengguna";
    public $timestamps = false;

    protected $fillable = [
        "id_kartu_keluarga",
        "nama_pengguna",
        "kata_sandi",
        "peran",
        "jabatan",
        "status_akun",
        "status_pengguna",
    ];

    public function kartukeluarga()
    {
        return $this->belongsTo(kartukeluargaModel::class, 'id_kartu_keluarga', 'id_kartu_keluarga');
    }

    public function username()
    {
        return 'nama_pengguna';
    }

    public function getAuthPassword()
    {
        return $this->kata_sandi;
    }

    public function setPasswordAttribute()
    {
        return $this->attributes['kata_sandi'];
    }
}
