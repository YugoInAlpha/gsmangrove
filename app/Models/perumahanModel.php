<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class perumahanModel extends Model
{
    use HasFactory;

    protected $table = "perumahan";
    protected $primaryKey = "id_perumahan";
    public $timestamps = false;

    protected $fillable = [
        "id_cluster",
        "blok_perumahan",
        "nama_perumahan",
        "kepemilikan_rumah",
        "status_perumahan",
        "periode_kontrak",
        "periode_waktu",
        "tanggal_mulai_kontrak",
        "tanggal_akhir_kontrak",
    ];

    public function cluster()
    {
        return $this->belongsTo(clusterModel::class, "id_cluster", "id_cluster");
    }

    public function kartukeluarga()
    {
        return $this->belongsToMany(kartukeluargaModel::class, "perumahan_warga", "id_perumahan", "id_kartu_keluarga");
    }

    public function iuran()
    {
        return $this->hasMany(iuranModel::class, "id_perumahan", "id_perumahan");
    }
}
