<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class metodepembayaranModel extends Model
{
    use HasFactory;

    protected $table = "metode_pembayaran";
    protected $primaryKey = "id_metode_pembayaran";
    public $timestamps = false;

    protected $fillable = [
        "nama_rekening",
        "id_cluster",
        "nomor_rekening",
        "rekening_atas_nama",
        "status_rekening",
    ];

    public function cluster()
    {
        return $this->belongsTo(clusterModel::class, "id_cluster", "id_cluster");
    }

    public function iuran()
    {
        return $this->hasMany(iuranModel::class, "id_metode_pembayaran", "id_metode_pembayaran");
    }
}
