<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detailiuranModel extends Model
{
    use HasFactory;

    protected $table = "detail_transaksi_iuran";
    protected $primaryKey = "id_detail_transaksi_iuran";
    public $timestamps = false;

    protected $fillable = [
        "id_transaksi_iuran",
        "periode_iuran_bulan",
        "deskripsi_iuran",
        "biaya_iuran",
    ];

    public function iuran()
    {
        return $this->belongsTo(iuranModel::class, "id_transaksi_iuran", "id_transaksi_iuran");
    }
}
