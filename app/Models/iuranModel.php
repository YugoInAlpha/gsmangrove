<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class iuranModel extends Model
{
    use HasFactory;

    protected $table = "transaksi_iuran";
    protected $primaryKey = "id_transaksi_iuran";
    public $timestamps = false;

    protected $fillable = [
        "id_metode_pembayaran",
        "id_perumahan",
        "id_warga",
        "tanggal_transaksi",
        "periode_pembayaran",
        "total_iuran",
        "file_bukti_transaksi",
        "catatan",
        "status_pembayaran",
    ];

    public function warga()
    {
        return $this->belongsTo(wargaModel::class, "id_warga", "id_warga");
    }

    public function perumahan()
    {
        return $this->belongsTo(perumahanModel::class, "id_perumahan", "id_perumahan");
    }

    public function metodepembayaran()
    {
        return $this->belongsTo(metodepembayaranModel::class, "id_metode_pembayaran", "id_metode_pembayaran");
    }

    public function detailiuran()
    {
        return $this->hasMany(detailiuranModel::class, 'id_transaksi_iuran', 'id_transaksi_iuran');
    }
}
