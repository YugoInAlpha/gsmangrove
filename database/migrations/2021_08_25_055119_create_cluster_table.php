<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClusterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cluster', function (Blueprint $table) {
            $table->id("id_cluster");
            $table->string("nama_cluster", 50)->unique();
            $table->integer("biaya_iuran");
            $table->integer("biaya_iuran_tidak_ditempati");
            $table->enum("status_cluster", ["Aktif", "Tidak Aktif"])->default("Aktif");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('cluster');
        Schema::disableForeignKeyConstraints();
    }
}
