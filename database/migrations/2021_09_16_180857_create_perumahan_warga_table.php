<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerumahanWargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perumahan_warga', function (Blueprint $table) {
            $table->unsignedBigInteger("id_perumahan");
            $table->unsignedBigInteger("id_kartu_keluarga");

            $table->foreign('id_perumahan')->references('id_perumahan')->on('perumahan');
            $table->foreign('id_kartu_keluarga')->references('id_kartu_keluarga')->on('kartu_keluarga');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('perumahan_warga');
        Schema::disableForeignKeyConstraints();
    }
}
