<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warga', function (Blueprint $table) {
            $table->id("id_warga");
            $table->string("nik", 16)->unique();
            $table->unsignedBigInteger("id_kartu_keluarga");
            $table->string("nama_lengkap", 100);
            $table->enum("jenis_kelamin", ["Laki-laki", "Perempuan"]);
            $table->string("agama", 50);
            $table->string("nomor_telepon", 15);
            $table->string("pekerjaan", 50);
            $table->string("alamat_ktp");
            $table->enum("status_kawin", ["Menikah", "Belum Menikah"]);
            $table->date("tanggal_lahir");
            $table->string("tempat_lahir", 50);
            $table->enum("golongan_darah", ["A", "B", "AB", "O"]);
            $table->enum("status_hubungan_keluarga", [
                "Kepala Keluarga",
                "Suami",
                "Istri",
                "Anak",
                "Menantu",
                "Cucu",
                "Orang Tua",
                "Mertua",
                "Famili Lain",
                "Pembantu",
                "Lainnya",
            ]);
            $table->string("file_ktp")->nullable();

            $table->foreign('id_kartu_keluarga')->references('id_kartu_keluarga')->on('kartu_keluarga');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('warga');
        Schema::disableForeignKeyConstraints();
    }
}
