<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksiIuranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_iuran', function (Blueprint $table) {
            $table->id("id_detail_transaksi_iuran");
            $table->unsignedBigInteger("id_transaksi_iuran");
            $table->date("periode_iuran_bulan");
            $table->string("deskripsi_iuran", 100);
            $table->integer("biaya_iuran");

            $table->foreign('id_transaksi_iuran')->references('id_transaksi_iuran')->on('transaksi_iuran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('detail_transaksi_iuran');
        Schema::disableForeignKeyConstraints();
    }
}
