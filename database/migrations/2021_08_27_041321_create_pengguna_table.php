<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenggunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengguna', function (Blueprint $table) {
            $table->id("id_pengguna");
            $table->unsignedBigInteger("id_kartu_keluarga");
            $table->string("nama_pengguna");
            $table->string("kata_sandi");
            $table->enum("peran", [
                "Warga",
                "Pihak Kepengurusan GSM",
                "Administrator",
            ])->default("Warga");
            $table->string("jabatan")->default("Warga");
            $table->enum("status_pengguna", ["Aktif", "Tidak Aktif"])->default("Aktif");
            $table->enum("status_akun", ["Terkonfirmasi", "Menunggu Konfirmasi", "Data Tidak Sesuai Ketentuan"])->default("Menunggu Konfirmasi");

            $table->foreign('id_kartu_keluarga')->references('id_kartu_keluarga')->on('kartu_keluarga');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('pengguna');
        Schema::disableForeignKeyConstraints();
    }
}
