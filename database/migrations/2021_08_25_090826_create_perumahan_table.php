<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerumahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perumahan', function (Blueprint $table) {
            $table->id("id_perumahan");
            $table->unsignedBigInteger("id_cluster");
            $table->string("blok_perumahan", 10);
            $table->string("nama_perumahan", 10);
            $table->string("kepemilikan_rumah", 50)->default("Kosong");
            $table->integer("periode_kontrak")->nullable();
            $table->string("periode_waktu")->nullable();
            $table->date("tanggal_mulai_kontrak")->nullable();
            $table->date("tanggal_akhir_kontrak")->nullable();
            $table->enum("status_perumahan", ["Aktif", "Tidak Aktif"])->default("Aktif");

            $table->foreign('id_cluster')->references('id_cluster')->on('cluster');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('perumahan');
        Schema::disableForeignKeyConstraints();
    }
}
