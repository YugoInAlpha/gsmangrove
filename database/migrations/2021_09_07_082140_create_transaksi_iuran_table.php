<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiIuranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_iuran', function (Blueprint $table) {
            $table->id("id_transaksi_iuran");
            $table->unsignedBigInteger("id_metode_pembayaran");
            $table->unsignedBigInteger("id_perumahan");
            $table->unsignedBigInteger("id_warga");
            $table->date("tanggal_transaksi");
            $table->string("periode_pembayaran", 15);
            $table->integer("total_iuran");
            $table->string("file_bukti_transaksi");
            $table->string("catatan")->nullable();
            $table->enum("status_pembayaran", ["Menunggu Konfirmasi", "Pembayaran Sukses", "Pembayaran Gagal"])->default("Menunggu Konfirmasi");

            $table->foreign('id_metode_pembayaran')->references('id_metode_pembayaran')->on('metode_pembayaran');
            $table->foreign('id_perumahan')->references('id_perumahan')->on('perumahan');
            $table->foreign('id_warga')->references('id_warga')->on('warga');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_iuran');
    }
}
