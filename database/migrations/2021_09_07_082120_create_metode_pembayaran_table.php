<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetodePembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metode_pembayaran', function (Blueprint $table) {
            $table->id("id_metode_pembayaran");
            $table->unsignedBigInteger("id_cluster");
            $table->string("nama_rekening", 50);
            $table->string("nomor_rekening", 50);
            $table->string("rekening_atas_nama", 50);
            $table->enum("status_rekening", ["Aktif", "Tidak Aktif"])->default("Aktif");

            $table->foreign('id_cluster')->references('id_cluster')->on('cluster');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('metode_pembayaran');
        Schema::disableForeignKeyConstraints();
    }
}
