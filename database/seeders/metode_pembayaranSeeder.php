<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class metode_pembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $metode_pembayaran = [
            [
                "id_cluster" => "1",
                "nama_rekening" => "BCA",
                "nomor_rekening" => "22518410100163",
                "rekening_atas_nama" => "Adjie Pramana Putra",
            ],
        ];

        DB::table("metode_pembayaran")->insert($metode_pembayaran);
    }
}
