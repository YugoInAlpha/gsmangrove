<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class perumahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perumahan = [
            [
                "id_cluster" => 1,
                "blok_perumahan" => "L1",
                "nama_perumahan" => "15",
                "kepemilikan_rumah" => "Kosong",
            ],
            [
                "id_cluster" => 1,
                "blok_perumahan" => "L1",
                "nama_perumahan" => "14",
                "kepemilikan_rumah" => "Kosong",
            ],
            [
                "id_cluster" => 1,
                "blok_perumahan" => "L1",
                "nama_perumahan" => "13",
                "kepemilikan_rumah" => "Kosong",
            ],
        ];

        DB::table("perumahan")->insert($perumahan);
    }
}
