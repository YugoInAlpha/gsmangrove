<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class kartukeluargaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kartu_keluarga = [
            [
                "no_kk" => "84111870965Admin",
                "file_kk" => "file_pernikahan_84111870965Admin",
            ],
        ];

        DB::table("kartu_keluarga")->insert($kartu_keluarga);
    }
}
