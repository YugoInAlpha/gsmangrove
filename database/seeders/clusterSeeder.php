<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class clusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cluster = [
            [
                "nama_cluster" => "Econtus",
                "biaya_iuran" => 100000,
                "biaya_iuran_tidak_ditempati" => 50000,
            ],
            [
                "nama_cluster" => "Osbornia",
                "biaya_iuran" => 100000,
                "biaya_iuran_tidak_ditempati" => 50000,
            ],
            [
                "nama_cluster" => "Agiceras",
                "biaya_iuran" => 100000,
                "biaya_iuran_tidak_ditempati" => 50000,
            ],
        ];

        DB::table("cluster")->insert($cluster);
    }
}
