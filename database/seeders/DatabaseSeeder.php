<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            clusterSeeder::class,
            perumahanSeeder::class,
            kartukeluargaSeeder::class,
            penggunaSeeder::class,
            metode_pembayaranSeeder::class,
        ]);
    }
}
