<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class penggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pengguna = [
            [
                "id_kartu_keluarga" => "1",
                "nama_pengguna" => "role-Administrator",
                "kata_sandi" => Hash::make("role-Administrator"),
                "peran" => "Administrator",
                "status_akun" => "Terkonfirmasi",
            ],
        ];

        DB::table("pengguna")->insert($pengguna);
    }
}
