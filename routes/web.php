<?php

use App\Http\Controllers\anggotakeluargaController;
use App\Http\Controllers\authController;
use App\Http\Controllers\clusterController;
use App\Http\Controllers\dashboardController;
use App\Http\Controllers\iuranwargaController;
use App\Http\Controllers\KKwargaController;
use App\Http\Controllers\laporaniuranController;
use App\Http\Controllers\metodepembayaranController;
use App\Http\Controllers\pencarianController;
use App\Http\Controllers\penggunaController;
use App\Http\Controllers\perumahanController;
use App\Http\Controllers\profilController;
use App\Http\Controllers\registrasiController;
use App\Http\Controllers\rumahController;
use App\Http\Controllers\rumahwargaController;
use App\Http\Controllers\transaksiiuranController;
use App\Http\Controllers\wargaController;
use App\Models\perumahanModel;
use App\Models\wargaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::middleware("guest")->group(function () {
    Route::get('/', [authController::class, "login"])->name("login");
    Route::post('authentication', [authController::class, "store"])->name("login.authentication");

    Route::get('registrasi/formulir_1', [registrasiController::class, 'form_1'])->name('registrasi.form_1');
    Route::post('registrasi/formulir_1', [registrasiController::class, 'store_1'])->name('registrasi.store_1');

    Route::get('registrasi/formulir_2', [registrasiController::class, 'form_2'])->name('registrasi.form_2');
    Route::post('registrasi/formulir_2', [registrasiController::class, 'store_2'])->name('registrasi.store_2');

    Route::get('registrasi/formulir_3', [registrasiController::class, 'form_3'])->name('registrasi.form_3');
    Route::post('registrasi/formulir_3', [registrasiController::class, 'store_3'])->name('registrasi.store_3');

    Route::get('registrasi/formulir_4', [registrasiController::class, 'form_4'])->name('registrasi.form_4');
    Route::post('registrasi/formulir_4', [registrasiController::class, 'store_4'])->name('registrasi.store_4');
});

Route::middleware(['auth', 'konfirmasi'])->group(function () {
    Route::middleware(['auth'])->group(function () {
        Route::get('/dashboardV1', [dashboardController::class, 'dashboardV1'])->name("dashboardV1");
        Route::get('/akses_kepengurusan', [dashboardController::class, 'akses_kepengurusan'])->name("akses.kepengurusan");
        Route::get('/akses_warga', [dashboardController::class, 'akses_warga'])->name("akses.warga");

        Route::resource('warga', wargaController::class);
        Route::get('/warga/{id_kartu_keluarga}/create', [wargaController::class, 'create'])->name("warga.create_spesifik");
        Route::post('/warga_export/dokumen', [wargaController::class, 'export_dokumen_warga'])->name("warga.export_dokumen");
        Route::post('/warga_export/data', [wargaController::class, 'export_data_warga'])->name("warga.export_data");

        Route::resource('cluster', clusterController::class)->except(["show"]);

        Route::resource('perumahan', perumahanController::class)->except(["show"]);

        Route::resource('kartu_keluarga_warga', KKwargaController::class)->parameters([
            'kartu_keluarga_warga' => 'kartu_keluarga',
        ]);

        Route::get('/rumah_warga/{id_kartu_keluarga}/create', [rumahwargaController::class, 'create'])->name("rumah_warga.create");
        Route::post('/rumah_warga/{id_kartu_keluarga}', [rumahwargaController::class, 'store'])->name("rumah_warga.store");
        Route::get('/rumah_warga/{id_rumah}/{id_kartu_keluarga}/edit', [rumahwargaController::class, 'edit'])->name("rumah_warga.edit");
        Route::put('/rumah_warga/{id_rumah}/{id_kartu_keluarga}', [rumahwargaController::class, 'update'])->name("rumah_warga.update");
        Route::delete('/rumah_warga/{id_rumah}/{id_kartu_keluarga}', [rumahwargaController::class, 'destroy'])->name("rumah_warga.destroy");

        Route::resource('metode_pembayaran', metodepembayaranController::class);

        Route::get('/iuran_warga', [iuranwargaController::class, 'index'])->name("iuran_warga.index");
        Route::get('/iuran_warga/{transaksi_iuran}', [iuranwargaController::class, 'show'])->name("iuran_warga.show");
        Route::get('/konfirmasi_iuran_warga/{transaksi_iuran}/{warga}/{status_iuran?}', [iuranwargaController::class, 'update'])->name("konfirmasi_iuran_warga.edit");

        Route::get('/pencarian', [pencarianController::class, 'index'])->name("pencarian.index");
        Route::post('/pencarian', [pencarianController::class, 'store'])->name("pencarian.store");
        Route::get('/pencarian/{warga}/result/{id_perumahan?}', [pencarianController::class, 'result_search'])->name("pencarian.search_rumah");
        Route::post('/pencarian/{warga}/result', [pencarianController::class, 'result_search_post'])->name("pencarian.search");
        Route::get('/pencarian/{transaksi_iuran}', [pencarianController::class, 'show'])->name("pencarian.show");
        Route::get('/konfirmasi_iuran/{transaksi_iuran}/{status_iuran?}/{warga?}/{perumahan?}', [pencarianController::class, 'update_status_iuran'])->name("pencarian.update_status_iuran");

        Route::get('/laporan_iuran', [laporaniuranController::class, 'index'])->name("laporan_iuran.index");
        Route::post('/laporan_iuran/laporan_tahunan', [laporaniuranController::class, 'laporan_tahunan'])->name("laporan_iuran.laporan_tahunan");
        Route::get('/laporan_iuran/laporan_tahunan/export', [laporaniuranController::class, 'laporan_tahunan_export'])->name("laporan_iuran.laporan_tahunan_export");
        Route::post('/laporan_iuran/laporan_bulanan', [laporaniuranController::class, 'laporan_bulanan'])->name("laporan_iuran.laporan_bulanan");

        Route::resource('pengguna', penggunaController::class);
        Route::get('/konfirmasi_pengguna/{pengguna}/{status_akun?}', [penggunaController::class, 'update_status_akun'])->name("pengguna.update_status_akun");
    });

    Route::get("profil/{pengguna}", [profilController::class, "index"])->name("profil.index");
    Route::put("profil/{pengguna}", [profilController::class, "update"])->name("profil.update");

    Route::middleware(['warga'])->group(function () {
        Route::get('/dashboardV2', [dashboardController::class, 'dashboardV2'])->name("dashboardV2");
        Route::post('/daftar_rumah_warga', [dashboardController::class, 'daftar_rumah_warga'])->name("daftar.rumah_warga");

        Route::resource('rumah', rumahController::class)->parameters([
            'rumah' => 'perumahan',
        ]);

        Route::resource('anggota_keluarga', anggotakeluargaController::class)->parameters([
            'anggota_keluarga' => 'warga',
        ]);
        Route::get("kartu_keluarga/{kartu_keluarga}/edit", [anggotakeluargaController::class, "edit_kartu_keluarga"])->name("kartu_keluarga.edit");
        Route::put("kartu_keluarga/{kartu_keluarga}", [anggotakeluargaController::class, "update_kartu_keluarga"])->name("kartu_keluarga.update");

        Route::resource('transaksi_iuran', transaksiiuranController::class);
        Route::post('rekening-dropdown', [transaksiiuranController::class, 'getRekening'])->name('rekening-dropdown');
        Route::get("transaksi_iuran_bulan/{bulan}/{tahun}/{id_perumahan}", function ($bulan, $tahun, $id_perumahan) {

            $warga = wargaModel::where("id_warga", session()->get('auth_data.0.id_warga'))->first();
            $perumahan = perumahanModel::with("cluster")->where("id_perumahan", $id_perumahan)->get();

            if (empty($warga)) {
                $warga = wargaModel::where("id_kartu_keluarga", session()->get('auth_data.0.id_kartu_keluarga'))->where("status_hubungan_keluarga", "Kepala Keluarga")->firstOrFail();
            }

            return view("pengguna.transaksi_iuran.create_option", compact("bulan", "tahun", "perumahan", "warga"));
        })->name("transaksi_iuran_bulan");
    });

    Route::post('/logout', [authController::class, "logout"])->name("logout");
});

Route::get("clear_data", function (Request $request) {
    $request->session()->flush();

    return redirect()->back();
});
